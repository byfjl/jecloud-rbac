# RBAC服务项目

## 项目简介

JECloud RBAC管理模块，用于管理平台的机构、组织、人员、账号和角色权限等，为平台提供统一的组织、用户、权限等管理机制。
其主要包括如下功能：

- 顶部菜单管理
- 菜单管理
- 机构管理
- 公司管理
- 组织管理
- 员工管理
- 账号管理
- 角色授权
- 数据授权
- 日志管理
  - 登录日志
  - 访问日志
- 外部管理
  - 开发者管理
  - 供应商管理

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- aggregate-tomcat： SpringBoot Tomcat聚合模块
- jecloud-rbac-facade: 接口门面模块
- jecloud-rbac-sdk: 消费者模块
- jecloud-rbac-service: 提供者模块

## 编译部署

使用maven生成对应环境的jar包，例: 生产环境打包
``` shell
//开发环境打包
mvn clean package -Pdev -Dmaven.test.skip=true
//本地开发环境打包
mvn clean package -Plocal -Dmaven.test.skip=true
//测试环境打包
mvn clean package -Ptest -Dmaven.test.skip=true
//生产环境打包
mvn clean package -Pprod -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)