/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.exception.AccountException;
import java.util.List;
import java.util.Map;

/**
 * 账户远程服务
 */
public interface AccountRpcService {

    /**
     * 获取增加或移除的id
     *
     * @param type
     * @param originalIds
     * @param currentIds
     * @return
     */
     String getAddedOrDeleteIds(String type, String originalIds, String currentIds);
    /**
     * 根据账户ID查找
     *
     * @param id
     * @return
     */
    DynaBean findAccountById(String id);

    /**
     * 根据账户类型查找账户
     *
     * @param account
     * @return
     */
    DynaBean findAccountByPhone(String account);

    /**
     * 根据账户类型查找账户
     *
     * @param account
     * @return
     */
    DynaBean findAccountByType(String type, String account);

    /**
     * 查找所有账号
     * @param type
     * @param account
     * @return
     */
    List<DynaBean> findAllTenantAccount(String type,String account);

    /**
     * 查找所有账号
     * @param type 登录类型
     * @param tenantId 租户ID
     * @param account
     * @return
     */
    DynaBean findTenantAccount(String type,String tenantId,String account);

    /**
     * 根据账户类型查找账户
     * @param type
     * @param account
     * @return
     */
    DynaBean findLoginAccountByType(String type, String account) throws AccountException;

    /**
     * 根据关联机构ID查找
     *
     * @param associationId
     * @return
     */
    List<DynaBean> findAccountByAssociationId(String associationId);

    /**
     * 根据公司ID查找
     *
     * @param companyId
     * @return
     */
    List<DynaBean> findAccountByCompanyId(String companyId);

    /**
     * 账户是否过期
     *
     * @param id
     * @return
     */
    boolean isExpire(String id) throws AccountException, AccountException;

    /**
     * 是否锁定
     *
     * @param id
     * @return
     */
    boolean isLocked(String id) throws AccountException;

    /**
     * 检查账户是否禁用
     *
     * @param accountId
     * @return
     */
    boolean checkDisable(String accountId);

    /**
     * 查找账户角色id
     * @param tenantId
     * @param accountId
     * @return
     */
    List<String> findAccountRoleIds(String tenantId,String accountId);

    /**
     * 查找账户部门
     * @param tenantId
     * @param accountId
     * @return
     */
    List<String> findAccountDeptIds(String tenantId,String accountId);

    /**
     * 查找账户机构
     * @param accountId
     * @return
     */
    List<String> findAccountOrgIds(String accountId);

    /**
     * 批量查账号信息
     */
    List<DynaBean> findBatchAccount(String tableCode,String ids,String column);


    /***
     * 获取账号部门树形
     * @param deptMapList
     * @return
     */
    JSONTreeNode getAccountDeptTreeNode(List<Map<String,Object>> deptMapList);
}
