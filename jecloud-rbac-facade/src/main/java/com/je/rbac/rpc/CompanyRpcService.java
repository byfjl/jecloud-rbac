/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import java.util.List;

public interface CompanyRpcService {

    /**
     * 查找所有
     *
     * @return
     */
    List<DynaBean> findAll();

    /**
     * 根据id查找
     *
     * @param id
     * @return
     */
    DynaBean findById(String id);

    /**
     * 根据编码查找
     *
     * @param code
     * @return
     */
    DynaBean findByCode(String code);

    /**
     * 根据名称查找
     *
     * @param name
     * @return
     */
    DynaBean findByName(String name);

    /**
     * 查找父级子公司
     *
     * @param id
     * @return
     */
    DynaBean findParent(String id);

    /**
     * 查找祖先公司
     *
     * @param id
     * @return
     */
    DynaBean findAncestors(String id);

    /**
     * 查找下一层的子公司
     *
     * @param id
     * @return
     */
    List<DynaBean> findNextChildren(String id, boolean containMe);

    /**
     * 查找所有子公司
     *
     * @param id
     * @param containMe
     * @return
     */
    List<DynaBean> findAllChildren(String id, boolean containMe);

    /**
     * 根据管理员查找
     *
     * @return
     */
    List<DynaBean> findByManagerId(String managerId);

    /**
     * 根据主管查找
     *
     * @param majorId
     * @return
     */
    List<DynaBean> findByMajorId(String majorId);

    /**
     * 禁用公司
     *
     * @param companyIds
     */
    void disableCompany(String companyIds);

    /**
     * 启用公司
     *
     * @param companyIds
     */
    void enableCompany(String companyIds);


    /**
     * 删除公司
     *
     * @param companyIds
     * @param withDeptAndUsers
     */
    long removeCompany(String companyIds, boolean withDeptAndUsers);

    /**
     * 构建公司树
     *
     * @param companyIds
     * @return
     */
    JSONTreeNode buildCompanyTree(String... companyIds);

}
