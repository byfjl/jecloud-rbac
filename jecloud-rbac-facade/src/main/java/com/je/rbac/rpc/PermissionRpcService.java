/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import java.util.List;
import java.util.Map;

/**
 * 权限RPC服务
 */
public interface PermissionRpcService {

    /**
     * 查找账户所有权限
     * @param loginId
     * @return
     */
    List<String> findAccountPermissionsWithLoginId(String loginId,String tenantId);

    /**
     * 查找账户所有权限
     * @param accountId
     * @return
     */
    List<String> findAccountPermissions(String departmentId,String accountId,String tenantId);

    /**
     * 查找账户功能权限
     * @param accountId
     * @return
     */
    List<String> findAccountFuncPermissions(String departmentId,String accountId,String tenantId,String funcCode);

    /**
     * 查找账户功能权限
     * @param accountId
     * @return
     */
    List<String> findAccountPluginPermissions(String departmentId,String accountId,String tenantId,String pluginCode);

    /**
     * 查找账户菜单权限
     * @param accountId
     * @return
     */
    List<String> findAccountMenuPermissions(String departmentId,String accountId,String tenantId);

    /**
     * 查找顶部菜单权限
     * @param accountId
     * @return
     */
    List<String> findAccountTopMenuPermissions(String departmentId,String accountId,String tenantId);

    /**
     * 快速菜单授权
     * @param menuId
     */
    void quickGrantMenu(String productId,String menuId);

    /**
     * 快速授权功能
     * @param funcCode
     */
    void quickGranFunc(String productId,String funcCode);

    /**
     * 快速授权子功能
     * @param productId 产品id
     * @param subFuncRelationId 子功能id
     */
    void quickGranSubFunc(String productId,String subFuncRelationId);

    /**
     * 快速授权功能
     * @param pluginCode
     */
    void quickGranPlugin(String productId,String pluginCode);

    /**
     * 快速授权功能按钮
     * @param productId
     * @param funcButtonMap
     */
    void quickGrantFuncButton(String productId, Map<String,List<String>> funcButtonMap);

    /**
     * 清除角色账号缓存
     * @param tenantId
     * @param roleIdList
     */
    void clearRoleAccountPermissions(String tenantId,List<String> roleIdList);

    /**
     * 清除部门账号缓存
     * @param deptIdList
     */
    void clearDeptAccountPermissions(String tenantId,List<String> deptIdList);

    /**
     * 清除机构账号缓存
     * @param orgIdList
     */
    void clearOrgAccountPermissions(String tenantId,List<String> orgIdList);

    /**
     * 清除机构账号缓存
     * @param acountIdList
     */
    void clearAccountPermissions(String tenantId,List<String> acountIdList);

    /**
     * 清除机构账号缓存
     * @param permGroupIdList
     */
    void clearPermGroupAccountPermissions(String tenantId,List<String> permGroupIdList);

    /**
     * 功能重新授权
     * @param originalFuncCode
     * @param targetFuncCode
     */
    void transferFuncPermission(String originalFuncCode,String targetFuncCode);

    /**
     * 更改功能按钮权限编码
     * @param funcCode
     * @param oldButtonCode
     * @param newButtonCode
     */
    void modifyFuncButtonPermCode(String funcCode,String oldButtonCode,String newButtonCode);

    /**
     * 更改子功能编码
     * @param funcCode
     * @param oldSubFuncCode
     * @param newSubFuncCode
     */
    void modifySubFuncPermCode(String funcCode,String oldSubFuncCode,String newSubFuncCode);

}
