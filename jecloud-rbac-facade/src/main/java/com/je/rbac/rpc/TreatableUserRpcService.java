/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.model.AssignmentPermission;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * 流程可处理人RPC服务
 */
public interface TreatableUserRpcService {


    /***
     * 指定角色展示
     * @param accountId    账号Id
     * @param roleIds   角色Id
     * @param perm      权限过滤条件
     * @param orgFlag   是否按照公司展示
     * @return
     */
    JSONTreeNode findRoleUsersAndBuildTreeNode(String accountId, String roleIds, AssignmentPermission perm, Boolean orgFlag, Boolean multiple, Boolean addOwn);

    /**
     * 当前登录人是否在可处理的这些角色中
     * @param accountId
     * @param roleIds
     * @param perm
     * @return
     */
    Boolean checkRoleContainsCurrentUser (String accountId, String roleIds, AssignmentPermission perm);
    /***
     * 指定部门展示
     * @param accountId   账号ids
     * @param deptIds
     * @param perm
     * @param orgFlag
     * @return
     */
    JSONTreeNode findDeptUsersAndBuildTreeNode(String accountId, String deptIds, AssignmentPermission perm, Boolean orgFlag, Boolean multiple, Boolean addOwn);

    /**
     * 当前登录人是否在可处理的这些部门中
     * @param accountId
     * @param deptIds
     * @param perm
     * @return
     */
    Boolean checkDeptUsersContainsCurrentUser(String accountId, String deptIds, AssignmentPermission perm);
    /***
     * 指定人员展示
     * @param accountIds   账号id
     * @param orgFlag
     * @return
     */
    JSONTreeNode findUsersAndBuildTreeNodeByAccountIds(List<String> accountIds, Boolean orgFlag, Boolean multiple, Boolean addOwn);

    /***
     * 加载组织架构
     * @param accountId
     * @param perm
     * @return
     */
    JSONTreeNode findOrgStructureAndBuildTreeNode(String accountId, AssignmentPermission perm, Boolean multiple, Boolean addOwn);


    /***
     * 特殊处理
     * @param types
     * @param userId
     * @return
     */
    JSONTreeNode findUserToSpecialProcessing(List<String> types, String userId,String taskAssigner, String frontTaskAssigner, String starterUser, Boolean multiple, Boolean addOwn);

    /**
     * 特殊处理是否包含当前人员参照人
     * @param types
     * @param userId
     * @param taskAssigner
     * @param frontTaskAssigner
     * @return
     */
    Boolean checkSpecialProcessingContainsCurrentUser(List<String> types, String userId,String taskAssigner, String frontTaskAssigner, String starterUser);
}
