/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service;

/**
 * 审计日志
 */
public interface AuditLogService {

    /**
     *
     * @param clientIp 客户端IP
     * @param moduleName 模块名称
     * @param url 调用的Url
     * @param operaterTypeCode 操作类型类型
     * @param operaterTypeName 操作类型名称
     * @param logTypeCode 日志类型编码
     * @param logTypeName 日志类型名称
     * @param staus 操作状态
     * @param content 操作内容
     * @param result 操作结果
     */
    void log(String clientIp,String moduleName,String url,String operaterTypeCode,String operaterTypeName,String logTypeCode,String logTypeName,String operatorUserName,String operatorUserCode,String operatorUserId,String staus,String content,String result);

}
