/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.impl;

import cn.hutool.core.date.DateUtil;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.rbac.service.AuditLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AuditLogServiceImpl implements AuditLogService {

    @Autowired
    private MetaRbacService metaRbacService;
    @Autowired
    private CommonService commonService;

    @Override
    @Async
    public void log(String clientIp, String moduleName, String url, String operaterTypeCode, String operaterTypeName,
                    String logTypeCode, String logTypeName, String operatorUserName,
                    String operatorUserCode, String operatorUserId, String status,
                    String content, String result) {
        DynaBean auditLogBean = new DynaBean("JE_RBAC_AUDITLOG", false);
        auditLogBean.set("AUDITLOG_OPERATOR_NAME", operatorUserName);
        auditLogBean.set("AUDITLOG_OPERATOR_CODE", operatorUserCode);
        auditLogBean.set("AUDITLOG_OPERATOR_ID", operatorUserId);
        auditLogBean.set("AUDITLOG_OPERATOR_IP", clientIp);
        auditLogBean.set("AUDITLOG_URL", url);
        auditLogBean.set("AUDITLOG_OPERATE_TIME", DateUtil.now());
        auditLogBean.set("AUDITLOG_STATUS", status);
        auditLogBean.set("AUDITLOG_MODULE_NAME", moduleName);
        auditLogBean.set("AUDITLOG_TYPE_CODE", operaterTypeCode);
        auditLogBean.set("AUDITLOG_TYPE_NAME", operaterTypeName);
        auditLogBean.set("AUDITLOG_LOGTYPE_CODE", logTypeCode);
        auditLogBean.set("AUDITLOG_LOGTYPE_NAME", logTypeName);
        auditLogBean.set("AUDITLOG_OPERATE_CONTENT", content);
        auditLogBean.set("AUDITLOG_RESULT", result);
        commonService.buildModelCreateInfo(auditLogBean);
        metaRbacService.insert(auditLogBean);
    }

}
