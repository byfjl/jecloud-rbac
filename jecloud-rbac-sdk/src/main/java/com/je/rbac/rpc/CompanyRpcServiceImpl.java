/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyRpcServiceImpl implements CompanyRpcService {

    @RpcReference(microserviceName = "rbac",schemaId = "companyRpcService")
    private CompanyRpcService companyRpcService;

    @Override
    public List<DynaBean> findAll() {
        return companyRpcService.findAll();
    }

    @Override
    public DynaBean findById(String id) {
        return companyRpcService.findById(id);
    }

    @Override
    public DynaBean findByCode(String code) {
        return companyRpcService.findByCode(code);
    }

    @Override
    public DynaBean findByName(String name) {
        return companyRpcService.findByName(name);
    }

    @Override
    public DynaBean findParent(String id) {
        return companyRpcService.findParent(id);
    }

    @Override
    public DynaBean findAncestors(String id) {
        return companyRpcService.findAncestors(id);
    }

    @Override
    public List<DynaBean> findNextChildren(String id, boolean containMe) {
        return companyRpcService.findNextChildren(id,containMe);
    }

    @Override
    public List<DynaBean> findAllChildren(String id, boolean containMe) {
        return companyRpcService.findAllChildren(id,containMe);
    }

    @Override
    public List<DynaBean> findByManagerId(String managerId) {
        return companyRpcService.findByManagerId(managerId);
    }

    @Override
    public List<DynaBean> findByMajorId(String majorId) {
        return companyRpcService.findByMajorId(majorId);
    }

    @Override
    public void disableCompany(String companyIds) {
        companyRpcService.disableCompany(companyIds);
    }

    @Override
    public void enableCompany(String companyIds) {
        companyRpcService.enableCompany(companyIds);
    }

    @Override
    public long removeCompany(String companyIds, boolean withDeptAndUsers) {
        return companyRpcService.removeCompany(companyIds,withDeptAndUsers);
    }

    @Override
    public JSONTreeNode buildCompanyTree(String... companyIds) {
        return companyRpcService.buildCompanyTree(companyIds);
    }

}
