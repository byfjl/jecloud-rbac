/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RoleRpcServiceImpl implements RoleRpcService {

    @RpcReference(microserviceName = "rbac", schemaId = "roleRpcService")
    private RoleRpcService roleRpcService;

    @Override
    public JSONTreeNode buildRoleTreeByRoleIds(String roleIds,Boolean develop) {
        return roleRpcService.buildRoleTreeByRoleIds(roleIds,develop);
    }

    @Override
    public boolean createProductRoleAndGrant(String productId, String productName) {
        return roleRpcService.createProductRoleAndGrant(productId, productName);
    }

    @Override
    public List<String> findRoleUserIds(List<String> roleIds) {
        return roleRpcService.findRoleUserIds(roleIds);
    }

    @Override
    public DynaBean findProductDevelopRole(String productId) {
        return roleRpcService.findProductDevelopRole(productId);
    }

    @Override
    public List<DynaBean> findProductDevelopRoles(List<String> productIdList) {
        return roleRpcService.findProductDevelopRoles(productIdList);
    }

    @Override
    public void bindUserToProductRole(String roleId, List<String> userIdList) {
        roleRpcService.bindUserToProductRole(roleId,userIdList);
    }

    @Override
    public void removeProductRoleUsers(String roleId, List<String> userIdList) {
        roleRpcService.removeProductRoleUsers(roleId,userIdList);
    }

}