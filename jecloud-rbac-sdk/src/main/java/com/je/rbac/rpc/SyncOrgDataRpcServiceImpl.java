/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.common.base.result.BaseRespResult;
import com.je.rbac.model.syncdata.SyncCompany;
import com.je.rbac.model.syncdata.SyncDepartment;
import com.je.rbac.model.syncdata.SyncRole;
import com.je.rbac.model.syncdata.SyncUser;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SyncOrgDataRpcServiceImpl implements SyncCompanyDataRpcService,SyncDeptDataRpcService,SyncUserDataRpcService,SyncRoleDataRpcService,SyncRpcLoginRpcService {

    @RpcReference(microserviceName = "rbac",schemaId = "syncCompanyDataRpcService")
    private SyncCompanyDataRpcService syncCompanyDataRpcService;

    @RpcReference(microserviceName = "rbac",schemaId = "syncDeptDataRpcService")
    private SyncDeptDataRpcService syncDeptDataRpcService;

    @RpcReference(microserviceName = "rbac",schemaId = "syncUserDataRpcService")
    private SyncUserDataRpcService syncUserDataRpcService;

    @RpcReference(microserviceName = "rbac",schemaId = "syncRoleDataRpcService")
    private SyncRoleDataRpcService syncRoleDataRpcService;

    @RpcReference(microserviceName = "rbac",schemaId = "syncRpcLoginRpcService")
    private SyncRpcLoginRpcService syncRpcLoginRpcService;

    @Override
    public BaseRespResult syncCompanyData(List<SyncCompany> companyData) {
        return syncCompanyDataRpcService.syncCompanyData(companyData);
    }

    @Override
    public BaseRespResult syncDepartmentData(List<SyncDepartment> deptData) {
        return syncDeptDataRpcService.syncDepartmentData(deptData);
    }

    @Override
    public BaseRespResult syncRoleData(List<SyncRole> roleData) {
        return syncRoleDataRpcService.syncRoleData(roleData);
    }

    @Override
    public BaseRespResult syncUserData(List<SyncUser> userData) {
        return syncUserDataRpcService.syncUserData(userData);
    }

    @Override
    public BaseRespResult rpcLogin(String account,String tenantId,String type) {
        return syncRpcLoginRpcService.rpcLogin(account,tenantId,type);
    }
}
