/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.model.AssignmentPermission;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreatableUserRpcServiceImpl implements TreatableUserRpcService {

    @RpcReference(microserviceName = "rbac", schemaId = "treatableUserRpcService")
    private TreatableUserRpcService treatableUserRpcService;

    @Override
    public JSONTreeNode findRoleUsersAndBuildTreeNode(String accountId, String roleIds, AssignmentPermission perm, Boolean orgFlag, Boolean multiple, Boolean addOwn) {
        return treatableUserRpcService.findRoleUsersAndBuildTreeNode(accountId, roleIds, perm, orgFlag, multiple, addOwn);
    }

    @Override
    public Boolean checkRoleContainsCurrentUser(String accountId, String roleIds, AssignmentPermission perm) {
        return treatableUserRpcService.checkRoleContainsCurrentUser(accountId, roleIds, perm);
    }

    @Override
    public JSONTreeNode findDeptUsersAndBuildTreeNode(String accountId, String deptIds, AssignmentPermission perm, Boolean orgFlag, Boolean multiple, Boolean addOwn) {
        return treatableUserRpcService.findDeptUsersAndBuildTreeNode(accountId, deptIds, perm, orgFlag, multiple, addOwn);
    }

    @Override
    public Boolean checkDeptUsersContainsCurrentUser(String accountId, String deptIds, AssignmentPermission perm) {
        return treatableUserRpcService.checkDeptUsersContainsCurrentUser(accountId, deptIds, perm);
    }

    @Override
    public JSONTreeNode findUsersAndBuildTreeNodeByAccountIds(List<String> accountIds, Boolean orgFlag, Boolean multiple, Boolean addOwn) {
        return treatableUserRpcService.findUsersAndBuildTreeNodeByAccountIds(accountIds, orgFlag, multiple, addOwn);
    }

    @Override
    public JSONTreeNode findOrgStructureAndBuildTreeNode(String accountId, AssignmentPermission perm, Boolean multiple, Boolean addOwn) {
        return treatableUserRpcService.findOrgStructureAndBuildTreeNode(accountId, perm, multiple, addOwn);
    }

    @Override
    public JSONTreeNode findUserToSpecialProcessing(List<String> types, String userId, String taskAssigner, String frontTaskAssigner, String starterUser, Boolean multiple, Boolean addOwn) {
        return treatableUserRpcService.findUserToSpecialProcessing(types, userId, taskAssigner, frontTaskAssigner, starterUser, multiple, addOwn);
    }

    @Override
    public Boolean checkSpecialProcessingContainsCurrentUser(List<String> types, String userId, String taskAssigner, String frontTaskAssigner, String starterUser) {
        return treatableUserRpcService.checkSpecialProcessingContainsCurrentUser(types, userId, taskAssigner, frontTaskAssigner, starterUser);
    }
}
