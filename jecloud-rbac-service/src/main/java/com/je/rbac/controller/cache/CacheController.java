/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.cache;

import com.google.common.base.Strings;
import com.je.auth.AuthLoginManager;
import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.cache.*;
import com.je.rbac.rpc.PermissionRpcService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/rbac/cache")
public class CacheController implements CommonRequestService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private AccountPermissionCache accountPermissionCache;
    @Autowired
    private AuthLoginManager authLoginManager;
    @Autowired
    private PermissionRpcService permissionRpcService;
    @Autowired
    private FuncPermissionCache funcPermissionCache;
    @Autowired
    private AccountProductCache accountProductCache;

    @AuthCheckPermission(value = {"pc-plugin-JE_CORE_RBAC-config"})
    @RequestMapping(value = {"/clear"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult clear(HttpServletRequest request) {
        Object loginId = authLoginManager.getLoginId();
        if (loginId == null) {
            return BaseRespResult.errorResult("请登录后查看！");
        }

        String accountId = loginId.toString();
        boolean enableSaas = SpringContextHolder.getBean("enableSaas");
        String currentTenantId = "";
        if (enableSaas) {
            currentTenantId = SecurityUserHolder.getCurrentAccountTenantId();
        }
        if (Strings.isNullOrEmpty(currentTenantId)) {
            accountPermissionCache.removeCache(accountId);
            accountProductCache.removeCache(accountId);
        } else {
            accountPermissionCache.removeCache(currentTenantId, accountId);
            accountProductCache.removeCache(currentTenantId, accountId);
        }

        return BaseRespResult.successResult("清除成功！");
    }

    @AuthCheckPermission(value = {"pc-plugin-JE_CORE_RBAC-config"})
    @RequestMapping(value = {"/clearAccount"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult clearAccount(HttpServletRequest request) {
        String accountId = getStringParameter(request, "accountId");
        if (Strings.isNullOrEmpty(accountId)) {
            return BaseRespResult.errorResult("请选择要清楚的账户");
        }

        DynaBean accountBean = metaService.selectOne("JE_RBAC_ACCOUNT", ConditionsWrapper.builder().eq("JE_RBAC_ACCOUNT_ID", accountId));
        if (accountBean == null) {
            return BaseRespResult.errorResult("不存在当前账号！");
        }

        boolean enableSaas = SpringContextHolder.getBean("enableSaas");
        String currentTenantId = "";
        if (enableSaas) {
            currentTenantId = SecurityUserHolder.getCurrentAccountTenantId();
        }
        if (Strings.isNullOrEmpty(currentTenantId)) {
            accountPermissionCache.removeCache(accountId);
            accountProductCache.removeCache(accountId);
        } else {
            accountPermissionCache.removeCache(currentTenantId, accountId);
            accountProductCache.removeCache(currentTenantId, accountId);
        }

        return BaseRespResult.successResult("清除成功！");
    }

    @AuthCheckPermission(value = {"pc-plugin-JE_CORE_RBAC-config"})
    @RequestMapping(value = {"/clearAll"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult clearAll(HttpServletRequest request) {
        Object loginId = authLoginManager.getLoginId();
        if (loginId == null) {
            return BaseRespResult.errorResult("请登录后再进行操作！");
        }
        accountPermissionCache.clear();
        funcPermissionCache.clear();
        accountProductCache.clear();
        return BaseRespResult.successResult("清除成功！");
    }

}
