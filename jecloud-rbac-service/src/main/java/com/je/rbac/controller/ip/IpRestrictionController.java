/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.ip;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/je/rbac/ip/restrict")
public class IpRestrictionController extends AbstractPlatformController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean settingBean = manager.doSave(param, request);
        cache(settingBean);
        return BaseRespResult.successResult(settingBean.getValues());
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean settingBean = manager.doUpdate(param, request);
        if ("1".equals(settingBean.getStr("IPVISITSETTING_OPEN_CODE"))) {
            cache(settingBean);
        } else {
            clearCache();
        }
        return BaseRespResult.successResult(settingBean.getValues());
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        clearCache();
        return super.doRemove(param, request);
    }

    private void cache(DynaBean settingBean) {
        List<DynaBean> ruleBeans = metaService.select("JE_RBAC_IPRULE", ConditionsWrapper.builder().eq("JE_RBAC_IPVISITSETTING_ID", settingBean.getStr("JE_RBAC_IPVISITSETTING_ID")));
        if (ruleBeans == null || ruleBeans.isEmpty()) {
            return;
        }

        JSONObject settingObj = new JSONObject();
        settingObj.put("switch", settingBean.getStr("IPVISITSETTING_OPEN_CODE"));
        JSONArray ruleArray = new JSONArray();
        settingObj.put("rules", ruleArray);
        JSONObject eachRule;
        for (DynaBean eachBean : ruleBeans) {
            eachRule = new JSONObject();
            eachRule.put("accountId", eachBean.getStr("IPRULE_ACCOUNT_ID"));
            eachRule.put("accountName", eachBean.getStr("IPRULE_ACCOUNT_NAME"));
            eachRule.put("accountCode", eachBean.getStr("IPRULE_ACCOUNT_CODE"));
            eachRule.put("ruleType", eachBean.getStr("IPRULE_TYPE_CODE"));
            eachRule.put("ruleContent", eachBean.getStr("IPRULE_CONTENT"));
            ruleArray.add(eachRule);
        }
        stringRedisTemplate.opsForValue().set("ipRestriction", settingObj.toJSONString());
    }

    private void clearCache() {
        stringRedisTemplate.delete("ipRestriction");
    }

}
