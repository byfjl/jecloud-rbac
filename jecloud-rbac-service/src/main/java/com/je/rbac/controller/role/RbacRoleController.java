/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.role;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import com.je.rbac.cache.*;
import com.je.rbac.exception.RoleException;
import com.je.rbac.service.grant.DevelopPlatformService;
import com.je.rbac.service.grant.DevelopProductService;
import com.je.rbac.service.grant.PlatfromProductService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/rbac/cloud/role")
public class RbacRoleController implements CommonRequestService {

    @Autowired
    private DevelopPlatformService developPlatformService;
    @Autowired
    private DevelopProductService developProductService;
    @Autowired
    private MetaResourceService metaResourceService;
    @Autowired
    private PlatfromProductService platfromProductService;
    @Autowired
    private AccountPermissionCache accountPermissionCache;
    @Autowired
    private FuncPermissionCache funcPermissionCache;
    @Autowired
    private AccountProductCache accountProductCache;

//    @AuthCheckPermission(value = {"pc-plugin-JE_CORE_RBAC-config"})
    @RequestMapping(value = "/platfromRole", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult createPlatfromRoleAndGrant(HttpServletRequest request) {
        developPlatformService.createPlatfromRole(null);
        try {
            developPlatformService.grant(null);
        } catch (RoleException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("创建成功！");
    }

//    @AuthCheckPermission(value = {"pc-plugin-JE_CORE_RBAC-config"})
    @RequestMapping(value = "/platformProductRole", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult createPlatformProductRoleAndGrant(HttpServletRequest request) {
        platfromProductService.createPlatfromRole(null);
        try {
            platfromProductService.grant(null);
        } catch (RoleException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("创建成功！");
    }

    @RequestMapping(value = "/productRole", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult createProductRoleAndGrant(HttpServletRequest request) {
        String productId = getStringParameter(request, "productId");
        String productName = getStringParameter(request, "productName");

        DynaBean productBean = metaResourceService.selectOneByNativeQuery("JE_PRODUCT_MANAGE", NativeQuery.build()
                .eq("JE_PRODUCT_MANAGE_ID", productId));
        if (productBean == null) {
            return BaseRespResult.errorResult("不存在此产品！");
        }
        developProductService.createProductRole(productId, productName);
        try {
            developProductService.grant(productId);
            developProductService.assignProductRoleToUser(productId, productBean);
            accountPermissionCache.clear();
            funcPermissionCache.clear();
            accountProductCache.clear();
        } catch (RoleException e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult("创建成功！");
    }

    @RequestMapping(value = "/deleteProductRole", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult deleteProductRole(HttpServletRequest request) {
        String productId = getStringParameter(request, "productId");
        String productName = getStringParameter(request, "productName");
        DynaBean productBean = metaResourceService.selectOneByNativeQuery("JE_PRODUCT_MANAGE", NativeQuery.build()
                .eq("JE_PRODUCT_MANAGE_ID", productId));
        if (productBean == null) {
            return BaseRespResult.errorResult("不存在此产品！");
        }

        try {
            developProductService.delteProductRole(productId, productName);
            accountPermissionCache.clear();
            funcPermissionCache.clear();
            accountProductCache.clear();
            return BaseRespResult.successResult("删除成功！");
        }catch (RoleException e){
            return BaseRespResult.errorResult("删除失败！");
        }
    }

}
