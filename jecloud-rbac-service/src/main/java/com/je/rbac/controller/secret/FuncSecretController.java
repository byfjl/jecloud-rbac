/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.secret;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.rbac.service.secret.FuncSecretService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/je/rbac/secret/func")
public class FuncSecretController {

    @Autowired
    private FuncSecretService funcSecretService;

    @RequestMapping(value = "/doSaveOrUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        if(Strings.isNullOrEmpty(request.getParameter("JE_CORE_FUNCINFO_ID"))
                || Strings.isNullOrEmpty(request.getParameter("FUNCSECRET_FUNCNAME")) || Strings.isNullOrEmpty(request.getParameter("FUNCSECRET_FUNCCODE"))){
            return BaseRespResult.errorResult("请确认功能信息！");
        }
        if(Strings.isNullOrEmpty(request.getParameter("FUNCSECRET_SECRET_CODE"))
                || Strings.isNullOrEmpty(request.getParameter("FUNCSECRET_SECRET_NAME"))){
            return BaseRespResult.errorResult("请确认密级信息！");
        }
        //获取新增数据
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        dynaBean = funcSecretService.doSaveOrUpdate(dynaBean);
        return BaseRespResult.successResult(dynaBean.getValues());
    }

    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        if(Strings.isNullOrEmpty(request.getParameter("JE_CORE_FUNCINFO_ID"))){
            return BaseRespResult.errorResult("请确认功能信息！");
        }
        int result = funcSecretService.doRemove(request.getParameter("JE_CORE_FUNCINFO_ID"));
        return BaseRespResult.successResult(result > 0);
    }

    @RequestMapping(value = "/funcSecret", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult funcSecret(BaseMethodArgument param, HttpServletRequest request) {
        if(Strings.isNullOrEmpty(request.getParameter("JE_CORE_FUNCINFO_ID"))){
            return BaseRespResult.errorResult("请确认功能信息！");
        }
        Map<String,Object> result = funcSecretService.funcSecret(request.getParameter("JE_CORE_FUNCINFO_ID"));
        return BaseRespResult.successResult(result);
    }

}
