/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.secret;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.SystemSecretUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.service.secret.SecretSettingService;
import com.je.rbac.util.IpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/je/rbac/secret/systemsetting")
public class SecretSettingController extends AbstractPlatformController {

    @Autowired
    private SecretSettingService secretSettingService;

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        String agentIp = IpUtil.getIpAddress(request);
        String accountId = SecurityUserHolder.getCurrentAccountId();
        DynaBean settingBean = manager.doSave(param, request);
        secretSettingService.initSecretContext(settingBean);
        if ("1".equals(settingBean.getStr("SYSSECRET_OPEN_CODE"))) {
            secretSettingService.requireUserContextSecretInfo(agentIp, accountId);
        } else {
            secretSettingService.clearSecretContext();
        }
        return BaseRespResult.successResult(settingBean.getValues());
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        String agentIp = IpUtil.getIpAddress(request);
        String accountId = SecurityUserHolder.getCurrentAccountId();
        DynaBean settingBean = manager.doUpdate(param, request);
        secretSettingService.initSecretContext(settingBean);
        if ("1".equals(settingBean.getStr("SYSSECRET_OPEN_CODE"))) {
            secretSettingService.requireUserContextSecretInfo(agentIp, accountId);
        } else {
            secretSettingService.clearSecretContext();
        }
        return BaseRespResult.successResult(settingBean.getValues());
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        secretSettingService.clearSecretContext();
        return super.doRemove(param, request);
    }

    /**
     * 获取系统密级
     *
     * @return
     */
    @RequestMapping(value = "/systemSecretInfo", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult systemSecretInfo() {
        return BaseRespResult.successResult(secretSettingService.requireSystemSecretInfo());
    }

    /**
     * 获取用户上下文密级
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/userContextSecret", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult userContextSecret(HttpServletRequest request) {
        String agentIp = IpUtil.getIpAddress(request);
        String accountId = SecurityUserHolder.getCurrentAccountId();
        return BaseRespResult.successResult(secretSettingService.requireUserContextSecretInfo(agentIp, accountId));
    }

    @RequestMapping(value = "/secretRelations", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult secretRelations(HttpServletRequest request) {
        String type = request.getParameter("type");
        String dataSecretCode = request.getParameter("dataSecretCode");
        String funcId = request.getParameter("funcId");
        String relation = request.getParameter("relation");

        JSONTreeNode resultNode;
        if ("systemFunc".equals(type)) {
            resultNode = secretSettingService.requireSystemFuncSecretInfo(relation);
        } else if ("systemFlow".equals(type)) {
            resultNode = secretSettingService.requireSystemFlowSecretInfo(relation);
        } else if ("funcForm".equals(type)) {
            if (Strings.isNullOrEmpty(funcId)) {
                return BaseRespResult.errorResult("请确认功能和功能密级！");
            }
            resultNode = secretSettingService.requireFuncFormSecretInfo(funcId);
        } else if ("dataAttachment".equals(type)) {
            if (Strings.isNullOrEmpty(dataSecretCode)) {
                return BaseRespResult.errorResult("请确认数据密级！");
            }
            resultNode = secretSettingService.requireDataAttachmentSecretInfo(relation, dataSecretCode);
        } else {
            return BaseRespResult.errorResult("非法的参数！");
        }
        return BaseRespResult.successResult(resultNode);
    }


}
