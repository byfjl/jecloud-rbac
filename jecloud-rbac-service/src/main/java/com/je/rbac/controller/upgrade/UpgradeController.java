/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.upgrade;

import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.rbac.service.upgrade.UpgradeService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/rbac/upgrade")
public class UpgradeController implements CommonRequestService {

    @Autowired
    private UpgradeService upgradeService;

    @RequestMapping(value = {"/addPackageRoles"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult addPackageRoles(HttpServletRequest request) {
        String upgradeId = getStringParameter(request,"upgradeId");
        String roleIds = getStringParameter(request,"roleIds");

        if(Strings.isNullOrEmpty(upgradeId)){
            return BaseRespResult.errorResult("请先构建升级包！");
        }

        if(Strings.isNullOrEmpty(roleIds)){
            return BaseRespResult.errorResult("请选择要导入的角色！");
        }
        upgradeService.insertPackageRoles(upgradeId,roleIds);
        return BaseRespResult.successResult("导入成功！");
    }

    @RequestMapping(value = {"/addPackageMenus"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult addPackageWorkflows(HttpServletRequest request) {
        String upgradeId = getStringParameter(request,"upgradeId");
        String ids = getStringParameter(request,"ids");
        if(Strings.isNullOrEmpty(upgradeId)){
            return BaseRespResult.errorResult("请选择升级包操作！");
        }
        if(Strings.isNullOrEmpty(ids)){
            return BaseRespResult.errorResult("请选择流程！");
        }
        upgradeService.insertPackageMenus(upgradeId,ids);
        return BaseRespResult.successResult("添加成功！");
    }

    @RequestMapping(value = {"/addPackageTopMenus"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult addPackageTopMenus(HttpServletRequest request) {
        String upgradeId = getStringParameter(request,"upgradeId");
        String ids = getStringParameter(request,"ids");
        if(Strings.isNullOrEmpty(upgradeId)){
            return BaseRespResult.errorResult("请选择升级包操作！");
        }
        if(Strings.isNullOrEmpty(ids)){
            return BaseRespResult.errorResult("请选择流程！");
        }
        upgradeService.insertPackageTopMenus(upgradeId,ids);
        return BaseRespResult.successResult("添加成功！");
    }

}
