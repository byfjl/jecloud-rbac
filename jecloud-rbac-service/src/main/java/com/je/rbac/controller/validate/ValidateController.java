/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.validate;

import com.google.common.base.Strings;
import com.je.common.auth.token.AuthSession;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.spring.SpringContextHolder;
import org.apache.servicecomb.swagger.invocation.context.InvocationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/je/rbac/cloud/validate")
public class ValidateController extends AbstractPlatformController {

    @RequestMapping(value = {"/validateToken"}, method = RequestMethod.POST)
    public Boolean validateToken(InvocationContext invocationContext) {
        String authorization = invocationContext.getContext("authorization");
        //构建登录用户
        RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");
        StringRedisTemplate stringRedisTemplate = SpringContextHolder.getBean("stringRedisTemplate");
        String authId = stringRedisTemplate.opsForValue().get("authorization:token:" + authorization);
        if (Strings.isNullOrEmpty(authId)) {
            return false;
        }
        Object sessionObj = redisTemplate.opsForValue().get("authorization:session:" + authId);
        if (sessionObj == null) {
            return false;
        }
        AuthSession session = (AuthSession) sessionObj;
        if (!session.getDataMap().containsKey("account")) {
            return false;
        }
        return true;
    }

}
