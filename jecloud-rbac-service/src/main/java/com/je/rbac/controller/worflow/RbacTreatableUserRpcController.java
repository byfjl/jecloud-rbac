/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.controller.worflow;

import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.model.AssignmentPermission;
import com.je.rbac.rpc.TreatableUserRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程可处理人rpc
 */
@RestController
@RequestMapping(value = "/je/rbac/cloud/rpc/treatableUser")
public class RbacTreatableUserRpcController extends AbstractPlatformController {

    @Autowired
    private TreatableUserRpcService treatableUserRpcService;

    /**
     * 根据roleIds 加载角色人员树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/loadtRoleUserTree"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult loadtRoleUserTree(HttpServletRequest request) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        String roleIds = getStringParameter(request, "roleIds");
        String orgFlag = getStringParameter(request, "orgFlag");
        //----------人员权限过滤条件-----------------
        String company = getStringParameter(request, "company");
        String companySupervision = getStringParameter(request, "companySupervision");
        String dept = getStringParameter(request, "dept");
        String deptAll = getStringParameter(request, "deptAll");
        String directLeader = getStringParameter(request, "directLeader");
        String deptLeader = getStringParameter(request, "deptLeader");
        String supervisionLeader = getStringParameter(request, "supervisionLeader");
        JSONTreeNode rootNode;
        Boolean orgFlagVal = false;
        if ("true".equals(orgFlag)) {
            orgFlagVal = true;
        }
        AssignmentPermission perm = new AssignmentPermission();
        perm.setCompany(company == null ? null : true);
        perm.setCompanySupervision(companySupervision == null ? null : true);
        perm.setDept(dept == null ? null : true);
        perm.setDeptAll(deptAll == null ? null : true);
        perm.setDirectLeader(directLeader == null ? null : true);
        perm.setDeptLeader(deptLeader == null ? null : true);
        perm.setSupervisionLeader(supervisionLeader == null ? null : true);
        rootNode = treatableUserRpcService.findRoleUsersAndBuildTreeNode(userId, roleIds, perm, orgFlagVal, false,true);
        return BaseRespResult.successResult(rootNode);
    }

    /**
     * 根据deptIds 加载部门人员树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/loadtDeptUserTree"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult loadtDeptUserTree(HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptIds = getStringParameter(request, "deptIds");
        JSONTreeNode rootNode;
        String orgFlag = getStringParameter(request, "orgFlag");
        //----------人员权限过滤条件-----------------
        String company = getStringParameter(request, "company");
        String companySupervision = getStringParameter(request, "companySupervision");
        String dept = getStringParameter(request, "dept");
        String deptAll = getStringParameter(request, "deptAll");
        String directLeader = getStringParameter(request, "directLeader");
        String deptLeader = getStringParameter(request, "deptLeader");
        String supervisionLeader = getStringParameter(request, "supervisionLeader");

        Boolean orgFlagVal = false;
        if ("true".equals(orgFlag)) {
            orgFlagVal = true;
        }
        AssignmentPermission perm = new AssignmentPermission();
        perm.setCompany(company == null ? false : true);
        perm.setCompanySupervision(companySupervision == null ? false : true);
        perm.setDept(dept == null ? false : true);
        perm.setDeptAll(deptAll == null ? false : true);
        perm.setDirectLeader(directLeader == null ? false : true);
        perm.setDeptLeader(deptLeader == null ? false : true);
        perm.setSupervisionLeader(supervisionLeader == null ? false : true);
        rootNode = treatableUserRpcService.findDeptUsersAndBuildTreeNode(userId,
                deptIds, perm, orgFlagVal, false,true);
        return BaseRespResult.successResult(rootNode);
    }

    /**
     * 加载人员树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/loadUserTree"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult loadUserTree(HttpServletRequest request) {
        JSONTreeNode rootNode;
        String orgFlag = getStringParameter(request, "orgFlag");
        Boolean orgFlagVal = false;
        if ("true".equals(orgFlag)) {
            orgFlagVal = true;
        }
        List<String> ids = new ArrayList<>();
        ids.add("33ff53e1e64f491d855adebdc2bb0973");
        ids.add("09db58129197410f8d109f405238d9be");
        ids.add("6beb6ccc7fa645d2ad98030dec27cef9");
        ids.add("b6953aa9f1e045c9aba4f5643342d2c0");
        ids.add("024ae37105044073b8665ae7adc8ee37");
        ids.add("e0c50371df6f4aeaae6226985efcfc10");
        ids.add("9b37266821be4e8783f306b7611dbad4");
        rootNode = treatableUserRpcService.findUsersAndBuildTreeNodeByAccountIds(ids, orgFlagVal, false,true);
        return BaseRespResult.successResult(rootNode);
    }

    /**
     * 加载组织架构人员树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/loadtDepartmentUserTree"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult loadtDepartmentUserTree(HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        String deptId = getStringParameter(request, "deptId");
        //----------人员权限过滤条件-----------------
        String company = getStringParameter(request, "company");
        String companySupervision = getStringParameter(request, "companySupervision");
        String dept = getStringParameter(request, "dept");
        String deptAll = getStringParameter(request, "deptAll");
        String directLeader = getStringParameter(request, "directLeader");
        String deptLeader = getStringParameter(request, "deptLeader");
        String supervisionLeader = getStringParameter(request, "supervisionLeader");

        AssignmentPermission perm = new AssignmentPermission();
        perm.setCompany(company == null ? null : true);
        perm.setCompanySupervision(companySupervision == null ? null : true);
        perm.setDept(dept == null ? null : true);
        perm.setDeptAll(deptAll == null ? null : true);
        perm.setDirectLeader(directLeader == null ? null : true);
        perm.setDeptLeader(deptLeader == null ? null : true);
        perm.setSupervisionLeader(supervisionLeader == null ? null : true);

        JSONTreeNode rootNode = treatableUserRpcService.findOrgStructureAndBuildTreeNode(userId, perm, false,true);
        return BaseRespResult.successResult(rootNode);
    }

    /**
     * 特殊处理接口
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/loadSpecialProcessingUserTree"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadSpecialProcessingUserTree(HttpServletRequest request) {
        String userId = getStringParameter(request, "userId");
        List<String> types = new ArrayList<>();
        types.add("LOGINED_USER");
        types.add("DIRECT_LEADER");
        types.add("DEPT_LEADER");
        types.add("DEPT_MONITOR_LEADER");
        types.add("STARTER_USER");
        types.add("TASK_ASSGINE");
        types.add("TASK_ASSGINE_HEAD");
        types.add("PREV_ASSIGN_USER");
        types.add("PREV_ASSIGN_USER_DIRECT_LEADER");
        types.add("DEPT_USERS");
        types.add("DEPT_ALL_USERS");
        types.add("DEPT_MONITOR_USERS");
        types.add("COMPANY_MONITOR_LEADERS");
//        types.add("DIRECT_LEADER");
//        types.add("LOGINED_USER");
        JSONTreeNode rootNode = treatableUserRpcService.findUserToSpecialProcessing(types, userId, null, null, "33ff53e1e64f491d855adebdc2bb0973",false,true);
        return BaseRespResult.successResult(rootNode);
    }
}
