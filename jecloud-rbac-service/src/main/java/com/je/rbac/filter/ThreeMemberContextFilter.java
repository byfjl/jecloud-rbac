/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.filter;

import com.je.common.base.context.GlobalExtendedContext;
import com.je.common.base.spring.SpringContextHolder;
import com.je.rbac.cache.ThreeMemberSettingCache;
import com.je.servicecomb.filter.AbstractHttpServerFilter;
import org.apache.servicecomb.common.rest.filter.HttpServerFilter;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;
import java.util.Map;

public class ThreeMemberContextFilter extends AbstractHttpServerFilter implements HttpServerFilter {

    private static final String[] keys = new String[]{
            "switch",
            "auditManagerLogSwitch",
            "systemManagerLogSwitch",
            "securityManagerLogSwitch",
            "auditRole",
            "securityRole",
            "systemRole"
    };

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {
        ThreeMemberSettingCache threeMemberSettingCache = SpringContextHolder.getBean(ThreeMemberSettingCache.class);
        Map<String,String> values = threeMemberSettingCache.getCacheValues();
        if(values == null || values.isEmpty()){
            GlobalExtendedContext.removeContext(keys);
        }else {
            GlobalExtendedContext.putAll(values);
        }
        return null;
    }

}
