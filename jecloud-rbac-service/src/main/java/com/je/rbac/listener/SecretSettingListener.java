/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.listener;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.secret.SecretSettingService;
import org.apache.servicecomb.core.BootListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SecretSettingListener implements BootListener {

    private static final Logger logger = LoggerFactory.getLogger(SecretSettingListener.class);
    private static boolean flag = false;

    @Override
    public int getOrder() {
        return 100002;
    }

    @Override
    public void onAfterRegistry(BootEvent event) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!flag) {
                    try {
                        MetaService metaService = SpringContextHolder.getBean(MetaService.class);
                        SecretSettingService secretSettingService = SpringContextHolder.getBean(SecretSettingService.class);
                        List<DynaBean> beanList = metaService.select("JE_RBAC_SYSSECRET", ConditionsWrapper.builder().orderByDesc("SY_CREATETIME"));
                        if (beanList == null || beanList.isEmpty()) {
                            return;
                        }
                        DynaBean settingBean = beanList.get(0);
                        secretSettingService.initSecretContext(settingBean);
                        flag = true;
                        logger.info("Init the secret context success!");
                    } catch (Throwable e) {
                        logger.error("Init the secret context failure,will retry in 2 seconds! the expression is {}", e.getMessage());
                        try {
                            Thread.sleep(1000 * 3);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                logger.info("The secret context init process success, will be finished!");
            }
        }).start();
    }

}
