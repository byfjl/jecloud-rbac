/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.company.RbacDepartmentUserService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@RpcSchema(schemaId = "departmentUserRpcService")
public class DepartmentUserRpcServiceImpl implements DepartmentUserRpcService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private RbacDepartmentUserService rbacDepartmentUserService;

    @Override
    public String findDirectLeaderId(String departmentId, String userId) {
        return rbacDepartmentUserService.findDirectLeader(departmentId, userId);
    }

    @Override
    public List<String> findDirectUserIds(String departmentId, String userId) {
        List<DynaBean> directUserBeanList = metaService.select("JE_RBAC_DEPTUSER", ConditionsWrapper.builder()
                .eq("DEPTUSER_DIRECTLEADER_ID", userId));
        if (directUserBeanList == null || directUserBeanList.isEmpty()) {
            return null;
        }
        List<String> idList = new ArrayList<>();
        directUserBeanList.forEach(each -> {
            idList.add(each.getStr("JE_RBAC_USER_ID"));
        });
        return idList;
    }

    @Override
    public List<String> findDeptLeaderIds(String departmentId) {
        DynaBean departmentBean = metaService.selectOneByPk("JE_RBAC_DEPARTMENT", departmentId);
        if (departmentBean == null) {
            return null;
        }
        String majorIdStr = departmentBean.getStr("DEPARTMENT_MAJOR_ID");
        if (Strings.isNullOrEmpty(majorIdStr)) {
            return null;
        }
        return Splitter.on(",").splitToList(majorIdStr);
    }

    @Override
    public List<String> findCompanyLeaderIds(String companyId) {
        DynaBean companyBean = metaService.selectOneByPk("JE_RBAC_COMPANY", companyId);
        if (companyBean == null) {
            return null;
        }
        String majorIdStr = companyBean.getStr("COMPANY_MAJOR_ID");
        if (Strings.isNullOrEmpty(majorIdStr)) {
            return null;
        }
        return Splitter.on(",").splitToList(majorIdStr);
    }

    @Override
    public List<String> findMonitorDeptIds(String departmentId, String userId) {
        DynaBean userBean = metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder()
                .eq("JE_RBAC_USER_ID", userId));
        if (userBean == null) {
            return null;
        }
        if(Strings.isNullOrEmpty(userBean.getStr("USER_MONITORDEPT_ID"))){
            return null;
        }
        return Splitter.on(",").splitToList(userBean.getStr("USER_MONITORDEPT_ID"));
    }

    @Override
    public List<String> findDeptMonitorDeptIds(String departmentId) {
        DynaBean deptBean = metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .eq("JE_RBAC_DEPARTMENT_ID", departmentId));
        if (deptBean == null) {
            return null;
        }

        if(Strings.isNullOrEmpty(deptBean.getStr("DEPARTMENT_MONITORDEPT_ID"))){
            return null;
        }
        return Splitter.on(",").splitToList(deptBean.getStr("DEPARTMENT_MONITORDEPT_ID"));
    }

    @Override
    public List<String> findCompanyMonitorCompanyIds(String companyId) {
        DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder()
                .eq("JE_RBAC_COMPANY_ID", companyId));
        if (companyBean == null) {
            return null;
        }
        if(Strings.isNullOrEmpty(companyBean.getStr("COMPANY_JGCOMPANY_ID"))){
            return null;
        }
        return Splitter.on(",").splitToList(companyBean.getStr("COMPANY_JGCOMPANY_ID"));
    }

}
