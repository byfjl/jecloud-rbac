/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.syncUser.user;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import com.je.rbac.exception.AccountException;
import com.je.rbac.rpc.AccountRpcService;
import com.je.rbac.rpc.SyncRpcLoginRpcService;
import com.je.rbac.service.account.RbacLoginService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RpcSchema(schemaId = "syncRpcLoginRpcService")
public class SyncRpcLoginRpcServiceImpl implements SyncRpcLoginRpcService {

    @Autowired
    private AccountRpcService accountRpcService;
    @Autowired
    private RbacLoginService rbacLoginService;
    @Autowired
    private MetaService metaService;

    @Override
    public BaseRespResult rpcLogin(String account,String tenantId, String type) {
        try {
            if (Strings.isNullOrEmpty(account)) {
                return BaseRespResult.errorResult("用户名不能为空！");
            }
            if (Strings.isNullOrEmpty(type)) {
                return BaseRespResult.errorResult("用户类型不能为空！");
            }
            String loginType = getLoginType(type);
            if (loginType == null) {
                return BaseRespResult.errorResult("用户类型格式不正确，请检查！");
            }
            DynaBean accountBean = accountRpcService.findLoginAccountByType(type, account);
            if (accountBean == null) {
                return BaseRespResult.errorResult("账号或密码错误，请重新登录！");
            }

//            DynaBean tenantRelationBean = null;
//            if (!Strings.isNullOrEmpty(tenantId)) {
//                tenantRelationBean = tenantRelationRpcService.findTenantRelation(accountBean.getStr("JE_RBAC_ACCOUNT_ID"), tenantId);
//                if (tenantRelationBean == null) {
//                    return BaseRespResult.errorResult("租户或账号不存在！");
//                }
//            }

            String userId = accountBean.getStr("USER_ASSOCIATION_ID");
            List<DynaBean> users = metaService.selectBeanSql("JE_RBAC_DEPTUSER", " AND JE_RBAC_USER_ID='" + userId + "'");
            String deptId = "";
            if (users != null && users.size() > 0) {
                deptId = users.get(0).getStr("JE_RBAC_DEPARTMENT_ID");
            }
            String tokenId = rbacLoginService.login(tenantId,deptId, accountBean, "default-device", "1");
            return BaseRespResult.successResult(tokenId, "操作成功！");
        } catch (AccountException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
//        return BaseRespResult.errorResult( "操作失败！");
    }

    public static String getLoginType(String type) {
        if (type.contains("PHONE")) {
            return "ACCOUNT_PHONE";
        }
        if (type.contains("USERNAME")) {
            return "ACCOUNT_NAME";
        }
        if (type.contains("ACCOUNT")) {
            return "ACCOUNT_CODE";
        }
        if (type.contains("EMAIL")) {
            return "ACCOUNT_MAIL";
        }
        return null;
    }
}
