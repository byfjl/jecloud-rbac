/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user;

import com.je.common.base.DynaBean;
import com.je.common.base.util.SecurityUserHolder;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.rpc.workflow.user.special.SpecialUserParserEnum;

import java.util.List;

/**
 * 公司、部门、人员树
 */
public class ParserCompanyJsonTreeFilter extends FilterChain implements Filter {

    @Override
    public void handler(ParserUserBo parserUserBo, JSONTreeNode jsonTreeNode, FilterChain filterChain) {
        List<String> types = parserUserBo.getTypes();
        if (types != null && types.size() > 0) {
            //包含所在子公司  则渲染公司部门人员树
            if (!types.contains(SpecialUserParserEnum.SUBSIDIARY.toString())) {
                filterChain.doFilter(parserUserBo, jsonTreeNode, filterChain);
                return;
            }
            //根据人员参照 人员id 构建账号部门视图信息
            DynaBean userDeptBean = findVacCountDeptUserById(parserUserBo.getUserId());
            //构建公司、部门、用户树
            buildCompanyDepartmentUserTreeAddOwn(parserUserBo.getMultiple(), parserUserBo.getAddOwn(), SecurityUserHolder.getCurrentAccount().getDeptId(), userDeptBean.get("SY_COMPANY_ID").toString());

            //去除重复选项
            removeOtherOption(parserUserBo);
            //此时的jsonTreeNode是公司部门树
            parserUserBo.setCompanyDeptUserTree(true);
            filterChain.doFilter(parserUserBo, jsonTreeNode, filterChain);
        }
    }

    /**
     * 去除重复人选选择项
     *
     * @param parserUserBo
     */
    public void removeOtherOption(ParserUserBo parserUserBo) {
        //直属领导可能不是一个子公司的
        //当前登录人跟人员参照是同一个人才可能看到提交的按钮 所以 当前登录人去除
        //部门领导可能不是一个子公司的
        //监管领导可能不是一个子公司的
        //子公司
        parserUserBo.getTypes().remove(SpecialUserParserEnum.SUBSIDIARY.toString());
        //当前登录人
        parserUserBo.getTypes().remove(SpecialUserParserEnum.LOGINED_USER.toString());
        //本部门内
        parserUserBo.getTypes().remove(SpecialUserParserEnum.DEPT_USERS.toString());
        //本部门内含子部门
        parserUserBo.getTypes().remove(SpecialUserParserEnum.DEPT_ALL_USERS.toString());
    }
}
