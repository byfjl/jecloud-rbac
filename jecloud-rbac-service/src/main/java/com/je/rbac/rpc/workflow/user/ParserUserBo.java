/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.user;

import java.util.List;

public class ParserUserBo {

    /**
     * 特殊处理的选择项
     */
    List<String> types;
    /**
     * 当前参照用户id
     */
    private String userId;
    /**
     * 任务指派人id
     */
    private String taskAssigner;
    /**
     * 前置任务指派人id
     */
    private String frontTaskAssigner;
    /**
     * 流程启动人
     */
    private String starterUser;
    /**
     * 是否多选
     */
    private Boolean multiple;

    /**
     * 默认添加自己
     */
    private Boolean addOwn = true;
    /**
     * 公司、部门、人员树
     */
    private Boolean isCompanyDeptUserTree = false;
    /**
     * 公司、人员树
     */
    private Boolean isCompanyUserTree = false;
    /**
     * 部门、人员树
     */
    private Boolean isDeptUserTree = false;
    /**
     * 人员树
     */
    private Boolean isUserTree = false;

    public static ParserUserBo build() {
        return new ParserUserBo();
    }

    public static ParserUserBo build(List<String> types, String userId, String taskAssigner, String frontTaskAssigner,
                                     String starterUser, Boolean multiple, Boolean addOwn) {
        ParserUserBo bo = new ParserUserBo();
        bo.setTypes(types).setUserId(userId).setTaskAssigner(taskAssigner).setFrontTaskAssigner(frontTaskAssigner)
                .setStarterUser(starterUser).setMultiple(multiple).setAddOwn(addOwn);
        return bo;
    }

    public Boolean getMultiple() {
        return multiple;
    }

    public ParserUserBo setMultiple(Boolean multiple) {
        this.multiple = multiple;
        return this;
    }


    public String getUserId() {
        return userId;
    }

    public ParserUserBo setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getTaskAssigner() {
        return taskAssigner;
    }

    public ParserUserBo setTaskAssigner(String taskAssigner) {
        this.taskAssigner = taskAssigner;
        return this;
    }

    public String getFrontTaskAssigner() {
        return frontTaskAssigner;
    }

    public ParserUserBo setFrontTaskAssigner(String frontTaskAssigner) {
        this.frontTaskAssigner = frontTaskAssigner;
        return this;
    }

    public Boolean getAddOwn() {
        return addOwn;
    }

    public ParserUserBo setAddOwn(Boolean addOwn) {
        this.addOwn = addOwn;
        return this;
    }

    public String getStarterUser() {
        return starterUser;
    }

    public ParserUserBo setStarterUser(String starterUser) {
        this.starterUser = starterUser;
        return this;
    }

    public List<String> getTypes() {
        return types;
    }

    public ParserUserBo setTypes(List<String> types) {
        this.types = types;
        return this;
    }

    public Boolean getCompanyDeptUserTree() {
        return isCompanyDeptUserTree;
    }

    public void setCompanyDeptUserTree(Boolean companyDeptUserTree) {
        isCompanyDeptUserTree = companyDeptUserTree;
    }

    public Boolean getCompanyUserTree() {
        return isCompanyUserTree;
    }

    public void setCompanyUserTree(Boolean companyUserTree) {
        isCompanyUserTree = companyUserTree;
    }

    public Boolean getDeptUserTree() {
        return isDeptUserTree;
    }

    public void setDeptUserTree(Boolean deptUserTree) {
        isDeptUserTree = deptUserTree;
    }

    public Boolean getUserTree() {
        return isUserTree;
    }

    public void setUserTree(Boolean userTree) {
        isUserTree = userTree;
    }
}
