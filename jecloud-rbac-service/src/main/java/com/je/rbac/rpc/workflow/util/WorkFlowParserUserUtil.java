/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.rpc.workflow.util;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.service.IconType;

public class WorkFlowParserUserUtil {

    /**
     * 解析部门tree
     *
     * @param resultJSONTreeNode
     * @param user
     * @return
     */
    public static JSONTreeNode buildDepartmentTree(JSONTreeNode resultJSONTreeNode, DynaBean user) {
        JSONTreeNode deptTree = new JSONTreeNode();
        deptTree.setId(user.getStr("ACCOUNTDEPT_DEPT_ID"));
        deptTree.setCode(user.getStr("ACCOUNTDEPT_DEPT_ID"));
        deptTree.setText(user.getStr("ACCOUNTDEPT_DEPT_NAME"));
        deptTree.setNodeType("LEAF");
        deptTree.setNodeInfo(user.get("ACCOUNTDEPT_DEPT_ID").toString());
        deptTree.setNodeInfoType("BM");
        deptTree.setIcon(IconType.TCONTYPE_DEPART.getVal());
        deptTree.setParent(resultJSONTreeNode.getId());
        deptTree.setChecked(false);
        deptTree.setLayer(resultJSONTreeNode.getLayer() + 1);
        resultJSONTreeNode.getChildren().add(deptTree);
        return deptTree;
    }

    /**
     * 解析部门账号信息为tree
     *
     * @param resultJSONTreeNode
     * @param user
     * @param multiple
     */
    public static void parseAccountDepartmentToTree(JSONTreeNode resultJSONTreeNode, DynaBean user, Boolean multiple, Boolean disFlag) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(user.getStr("JE_RBAC_ACCOUNTDEPT_ID"));
        node.setCode(user.getStr("ACCOUNT_CODE"));
        //排除部门，组织架构；其余调用均添加部门标示
        if (disFlag) {
            node.setText(user.getStr("ACCOUNT_NAME") + "(" + user.getStr("ACCOUNTDEPT_DEPT_NAME") + ")");
        } else {
            node.setText(user.getStr("ACCOUNT_NAME"));
        }
        node.setNodeType("LEAF");
        JSONObject nodeInfo = new JSONObject();
        nodeInfo.put("sex", user.getStr("ACCOUNT_SEX"));
        nodeInfo.put("ACCOUNT_NAME", user.getStr("ACCOUNT_NAME"));
        node.setNodeInfo(nodeInfo.toJSONString());
        node.setNodeInfoType("json");
        node.setIcon(IconType.TCONTYPE_ROLE.getVal());
        node.setParent(resultJSONTreeNode.getId());
        node.setChecked(multiple);
        node.setLayer(resultJSONTreeNode.getLayer() + 1);
        node.setBean(user.getValues());
        resultJSONTreeNode.getChildren().add(node);
    }

    /**
     * 解析部门账号信息为tree
     *
     * @param resultJSONTreeNode
     * @param user
     */
    public static void parseUserDepartmentToTree(JSONTreeNode resultJSONTreeNode, DynaBean user) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(user.getStr("JE_RBAC_DEPTUSER_ID"));
        node.setCode(user.getStr("USER_CODE"));
        node.setText(user.getStr("USER_NAME"));
        node.setNodeType("LEAF");
        node.setNodeInfoType("user");
        node.setIcon(IconType.TCONTYPE_ROLE.getVal());
        node.setParent(resultJSONTreeNode.getId());
        node.setLayer(resultJSONTreeNode.getLayer() + 1);
        node.setBean(user.getValues());
        resultJSONTreeNode.getChildren().add(node);
    }
}
