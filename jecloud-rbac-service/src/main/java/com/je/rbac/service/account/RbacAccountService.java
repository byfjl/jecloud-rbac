/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.account;

import com.je.common.auth.impl.PlatformOrganization;
import com.je.common.auth.impl.RealOrganizationUser;
import com.je.common.auth.impl.account.Account;
import com.je.common.auth.impl.role.Role;
import com.je.common.base.DynaBean;
import com.je.common.base.result.BaseRespResult;
import com.je.rbac.exception.AccountException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 账号服务定义
 */
public interface RbacAccountService {

    /**
     * 根据机构ID构建机构
     * @param orgId
     * @return
     */
    PlatformOrganization findOrganizationById(String orgId);


    /***
     * 账号校验人员状态
     * @param accountIds
     * @return
     * @throws AccountException
     */
    boolean checkUserStatusByAccountId(String accountIds,int accountIdSize) throws AccountException;

    /***
     * 校验账号表-账号编码,手机号，邮箱 是否重复
     * @param userId
     * @throws AccountException
     */
    void checkAccountUnique(String userId,int size) throws AccountException;

    void checkAccountCodeUnique(String accountId,String accountCode) throws AccountException;

    void checkAccountPhoneUnique(String accountId,String accountPhone) throws AccountException;

    void checkAccountEmailUnique(String accountId,String accountEmail) throws AccountException;


    /***
     * 账号登录校验所属机构是否是公司员工
     * @param accountId
     * @return
     * @throws AccountException
     */
    boolean checkOrgTypeByAccountId(String accountId) throws AccountException;

    /**
     * 删除
     *
     * @return
     */
    int doRemove(String accountIds);

    /***
     * 账号数据同步
     * @throws AccountException
     */
    void dataSync() throws AccountException;

    /***
     * 账号数据保存
     * @param request
     * @throws AccountException
     */
    void dataSave(HttpServletRequest request) throws AccountException;

    /***
     * 插入账号部门
     * @param accountId
     * @param departmentId
     * @param status
     * @throws AccountException
     */
    void insertAccountDept(String accountId,String departmentId,String status) throws AccountException;

    /***
     * 修改用户账号头像
     * @param accountId
     * @throws AccountException
     */
    void updateUserAvatar(String accountId,String avatar) throws AccountException;

    /**
     * 根据ID查找账号模型
     *
     * @param id 账户ID
     * @return
     */
    Account buildAccountModelById(String id, RealOrganizationUser realUser, List<Role> roles, Set<String> permissions) throws AccountException;

    /**
     * 根据账户类型和账户查找账户
     * @param account
     * @param realUser
     * @param roles
     * @param permissions
     * @return
     */
    Account buildAccountModelByType(String type,String account, RealOrganizationUser realUser, List<Role> roles, Set<String> permissions) throws AccountException;

    /**
     * 构建账号model
     *
     * @param accountBean 账户Bean
     * @param realUser    真实用户
     * @param roles       真实人员角色
     * @param permissions 权限集合
     * @return
     */
    Account buildAccountModel(DynaBean accountBean, RealOrganizationUser realUser, List<Role> roles, Set<String> permissions) throws AccountException;

    /**
     * 根据租户ID查找
     *
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 账号恢复默认密码
     * @param accountIds
     */
    void recoverPassword(String accountIds) throws AccountException;

    /**
     * 获取在线用户
     * @return
     */
    List<Map<String,Object>> requireOnlineUsers(String keyword, int start, int limit);

    /**
     * 校验密码
     * @param password
     * @return
     */
    String validPassword(String password);

    /**
     * 校验复杂策略
     * @param password
     * @param num
     * @return
     */
    boolean validComplex(String password, String num);

    /**
     * 校验中等策略
     * @param password
     * @param num
     * @return
     */
    boolean validMedium(String password, String num);

    /**
     * 校验简单策略
     * @param password
     * @param num
     * @return
     */
    boolean validSimple(String password, String num);

    /**
     * 查找账户角色
     * @param tenantId
     * @param accountId
     * @return
     */
    List<String> findAccountRoles(String tenantId,String accountId);

    /**
     * 锁定账号
     *
     * @param accountIds
     * @param disableTime 锁定时间
     */
    void lockAccount(String accountIds, long disableTime);

    /**
     * 解除账号
     *
     * @param accountIds
     */
    void unlockAccount(String accountIds);

    /**
     * 禁用账号
     *
     * @param accountIds 账户ID列表
     */
    void disableAccount(String accountIds) throws AccountException;

    /**
     * 启用账号
     *
     * @param accountIds
     */
    void enableAccount(String accountIds) throws AccountException;



    /***
     * 登录人次统计（按日，按照每日登录算做一次）
     * @param year
     * @param month
     * @return
     * @throws AccountException
     */
    List<Map<String,Object>> queryLoginStatistic(String year,String month) throws AccountException;


    /***
     * 组织信息统计人数（按月1-12月）
     * @param year
     * @return
     * @throws AccountException
     */
    Map<String, Object> staffStatistic(String year) throws AccountException;

    /**
     * 账户管理列表保存
     * @param obj
     * @return
     */
    void doSaveList(Object obj);

    /**
     * 根据角色id修改角色名称
     * @param roleId
     * @param roleName
     */
    void updateRoleName(String roleId,String roleName);

}
