/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company;

public enum OrgUserType {
    LEVEL_CODE_GROUPCOMPANY("GROUP_COMPANY","集团公司"),
    LEVEL_CODE_COMPANY("COMPANY","公司"),

    LEVEL_CODE_DEPT("DEPT","部门"),
    LEVEL_CODE_WORKSHOP("WORKSHOP","车间"),
    LEVEL_CODE_TEAM("TEAM","班组"),

    MAN("MAN","男"),
    WOMAN("WOMAN","女"),

    MAINDEPT_TRUE("0","分"),
    MAINDEPT_FALSE("1","主"),

    YESORNO_TRUE("1","是"),
    YESORNO_FALSE("0","否"),

    ROLE_TYPE_ROLE("role","角色"),
    ROLE_TYPE_FOLDER("folder","文件夹");


    // 成员变量
    private String code;
    private String name;
    // 构造方法
    private OrgUserType(String code, String name) {
        this.code = code;
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static String getCode(String code) {
        for (OrgUserType m : OrgUserType.values()) {
            if (m.getCode().equals(code) ) {
                return m.code;
            }
        }
        return "";
    }

    public static String getCodeName(String code) {
        for (OrgUserType m : OrgUserType.values()) {
            if (m.getCode().equals(code)) {
                return m.name;
            }
        }
        return "";
    }

    public static void main(String[] args) {
        System.out.println(getCode("GROUP_COMPANY"));
    }
}
