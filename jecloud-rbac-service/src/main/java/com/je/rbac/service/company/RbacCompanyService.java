/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company;

import com.je.common.auth.impl.Company;
import com.je.common.base.DynaBean;
import com.je.rbac.exception.CompanyException;

import java.util.List;

public interface RbacCompanyService {


    /***
     * 保存
     * @param dynaBean
     */
    DynaBean doSave(DynaBean dynaBean) throws CompanyException;

    /**
     * 检查公司名称是否唯一
     * @param dynaBean
     * @return
     */
    boolean checkCompanyNameUnique(DynaBean dynaBean);

    /**
     * 检查公司编码是否唯一
     * @param dynaBean
     * @return
     */
    boolean checkCompanyCodeUnique(DynaBean dynaBean);


    /***
     * 修改校验公司层级
     * @param dynaBean
     * @throws CompanyException
     */
     void checkCompanyLevelCode(DynaBean dynaBean) throws CompanyException;

    /***
     * 修改公司名称
     * @param dynaBean
     * @throws CompanyException
     */
    void updateCompanyName(DynaBean dynaBean) throws CompanyException;

    /***
     * 修改公司图标
     * @param dynaBean
     * @throws CompanyException
     */
    void updateCompanyIcon(DynaBean dynaBean) throws CompanyException;

    /**
     * 修改时检查是否唯一
     * @param companyBean
     * @return
     */
    boolean updateCheckUnique(DynaBean companyBean);

    /**
     * 判断上级公司状态
     * @param companyIds
     * @return
     */
    Boolean findGroupCompanyStatus(String companyIds) throws CompanyException;


    /**
     * 检查公司下是否有关联部门
     * @param companyIds
     * @return
     */
    boolean checkChildCompany(String companyIds);
    /**
     * 检查公司下是否有关联部门
     * @param companyIds
     * @return
     */
    boolean checkChildDept(String companyIds);

    /**
     * 根据id查找Model
     * @param id
     * @return
     */
    Company findCompanyModelById(String id);

    /**
     * 根据Code查找Model
     * @param code
     * @return
     */
    Company findCompanyModelByCode(String code);

    /**
     * 根据Code查找Model
     * @param name
     * @return
     */
    Company findCompanyModelByName(String name);

    /**
     * 根据Code查找Model
     * @param id
     * @return
     */
    Company findCompanyModelByParent(String id);

    /**
     * 根据Code查找Model
     * @param id
     * @return
     */
    Company findCompanyModelAncestors(String id);

    /**
     * 根据租户ID查找
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户Name查找
     * @param tenantName
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

}
