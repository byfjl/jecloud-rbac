/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company;

import com.je.common.auth.impl.Department;
import com.je.common.auth.impl.DepartmentUser;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.exception.AccountException;
import com.je.rbac.exception.DepartmentUserException;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 部门人员服务定义
 */
public interface RbacDepartmentUserService {

    /***
     * 列表查询拼接参数
     * @param param
     * @return
     */
    void loadParam(BaseMethodArgument param);

    /***
     * 检查用户编码是否唯一
     * @param orgBean
     * @return
     */
    boolean checkUserCodeUnique(DynaBean orgBean);

    /***
     * 检查用户编码是否唯一
     * @param orgBean
     * @return
     */
    boolean checkUserCodeUnique(DynaBean orgBean, List<DynaBean> rbacUserList);

    /***
     * 检查用户编码是否唯一
     * @param orgBean
     * @return
     */
    boolean checkUserPhoneUnique(DynaBean orgBean);

    /***
     * 检查用户编码是否唯一
     * @param orgBean
     * @return
     */
    boolean checkUserMailUnique(DynaBean orgBean);

    /**
     * 保存
     * @param viewDeptUserBean
     * @return
     */
    DynaBean doSave(DynaBean viewDeptUserBean) throws DepartmentUserException;

    /**
     * 更新
     * @param viewDeptUserBean
     * @return
     */
    DynaBean doUpdate(DynaBean viewDeptUserBean) throws DepartmentUserException, AccountException;

    /**
     * 移除
     * @param beanList
     * @return
     */
    void doRemove(List<DynaBean> beanList);

    /**
     * 移除
     * @param userDeptIds
     * @return
     */
    void doUserRemove(String userDeptIds);


    /***
     * 移除部门用户
     * @param mainDepartUserMapList
     * @param otherDepartUserMapList
     */
    void removeUsers(List<Map<String, String>> mainDepartUserMapList, List<Map<String, String>> otherDepartUserMapList);


    /**
     * 禁用
     * @param deptUserList
     * @return
     */
    void doDisableUser(List<DynaBean> deptUserList);


    /**
     * 查找部门model
     * @param id
     * @return
     */
    Department findDepartmentModel(String id);

    /**
     * 根据ID查找
     * @param departmentId 部门ID
     * @param userId 人员ID
     * @return
     */
    DynaBean findDepartmentUserById(String departmentId, String userId);

    /**
     * 查找部门
     * @param departmentId
     * @param userId
     * @return
     */
    default DepartmentUser findDepartmentUserModelById(String departmentId, String userId) {
        Department department = findDepartmentModel(departmentId);
        DynaBean userBean = findDepartmentUserById(departmentId, userId);
        DepartmentUser departmentUser = new DepartmentUser(department);
        departmentUser.parse(userBean.getValues());
        return departmentUser;
    }

    /**
     * 根据Code查找
     * @param departmentId 部门ID
     * @param userCode 人员编码
     * @return
     */
    DynaBean findDepartmentUserByCode(String departmentId, String userCode);

    /**
     * 查找部门
     * @param departmentId 部门ID
     * @param userCode 人员编码
     * @return
     */
    default DepartmentUser findDepartmentUserModelByCode(String departmentId, String userCode) {
        Department department = findDepartmentModel(departmentId);
        DynaBean userBean = findDepartmentUserByCode(departmentId, userCode);
        DepartmentUser departmentUser = new DepartmentUser(department);
        departmentUser.parse(userBean.getValues());
        return departmentUser;
    }

    /**
     * 根据Name查找
     * @param departmentId 部门ID
     * @param userName 人员名称
     * @return
     */
    DynaBean findDepartmentUserByName(String departmentId, String userName);

    /**
     * 查找部门
     * @param departmentId 部门ID
     * @param userName 人员名称
     * @return
     */
    default DepartmentUser findDepartmentUserModelByName(String departmentId, String userName) {
        Department department = findDepartmentModel(departmentId);
        DynaBean userBean = findDepartmentUserByName(departmentId, userName);
        DepartmentUser departmentUser = new DepartmentUser(department);
        departmentUser.parse(userBean.getValues());
        return departmentUser;
    }

    /**
     * 是否是主管
     * @param departmentId 部门ID
     * @param userId 人员ID
     * @return
     */
    default boolean isDeparmentMajor(String departmentId, String userId) {
        DynaBean userBean = findDepartmentUserById(departmentId, userId);
        return "1".equals(userBean.getStr("DEPTUSER_SFZG_CODE"));
    }

    /**
     * 查找部门主管
     * @param departmentId 部门id
     * @param userId 人员ID
     * @return
     */
    DynaBean findDepartmentMajor(String departmentId, String userId);

    /**
     * 查找人员主管
     * @param departmentId 部门ID
     * @param userId 人员ID
     * @return
     */
    default DepartmentUser findDepartmentMajorModel(String departmentId, String userId) {
        Department department = findDepartmentModel(departmentId);
        DynaBean userBean = findDepartmentMajor(departmentId, userId);
        DepartmentUser departmentUser = new DepartmentUser(department);
        departmentUser.parse(userBean.getValues());
        return departmentUser;
    }

    /**
     * 查找直接领导
     * @param departmentId 部门ID
     * @param userId 人员ID
     * @return
     */
    DynaBean findDepartmentDirectLeader(String departmentId, String userId);

    /**
     * 查找人员直接领导
     * @param departmentId 部门ID
     * @param userId 人员ID
     * @return
     */
    default DepartmentUser findDepartmentDirectLeaderModel(String departmentId, String userId) {
        Department department = findDepartmentModel(departmentId);
        DynaBean userBean = findDepartmentDirectLeader(departmentId, userId);
        DepartmentUser departmentUser = new DepartmentUser(department);
        departmentUser.parse(userBean.getValues());
        return departmentUser;
    }

    /**
     * 查找公司人员
     * @param companyId
     * @return
     */
    List<DynaBean> findCompanyUsers(String companyId);

    /**
     * 查找部门人员
     * @param departmentId
     * @return
     */
    List<DynaBean> findDepartmentUsers(String departmentId);

    /**
     * 根据租户ID查找
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户NAME查找
     * @param tenantName
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

    /**
     * 禁用部门人员关系
     * @param userDeptIds 用户部门集合
     */
    void disableUsers(String userDeptIds);

    /**
     * 启用部门人员关系
     * @param departmentId 部门ID
     * @param userIds 用户ID集合
     */
    void enableUsers(String departmentId, String userIds);

    /**
     * 变更部门人员关系
     * @param userDeptIds 用户部门集合
     */
    void changeDepartment(String userDeptIds);

    /**
     * 开通账号
     * @param userDeptIds 用户部门集合
     */
    void openAccount(String userDeptIds) throws AccountException;


    /**
     * 根据userId查找用户主部门
     * @param userId
     * @return
     */
    Boolean checkMainDeptStatusById(String userId);


    /**
     * 操作在职
     * @param userIds
     */
    void operationOnTheJob(String userIds);

    /**
     * 操作离职
     * @param userIds
     */
    void operationQuitJob(String userIds);

    /***
     * 加载账号权限树
     * @param userIds
     * @return
     * @throws DepartmentUserException
     */
    JSONTreeNode loadAccountPermissionTree(String userIds) throws DepartmentUserException;

    /***
     * 人员年龄计算
     * @throws DepartmentUserException
     */
    void userAgeCalculation() throws DepartmentUserException;

    /***
     * 重置人员账号密码
     * @param userIds
     * @return
     * @throws AccountException
     * @throws DepartmentUserException
     */
    long userResetPassword(String userIds) throws AccountException, DepartmentUserException;


    /***
     * 解除人员账号锁定
     * @param userIds
     * @return
     * @throws DepartmentUserException
     */
    long userUnlock(String userIds) throws DepartmentUserException;

    /**
     * 禁用人员账号
     * @param departmentId
     * @param userIds
     */
    void disableAccount(String departmentId, String userIds);

    /**
     * 启用人员账号
     * @param userIds
     */
    void enableAccount(String userIds);

    /**
     * 查找人员直接领导
     * @param departmentId
     * @param userId
     * @return
     */
    DynaBean findDirectLeaderBean(String departmentId, String userId);

    /**
     * 查找人员直接领导
     * @param departmentId
     * @param userId
     * @return
     */
    String findDirectLeader(String departmentId, String userId);


    /**
     * 添加人员到其他部门
     * @param userDeptIds
     */
    void addOtherDepartment(String userDeptIds) throws DepartmentUserException;

    /***
     * 公司组织人员树
     * @return
     */
    JSONTreeNode findOrgStructureAndBuildTreeNode();


    /***
     * 移动部门人员
     * @param id
     * @param told
     * @param place
     */
    void moveDeptUser(String id, String told, String place) throws DepartmentUserException;

    /***
     * 移动部门
     * @param id
     * @param told
     * @param place
     */
    void moveDept(String id, String told, String place) throws DepartmentUserException;


    /***
     * 更新用户名称
     * @param deptUserBean
     */
    void updateUserName(DynaBean deptUserBean);

    /***
     * 移除该人员-全局同步更新移除该人员信息数据
     * @param userId
     */
    public void syncRemoveUserMsg(String userId);

    /***
     * 修改部门用户信息时-同步所属部门主管领导信息
     * @param deptUserBean
     * @param deptBean
     * @param userBean
     * @param departmentId
     */
    public void syncMajor(DynaBean deptUserBean ,DynaBean deptBean ,DynaBean userBean, String departmentId);


}
