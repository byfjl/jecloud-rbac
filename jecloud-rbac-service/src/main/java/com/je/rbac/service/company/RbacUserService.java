/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company;

import com.je.common.base.DynaBean;
import java.util.List;

/**
 * 基础用户服务定义
 */
public interface RbacUserService {

    /**
     * 查找人员基本信息
     * @param userId
     * @return
     */
    DynaBean findBaseUserById(String userId);

    /**
     * 查找人员基本信息
     * @param userCode
     * @return
     */
    DynaBean findBaseUserByCode(String userCode);

    /**
     * 查找人员基本信息
     * @param userName
     * @return
     */
    DynaBean findBaseUserByName(String userName);

    /**
     * 查找人员基本信息
     * @param phone
     * @return
     */
    DynaBean findBaseUserByPhone(String phone);

    /**
     * 查找人员基本信息
     * @param email
     * @return
     */
    DynaBean findBaseUserByEmail(String email);

    /**
     * 是否初始化
     * @param id
     * @return
     */
    default boolean isInit(String id){
        DynaBean userBean = findBaseUserById(id);
        return "1".equals(userBean.getStr("USER_INITED_CODE"));
    }

    /**
     * 是否已婚
     * @param id
     * @return
     */
    default boolean isMarried(String id){
        DynaBean userBean = findBaseUserById(id);
        return "1".equals(userBean.getStr("USER_MARRIED_CODE"));
    }

    /**
     * 根据租户ID查找
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户CODE查找
     * @param tenantCode
     * @return
     */
    List<DynaBean> findByTenantCode(String tenantCode);

    /**
     * 根据租户NAME查找
     * @param tenantName
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

    /**
     * 角色名称修改，同步更新user角色名称
     * @param roleId
     * @param roleName
     */
    void updateUserRoleName(String roleId,String roleName);

}
