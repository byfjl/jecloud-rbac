/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.PlatformService;
import com.je.common.base.service.impl.move.AbstractMoveService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.*;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.model.dd.DicInfoVo;
import com.je.rbac.exception.DepartmentException;
import com.je.rbac.exception.DepartmentUserException;
import com.je.rbac.rpc.AccountRpcService;
import com.je.rbac.rpc.CompanyRpcService;
import com.je.rbac.service.IconType;
import com.je.rbac.service.company.RbacDepartmentService;
import com.je.rbac.service.company.RbacDepartmentUserService;
import com.je.rbac.service.grant.role.RbacGrantRoleService;
import com.je.rbac.service.organization.OrgType;
import com.je.rbac.service.remove.CompanyDeptMoveService;
import com.je.rbac.util.HypyUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class RbacDepartmentServiceImpl implements RbacDepartmentService {

    @Autowired
    private MetaService metaService;
    @Autowired
    protected PlatformService manager;
    @Autowired
    private BeanService beanService;
    @Lazy
    @Autowired
    private CompanyRpcService companyRpcService;
    @Autowired
    private CommonService commonService;
    @Lazy
    @Autowired
    private RbacDepartmentUserService rbacDepartmentUserService;
    @Autowired
    private AccountRpcService accountRpcService;
    @Autowired
    private CompanyDeptMoveService companyDeptMoveService;
    @Autowired
    private RbacGrantRoleService rbacGrantRoleService;

    @Override
    @Transactional(rollbackFor = {RuntimeException.class})
    public void updateSyncMajorUser(DynaBean dynaBean) {
        //处理主管，同步部门员工表
        if (!Strings.isNullOrEmpty(dynaBean.getStr("DEPARTMENT_MAJOR_ID"))) {
            List<DynaBean> deptUserBeans = metaService.select("JE_RBAC_DEPTUSER",
                    ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", dynaBean.getStr("JE_RBAC_DEPARTMENT_ID")).eq("DEPTUSER_SFZG_CODE", "1"));
            List<String> originalMajorIds = new ArrayList<>();
            for (DynaBean eachBean : deptUserBeans) {
                originalMajorIds.add(eachBean.getStr("JE_RBAC_USER_ID"));
            }
            String deletedMajorIds = accountRpcService.getAddedOrDeleteIds("removed", StringUtils.join(originalMajorIds, ","), dynaBean.getStr("DEPARTMENT_MAJOR_ID"));
            String addedMajorIds = accountRpcService.getAddedOrDeleteIds("added", StringUtils.join(originalMajorIds, ","), dynaBean.getStr("DEPARTMENT_MAJOR_ID"));
            if (!Strings.isNullOrEmpty(deletedMajorIds)) {
                List<String> deptMajorIdList = Arrays.asList(deletedMajorIds.split(ArrayUtils.SPLIT));
                //修改部门人员表，去掉主管标示
                metaService.executeSql("UPDATE JE_RBAC_DEPTUSER SET DEPTUSER_SFZG_CODE='0' WHERE JE_RBAC_DEPARTMENT_ID={0} AND JE_RBAC_USER_ID IN ({1}) ", dynaBean.getStr("JE_RBAC_DEPARTMENT_ID"), deptMajorIdList);
            }
            if (!Strings.isNullOrEmpty(addedMajorIds)) {
                List<String> deptMajorIdList = Arrays.asList(addedMajorIds.split(ArrayUtils.SPLIT));
                //修改部门人员表，设置主管标示
                metaService.executeSql("UPDATE JE_RBAC_DEPTUSER SET DEPTUSER_SFZG_CODE='1' WHERE JE_RBAC_DEPARTMENT_ID={0} AND JE_RBAC_USER_ID IN ({1}) ", dynaBean.getStr("JE_RBAC_DEPARTMENT_ID"), deptMajorIdList);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, DepartmentException.class})
    public void checkAndMoveDeptCompanyInfo(DynaBean sourceDeptBean, DynaBean dynaBean) throws DepartmentException {
        if (sourceDeptBean.getStr("SY_COMPANY_ID").equals(dynaBean.getStr("SY_COMPANY_ID")) &&
                sourceDeptBean.getStr("SUPERIOR_DEPARTMENT").equals(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
            return;
        }
        String id = sourceDeptBean.getStr("JE_RBAC_DEPARTMENT_ID");
        String toId = "";
        String place = AbstractMoveService.INSIDE;
        if (!sourceDeptBean.getStr("SY_COMPANY_ID").equals(dynaBean.getStr("SY_COMPANY_ID"))) {
            toId = dynaBean.getStr("SY_COMPANY_ID");
            if (!sourceDeptBean.getStr("SUPERIOR_DEPARTMENT").equals(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
                if (!Strings.isNullOrEmpty(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
                    toId = dynaBean.getStr("SY_PARENT");
                }
            }
        } else if (sourceDeptBean.getStr("SY_COMPANY_ID").equals(dynaBean.getStr("SY_COMPANY_ID"))) {
            DynaBean newDeptBean = findById(dynaBean.getStr("SY_PARENT"));
            toId = sourceDeptBean.getStr("SY_COMPANY_ID");
            //同公司，部门改变；则需判断所选部门是否是原部门的下级；
            if (!sourceDeptBean.getStr("SUPERIOR_DEPARTMENT").equals(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
                if (!Strings.isNullOrEmpty(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
                    if (newDeptBean.getStr("SY_PATH").indexOf(id) != -1) {
                        //下级不允许移动
                        throw new DepartmentException("不允许修改为当前部门的下级，请检查！");
                    } else {
                        toId = dynaBean.getStr("SY_PARENT");
                    }
                }
            }
        }
        //若上级部门不为空，则判断与原数据是否一致，不一致 则目标节点为上级部门
        //若上级部门为空，则判断若与原数据一致，则判断公司是否一致，不一致则目标节点为所属公司
        //若与原数据不一致，则目标节点为上级部门，
        //若目标节点为部门，则判断是否是下级部门，若 是 则不处理，返回提示

        if (!Strings.isNullOrEmpty(toId)) {
            DynaBean moveDynaBean = new DynaBean("", false);
            moveDynaBean.put("id", id);
            moveDynaBean.put("toId", toId);
            moveDynaBean.put("place", place);
            companyDeptMoveService.move(moveDynaBean);
        }
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, DepartmentUserException.class})
    public void addOwnDepartment(String departmentId, String userIds) throws DepartmentUserException {
        List<String> userIdList = Splitter.on(",").splitToList(userIds);
        DynaBean departmentBean = findById(departmentId);
        if (departmentBean == null) {
            throw new DepartmentUserException("不存在的部门！");
        }
        List<DynaBean> userBeanList = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder().in("JE_RBAC_USER_ID", userIdList).eq("DEPTUSER_MAIN_CODE", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        List<DynaBean> deptUserBeanList = metaService.select("JE_RBAC_VDEPTUSER", ConditionsWrapper.builder().in("JE_RBAC_USER_ID", userIdList));
        List<DynaBean> eachDeptUserBeanList;
        for (DynaBean eachUserBean : userBeanList) {
            eachDeptUserBeanList = new ArrayList<>();
            for (DynaBean eachDeptUserBean : deptUserBeanList) {
                if (eachUserBean.getStr("JE_RBAC_USER_ID").equals(eachDeptUserBean.getStr("JE_RBAC_USER_ID"))) {
                    eachDeptUserBeanList.add(eachDeptUserBean);
                }
            }
            if (eachDeptUserBeanList != null && eachDeptUserBeanList.size() > 0) {
                //遍历当前用户的所有部门，查找是否已经是分部门
                Boolean falg = false;
                for (DynaBean eachBean : eachDeptUserBeanList) {
                    if (eachBean.getStr("JE_RBAC_DEPARTMENT_ID").equals(departmentId) && eachBean.getStr("DEPTUSER_MAIN_CODE").equals("0")) {
                        falg = true;
                        break;
                    }
                }
                if (!falg) {
                    List<Map<String, Object>> countList = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_DEPTUSER WHERE JE_RBAC_DEPARTMENT_ID ='" + departmentId + "'");
                    if (countList.size() <= 0) {
                        countList = metaService.selectSql("SELECT MAX(DEPARTMENT_USER_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_DEPARTMENT WHERE JE_RBAC_DEPARTMENT_ID ='" + departmentId + "'");
                    }
                    int count = Integer.valueOf(countList.get(0).get("MAX_COUNT").toString());

                    //添加当前部门为用户的分部门
                    DynaBean needAddDeptUserBean = new DynaBean("JE_RBAC_DEPTUSER", false);
                    needAddDeptUserBean.set("JE_RBAC_DEPARTMENT_ID", departmentId);
                    needAddDeptUserBean.set("JE_RBAC_USER_ID", eachUserBean.getStr("JE_RBAC_USER_ID"));
                    needAddDeptUserBean.set("DEPTUSER_MAIN_CODE", "0");
                    needAddDeptUserBean.set("DEPTUSER_MAIN_NAME", "分部门");
                    needAddDeptUserBean.set("SY_COMPANY_ID", departmentBean.getStr("SY_COMPANY_ID"));
                    needAddDeptUserBean.set("SY_COMPANY_NAME", departmentBean.getStr("SY_COMPANY_NAME"));
                    DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder().in("JE_RBAC_COMPANY_ID", departmentBean.getStr("SY_COMPANY_ID")));
                    needAddDeptUserBean.set("SY_COMPANY_CODE", companyBean.getStr("COMPANY_CODE"));
                    needAddDeptUserBean.set("SY_GROUP_COMPANY_ID", departmentBean.getStr("SY_GROUP_COMPANY_ID"));
                    needAddDeptUserBean.set("SY_GROUP_COMPANY_NAME", departmentBean.getStr("SY_GROUP_COMPANY_NAME"));
                    needAddDeptUserBean.set("SY_TENANT_ID", departmentBean.getStr("SY_TENANT_ID"));
                    needAddDeptUserBean.set("SY_TENANT_NAME", departmentBean.getStr("SY_TENANT_NAME"));

                    if (departmentBean.getStr("DEPARTMENT_MAJOR_ID") != null && departmentBean.getStr("DEPARTMENT_MAJOR_ID").equals(eachUserBean.getStr("JE_RBAC_USER_ID"))) {
                        needAddDeptUserBean.set("DEPTUSER_SFZG_CODE", "1");
                        needAddDeptUserBean.set("DEPTUSER_SFZG_NAME", "是");
                    } else {
                        needAddDeptUserBean.set("DEPTUSER_SFZG_CODE", "0");
                        needAddDeptUserBean.set("DEPTUSER_SFZG_NAME", "否");
                        needAddDeptUserBean.set("DEPTUSER_MAJOR_ID", departmentBean.getStr("DEPARTMENT_MAJOR_ID"));
                        needAddDeptUserBean.set("DEPTUSER_MAJOR_ID", departmentBean.getStr("DEPARTMENT_MAJOR_NAME"));
                    }
                    needAddDeptUserBean.set("SY_ORDERINDEX", count + 1);
                    needAddDeptUserBean.set("SY_STATUS", "0");
                    needAddDeptUserBean.set("SY_CREATETIME", DateUtils.formatDateTime(new Date()));
                    metaService.insert(needAddDeptUserBean);
                }
            } else {
                continue;
            }

        }

    }

    @Override
    public Page loadDepartmentUser(BaseMethodArgument param, HttpServletRequest request) {
        if (param.getLimit() == 0) {
            param.setLimit(-1);
        }
        //分页对象
        Page page = new Page<>(param.getPage(), param.getLimit());

        //如果是SQL或者存储过程查询
//        String queryType = param.getQueryType();
        String funcId = param.getFuncId();
        String funcCode = param.getFuncCode();
        //构建查询条件
//        ConditionsWrapper wrapper = queryBuilder.buildWrapper(param);
        List<Map<String, Object>> list = null;
        //普通功能查询数据
        String departmentId = request.getAttribute("departmentId").toString();

        list = metaService.selectSql(page, ConditionsWrapper.builder().table("JE_RBAC_VDEPTUSER").eq("JE_RBAC_DEPARTMENT_ID", departmentId));
        page.setRecords(list);
        return page;
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, DepartmentUserException.class})
    public DynaBean doSave(DynaBean dynaBean) throws DepartmentUserException {
        String deptId = JEUUID.uuid();
        dynaBean.set("JE_RBAC_DEPARTMENT_ID", deptId);
        if (Strings.isNullOrEmpty(dynaBean.getStr("SY_COMPANY_ID"))) {
            throw new DepartmentUserException("请选择上级公司！");
        }

        int currentChildCount = 0;
        List<Map<String, Object>> currentChildList = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_VCOMPANYDEPT WHERE SY_PARENT = {0}", dynaBean.getStr("DEPARTMENT_PARENT"));
        if (currentChildList.size() > 0) {
            currentChildCount = Integer.valueOf(currentChildList.get(0).get("MAX_COUNT").toString());
        }

        int userOrderCount = 10000;
        List<Map<String, Object>> userOrderList = metaService.selectSql("SELECT MAX(DEPARTMENT_USER_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_DEPARTMENT");
        if (userOrderList.size() > 0) {
            userOrderCount = Integer.valueOf(userOrderList.get(0).get("MAX_COUNT").toString());
        }
        if (!Strings.isNullOrEmpty(dynaBean.getStr("SY_COMPANY_ID")) && Strings.isNullOrEmpty(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
            //上级为公司
            //判断公司状态，若公司状态为禁用 则不可新增
            DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder().eq("JE_RBAC_COMPANY_ID", dynaBean.getStr("SY_COMPANY_ID")));
            if (companyBean == null) {
                throw new DepartmentUserException("上级部门不存在，请重新选择！");
            }
            //校验状态
            if (companyBean.getStr("SY_STATUS").equals("0")) {
                throw new DepartmentUserException("上级公司已被禁用！");
            }
            metaService.executeSql("UPDATE JE_RBAC_COMPANY SET SY_NODETYPE='GENERAL' WHERE JE_RBAC_COMPANY_ID = {0}", companyBean.getStr("SY_COMPANY_ID"));

            //设置公司数据
            dynaBean.setStr("SY_COMPANY_ID", companyBean.getStr("JE_RBAC_COMPANY_ID"));
            dynaBean.setStr("SY_COMPANY_NAME", companyBean.getStr("COMPANY_NAME"));
            dynaBean.setStr("SY_GROUP_COMPANY_ID", companyBean.getStr("SY_GROUP_COMPANY_ID"));
            dynaBean.setStr("SY_GROUP_COMPANY_NAME", companyBean.getStr("SY_GROUP_COMPANY_NAME"));
            //公司下添加部门，部门层级为root下级，设置树形数据
            dynaBean.set("SY_PARENT", "ROOT");
            dynaBean.set("DEPARTMENT_PARENT", companyBean.getStr("JE_RBAC_COMPANY_ID"));
            dynaBean.set("SY_PARENTPATH", "/ROOT");
            dynaBean.set("SY_LAYER", companyBean.getStr("SY_LAYER") + 1);
            dynaBean.set("SY_NODETYPE", "LEAF");
            dynaBean.set("PARENT_NODETYPE", "company");
            dynaBean.set("SY_PATH", companyBean.getStr("SY_PATH") + "/" + deptId);
            dynaBean.set("SY_TREEORDERINDEX", companyBean.getStr("SY_TREEORDERINDEX") + String.format("%06d", currentChildCount + 1));
        } else if (!Strings.isNullOrEmpty(dynaBean.getStr("SY_COMPANY_ID")) && !Strings.isNullOrEmpty(dynaBean.getStr("SUPERIOR_DEPARTMENT"))) {
            //上级为部门，若部门状态为禁用 则不可新增
            DynaBean parentDeptBean = findById(dynaBean.getStr("SY_PARENT"));
            if (parentDeptBean == null) {
                throw new DepartmentUserException("不存在的上级部门，请确认上级部门是否存在！");
            }
            //校验状态
            if (parentDeptBean.getStr("SY_STATUS").equals("0")) {
                throw new DepartmentUserException("上级部门已被禁用！");
            }
            metaService.executeSql("UPDATE JE_RBAC_DEPARTMENT SET SY_NODETYPE='GENERAL' WHERE JE_RBAC_DEPARTMENT_ID = {0}", parentDeptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
            //设置上级部门数据，设置树形数据

            //设置公司数据
            dynaBean.setStr("SY_COMPANY_ID", parentDeptBean.getStr("SY_COMPANY_ID"));
            dynaBean.setStr("SY_COMPANY_NAME", parentDeptBean.getStr("SY_COMPANY_NAME"));
            dynaBean.setStr("SY_GROUP_COMPANY_ID", parentDeptBean.getStr("SY_GROUP_COMPANY_ID"));
            dynaBean.setStr("SY_GROUP_COMPANY_NAME", parentDeptBean.getStr("SY_GROUP_COMPANY_NAME"));
            //设置树形数据
            dynaBean.set("SY_PARENT", parentDeptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
            dynaBean.set("DEPARTMENT_PARENT", parentDeptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
            dynaBean.set("SY_PARENTPATH", parentDeptBean.getStr("SY_PATH"));
            dynaBean.set("SY_LAYER", parentDeptBean.getInt("SY_LAYER") + 1);
            dynaBean.set("SY_NODETYPE", "LEAF");
            dynaBean.set("PARENT_NODETYPE", "department");
            dynaBean.set("SY_PATH", parentDeptBean.getStr("SY_PATH") + "/" + deptId);
            dynaBean.set("SY_TREEORDERINDEX", parentDeptBean.getStr("SY_TREEORDERINDEX") + String.format("%06d", currentChildCount + 1));
        }
        dynaBean.set("SY_STATUS", "1");
        dynaBean.set("DEPARTMENT_ICON", IconType.TCONTYPE_DEPART.getVal());
        dynaBean.set("SY_ORG_ID", OrgType.DEPARTMENT_ORG_ID.getCode());
        dynaBean.set("SY_ORDERINDEX", currentChildCount + 1);
        dynaBean.set("DEPARTMENT_USER_ORDERINDEX", userOrderCount + 10000);
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);

        //处理主管，同步部门员工表
        if (!Strings.isNullOrEmpty(dynaBean.getStr("DEPARTMENT_MAJOR_ID"))) {
            List<String> deptMajorIdList = Arrays.asList(dynaBean.getStr("DEPARTMENT_MAJOR_ID").split(ArrayUtils.SPLIT));
            //修改部门人员表，设置主管标示
            metaService.executeSql("UPDATE JE_RBAC_DEPTUSER SET DEPTUSER_SFZG_CODE='1' WHERE JE_RBAC_DEPARTMENT_ID={0} AND JE_RBAC_USER_ID IN ({1}) ", dynaBean.getStr("JE_RBAC_DEPARTMENT_ID"), deptMajorIdList);
        }
        return dynaBean;
    }

    @Override
    public boolean checkDeptNameUnique(DynaBean deptBean) {
        long count = 0;
        if (Strings.isNullOrEmpty(deptBean.getStr("JE_RBAC_DEPARTMENT_ID"))) {
            count = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_DEPARTMENT").eq("DEPARTMENT_NAME", deptBean.getStr("DEPARTMENT_NAME")));
        } else {
            count = metaService.countBySql(ConditionsWrapper.builder().apply("SELECT * FROM JE_RBAC_DEPARTMENT WHERE JE_RBAC_DEPARTMENT_ID != {0} AND (DEPARTMENT_NAME = {1})", deptBean.getStr("JE_RBAC_DEPARTMENT_ID"), deptBean.getStr("DEPARTMENT_NAME")));
        }
        if (count > 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean checkDeptCodeUnique(DynaBean deptBean) {
        long count = 0;
        if (Strings.isNullOrEmpty(deptBean.getStr("JE_RBAC_DEPARTMENT_ID"))) {
            count = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_DEPARTMENT").eq("DEPARTMENT_CODE", deptBean.getStr("DEPARTMENT_CODE")));
        } else {
            count = metaService.countBySql(ConditionsWrapper.builder().apply("SELECT * FROM JE_RBAC_DEPARTMENT WHERE JE_RBAC_DEPARTMENT_ID != {0} AND (DEPARTMENT_CODE = {1})", deptBean.getStr("JE_RBAC_DEPARTMENT_ID"), deptBean.getStr("DEPARTMENT_CODE")));
        }
        if (count > 0) {
            return false;
        }
        return true;
    }

    @Override
    public void updateDeptName(DynaBean deptBean) {
        DynaBean sourceDeptBean = findById(deptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
        if (!sourceDeptBean.getStr("DEPARTMENT_NAME").equals(deptBean.getStr("DEPARTMENT_NAME"))) {
            //修改部门表，监管部门名称
            List<DynaBean> listBean = metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().like("DEPARTMENT_MONITORDEPT_ID", "%" + deptBean.getStr("JE_RBAC_DEPARTMENT_ID") + "%"));
            if (listBean != null && listBean.size() > 0) {
                for (DynaBean eachBean : listBean) {
                    List<String> jgIdList = Arrays.asList(eachBean.getStr("DEPARTMENT_MONITORDEPT_ID").split(ArrayUtils.SPLIT));
                    List<String> jgNameList = Arrays.asList(eachBean.getStr("DEPARTMENT_MONITORDEPT_NAME").split(ArrayUtils.SPLIT));
                    int indexCount = jgIdList.indexOf(sourceDeptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
                    jgNameList.set(indexCount, deptBean.getStr("DEPARTMENT_NAME"));
                    metaService.executeSql("UPDATE JE_RBAC_DEPARTMENT SET DEPARTMENT_MONITORDEPT_NAME={0} WHERE JE_RBAC_DEPARTMENT_ID={1} ", StringUtils.join(jgNameList, ","), eachBean.getStr("JE_RBAC_DEPARTMENT_ID"));
                }
            }

            //修改人员表，监管部门名称
            List<DynaBean> listUserBean = metaService.select("JE_RBAC_USER", ConditionsWrapper.builder().like("USER_MONITORDEPT_ID", "%" + deptBean.getStr("JE_RBAC_DEPARTMENT_ID") + "%"));
            if (listUserBean != null && listUserBean.size() > 0) {
                for (DynaBean eachBean : listUserBean) {
                    List<String> jgIdList = Arrays.asList(eachBean.getStr("USER_MONITORDEPT_ID").split(ArrayUtils.SPLIT));
                    List<String> jgNameList = Arrays.asList(eachBean.getStr("USER_MONITORDEPT_NAME").split(ArrayUtils.SPLIT));
                    int indexCount = jgIdList.indexOf(sourceDeptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
                    jgNameList.set(indexCount, deptBean.getStr("DEPARTMENT_NAME"));
                    metaService.executeSql("UPDATE JE_RBAC_USER SET USER_MONITORDEPT_NAME={0} WHERE JE_RBAC_USER_ID={1} ", StringUtils.join(jgNameList, ","), eachBean.getStr("JE_RBAC_USER_ID"));
                }
            }

            //角色账号部门表
            metaService.executeSql("UPDATE JE_RBAC_ACCOUNTROLE SET ACCOUNTROLE_DEPT_NAME={0} WHERE ACCOUNTROLE_DEPT_ID = {1}", deptBean.getStr("DEPARTMENT_NAME"), deptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
            //账号部门表
            metaService.executeSql("UPDATE JE_RBAC_ACCOUNTDEPT SET ACCOUNTDEPT_DEPT_NAME={0} WHERE ACCOUNTDEPT_DEPT_ID = {1}", deptBean.getStr("DEPARTMENT_NAME"), deptBean.getStr("JE_RBAC_DEPARTMENT_ID"));
        }
    }

    @Override
    public boolean updateCheckUnique(DynaBean deptBean) {
        String deptName = deptBean.getStr("DEPARTMENT_NAME");
        String deptCode = deptBean.getStr("DEPARTMENT_CODE");
        String tableId = deptBean.getStr("JE_RBAC_DEPARTMENT_ID");
        long count = metaService.countBySql(ConditionsWrapper.builder().apply("SELECT * FROM JE_RBAC_DEPARTMENT WHERE JE_RBAC_DEPARTMENT_ID != {0} AND (DEPARTMENT_NAME = {1} or DEPARTMENT_CODE = {2})", tableId, deptName, deptCode));
        if (count > 0) {
            return false;
        }
        return true;
    }

    @Override
    public Boolean findUpCompanyStatus(String departmentIds) throws DepartmentException {
        List<String> idList = Splitter.on(",").splitToList(departmentIds);
        List<DynaBean> deptBeans = metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", idList));
        for (DynaBean eachBean : deptBeans) {
            if (eachBean.getStr("SY_PARENT").equals("ROOT")) {
                //如果当前部门为顶级部门则判断所属公司是否被禁用
                DynaBean companyBean = metaService.selectOne("JE_RBAC_COMPANY", ConditionsWrapper.builder().eq("JE_RBAC_COMPANY_ID", eachBean.getStr("SY_COMPANY_ID")));
                if (companyBean != null && companyBean.getStr("SY_STATUS").equals("0")) {
                    if (idList.size() == 1) {
                        throw new DepartmentException("该组织上级公司已被禁用，请先启用！");
                    } else {
                        throw new DepartmentException("该组织【" + eachBean.getStr("DEPARTMENT_NAME") + "】的上级公司已被禁用，请先启用！");
                    }

                }
            }
        }
        return true;
    }

    @Override
    public Boolean findUpDeptStatus(String departmentIds) throws DepartmentException {
        List<String> idList = Splitter.on(",").splitToList(departmentIds);
        List<DynaBean> deptBeans = metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", idList));
        for (DynaBean eachBean : deptBeans) {
            if (!eachBean.getStr("SY_PARENT").equals("ROOT")) {
                //判断上级部门
                DynaBean parentDeptBean = metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("JE_RBAC_DEPARTMENT_ID", eachBean.getStr("SY_PARENT")));
                if (parentDeptBean != null && parentDeptBean.getStr("SY_STATUS").equals("0")) {
                    if (idList.size() == 1) {
                        throw new DepartmentException("该组织上级部门已被禁用，请先启用！");
                    } else {
                        throw new DepartmentException("该组织【" + eachBean.getStr("DEPARTMENT_NAME") + "】的上级部门已被禁用，请先启用！");
                    }
                }
            }
        }
        return true;
    }


    @Override
    public boolean checkChildDept(String deptIds) {
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        if (!Strings.isNullOrEmpty(deptIds)) {
            for (String idStr : idList) {
                long childDeptCount = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_DEPARTMENT").in("SY_PARENT", idStr));
                if (childDeptCount > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean checkChildUser(String deptIds) {
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        long deptUserCount = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_DEPTUSER").in("JE_RBAC_DEPARTMENT_ID", idList));
        if (deptUserCount > 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean checkCompanyStatus(String departmentIds) {
        List<String> idList = Arrays.asList(departmentIds.split(ArrayUtils.SPLIT));
        ;
        List<DynaBean> deptList = metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", idList));
        List<String> companyIds = new ArrayList<>();
        for (DynaBean eachBean : deptList) {
            companyIds.add(eachBean.getStr("SY_COMPANY_ID"));
        }
        long count = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_COMPANY").eq("SY_STATUS", "0").in("JE_RBAC_COMPANY_ID", companyIds));
        if (count > 0) {
            return false;
        }
        return true;
    }

    @Override
    public List<DynaBean> findAll() {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_STATUS", "1").apply(" ORDER BY SY_TREEORDERINDEX ASC , SY_ORDERINDEX ASC"));
    }

    @Override
    public DynaBean findById(String id) {
        return metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("JE_RBAC_DEPARTMENT_ID", id));
    }

    @Override
    public DynaBean findByCode(String code) {
        return metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("DEPARTMENT_CODE", code));
    }

    @Override
    public DynaBean findByName(String name) {
        return metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("DEPARTMENT_NAME", name));
    }

    @Override
    public DynaBean findParent(String id) {
        DynaBean currentDeptBean = findById(id);
        if (currentDeptBean == null) {
            return null;
        }
        String parentId = currentDeptBean.getStr("SY_PARENT");
        if (Strings.isNullOrEmpty(parentId)) {
            return null;
        }
        return findById(parentId);
    }

    @Override
    public DynaBean findAncestors(String id) {
        return metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().likeLeft("SY_PATH", id).eq("SY_PARENT", "ROOT"));
    }

    @Override
    public List<DynaBean> findByTenantId(String tenantId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_TENANT_ID", tenantId));
    }

    @Override
    public List<DynaBean> findByTenantName(String tenantName) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_TENANT_NAME", tenantName));
    }

    @Override
    public List<DynaBean> findNextChildren(String id, boolean containMe) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_PARENT", id)
                .or(containMe).eq(containMe, "JE_RBAC_DEPARTMENT_ID", id));
    }

    @Override
    public List<DynaBean> findAllChildren(String id, boolean containMe) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().likeRight("SY_PATH", id)
                .or(containMe).eq(containMe, "JE_RBAC_DEPARTMENT_ID", id));
    }

    @Override
    public List<DynaBean> findByMajorId(String majorId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("DEPARTMENT_MAJOR_ID", majorId));
    }

    @Override
    public List<DynaBean> findCompanyAllDepartments(String... companyIds) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_STATUS", "1").in("SY_COMPANY_ID", companyIds).apply(" ORDER BY SY_TREEORDERINDEX ASC , SY_ORDERINDEX ASC"));
    }

    @Override
    public List<DynaBean> findCompanyAllDepartments(String companyId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_COMPANY_ID", companyId));
    }

    @Override
    public List<DynaBean> findCompanyFirstLevelDepartements(String companyId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .eq("SY_COMPANY_ID", companyId).eq("SY_PARENT", "ROOT"));
    }

    @Override
    public List<DynaBean> findGroupCompanyAllDepartments(String groupCompanyId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().eq("SY_GROUP_COMPANY_ID", groupCompanyId));
    }

    @Override
    public List<DynaBean> findCompanyFirstLevelDepartments(String groupCompanyId) {
        return metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .eq("SY_GROUP_COMPANY_ID", groupCompanyId).eq("SY_PARENT", "ROOT"));
    }

    @Override
    public Set<String> findExtendIds(List<String> departmentIds) {
        List<DynaBean> roleBeanList = metaService.select("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .in("JE_RBAC_DEPARTMENT_ID", departmentIds).ne("SY_NODETYPE", "ROOT"));
        Set<String> extendRoleIdList = new HashSet<>();
        for (DynaBean eachRoleBean : roleBeanList) {
            String parentPath = eachRoleBean.getStr("SY_PARENTPATH");
            if (!Strings.isNullOrEmpty(parentPath) && !"ROOT".equals("SY_NODETYPE")) {
                extendRoleIdList.addAll(Splitter.on("/").splitToList(parentPath));
            }
        }
        return extendRoleIdList;
    }

    @Override
    public void disableDepartments(String departmentIds) {
        List<String> departmentIdList = findAllPathDepartments(departmentIds);
        //禁用部门及其子部门
        metaService.executeSql("UPDATE JE_RBAC_DEPARTMENT SET SY_STATUS = '0' WHERE JE_RBAC_DEPARTMENT_ID IN ({0})", departmentIdList);

        //需要移除用户和账号的用户集合
        List<DynaBean> deptUserList = metaService.select("JE_RBAC_DEPTUSER", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", departmentIdList));
        if (deptUserList != null && deptUserList.size() > 0) {
            rbacDepartmentUserService.doDisableUser(deptUserList);
        }
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void enableDepartments(String departmentIds) {
        List<String> deptIdList = Arrays.asList(departmentIds.split(ArrayUtils.SPLIT));
        metaService.executeSql("UPDATE JE_RBAC_DEPARTMENT SET SY_STATUS = '1' WHERE JE_RBAC_DEPARTMENT_ID IN ({0})", deptIdList);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public long removeDepartments(String departmentIds, boolean withUsers) {
        List<String> deptIdList = findAllPathDepartments(departmentIds);
        //部门表-全局监管部门数据去除该部门信息
        updateDeptJgField(departmentIds);

        //人员表-全局监管部门数据去除该部门信息
        updateUserDeptJgField(departmentIds);

        long count = metaService.delete("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", deptIdList));
        if (!withUsers) {
            return count;
        }
        //需要移除用户和账号的用户集合
        List<DynaBean> deptUserList = metaService.select("JE_RBAC_DEPTUSER", ConditionsWrapper.builder().in("JE_RBAC_DEPARTMENT_ID", deptIdList));
        if (deptUserList != null && deptUserList.size() > 0) {
            rbacDepartmentUserService.doRemove(deptUserList);
        }
        return count;
    }

    /***
     * 部门表-全局同步更新去除该部门信息
     * @param deptIds
     */
    @Transactional(rollbackFor = RuntimeException.class)
    void updateDeptJgField(String deptIds) {
        List<Map<String, Object>> listDeptBeanMap = findAllJGDeptById(deptIds);
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        for (Map<String, Object> eachBeanMap : listDeptBeanMap) {
            //监管部门Id
            List<String> jgIdList = Arrays.asList(eachBeanMap.get("DEPARTMENT_MONITORDEPT_ID").toString().split(ArrayUtils.SPLIT));
            List<String> newGgIdList = new ArrayList(jgIdList);
            List<String> indexData = new ArrayList<>();
            for (String deptId : idList) {
                if (jgIdList.indexOf(deptId) >= 0) {
                    //记录要删除的下标
                    indexData.add(jgIdList.indexOf(deptId) + "");
                    newGgIdList.remove(deptId);
                }
            }
            //监管公司名称
            List<String> jgNameList = Arrays.asList(eachBeanMap.get("DEPARTMENT_MONITORDEPT_NAME").toString().split(ArrayUtils.SPLIT));
            List<String> newGgNameList = new ArrayList(jgNameList);
            for (String eachIndex : indexData) {
                newGgNameList.remove(Integer.parseInt(eachIndex));
            }
            metaService.executeSql("UPDATE JE_RBAC_DEPARTMENT SET DEPARTMENT_MONITORDEPT_ID={0},DEPARTMENT_MONITORDEPT_NAME={1} WHERE JE_RBAC_DEPARTMENT_ID={2} ", StringUtils.join(newGgIdList, ","), StringUtils.join(newGgNameList, ","), eachBeanMap.get("JE_RBAC_DEPARTMENT_ID"));
        }
    }

    /***
     * 人员表-全局同步更新去除该部门信息
     * @param deptIds
     */
    @Transactional(rollbackFor = RuntimeException.class)
    void updateUserDeptJgField(String deptIds) {
        List<Map<String, Object>> listUserBeanMap = findUserAllJGDeptById(deptIds);
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        for (Map<String, Object> eachBeanMap : listUserBeanMap) {
            //监管部门Id
            List<String> jgIdList = Arrays.asList(eachBeanMap.get("USER_MONITORDEPT_ID").toString().split(ArrayUtils.SPLIT));
            List<String> newGgIdList = new ArrayList(jgIdList);
            List<String> indexData = new ArrayList<>();
            for (String deptId : idList) {
                if (jgIdList.indexOf(deptId) >= 0) {
                    //记录要删除的下标
                    indexData.add(jgIdList.indexOf(deptId) + "");
                    newGgIdList.remove(deptId);
                }
            }
            //监管公司名称
            List<String> jgNameList = Arrays.asList(eachBeanMap.get("USER_MONITORDEPT_NAME").toString().split(ArrayUtils.SPLIT));
            List<String> newGgNameList = new ArrayList(jgNameList);
            for (String eachIndex : indexData) {
                newGgNameList.remove(Integer.parseInt(eachIndex));
            }
            metaService.executeSql("UPDATE JE_RBAC_USER SET USER_MONITORDEPT_ID={0},USER_MONITORDEPT_NAME={1} WHERE JE_RBAC_USER_ID={2} ", StringUtils.join(newGgIdList, ","), StringUtils.join(newGgNameList, ","), eachBeanMap.get("JE_RBAC_USER_ID"));
        }
    }

    /***
     * 查询所有监管该部门的部门数据
     * @param deptIds
     * @return
     */
    private List<Map<String, Object>> findAllJGDeptById(String deptIds) {
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        StringBuilder deptSqlBuffer = new StringBuilder("SELECT * FROM JE_RBAC_DEPARTMENT WHERE ");
        String[] params = new String[idList.size()];
        for (int i = 0; i < idList.size(); i++) {
            if (i == 0) {
                deptSqlBuffer.append(" DEPARTMENT_MONITORDEPT_ID LIKE {" + i + "}");
            } else {
                deptSqlBuffer.append(" OR DEPARTMENT_MONITORDEPT_ID LIKE {" + i + "}");
            }
            params[i] = "%" + idList.get(i) + "%";
        }
        List<Map<String, Object>> deptBeans = metaService.selectSql(deptSqlBuffer.toString(), params);
        return deptBeans;
    }

    /***
     * 查询所有监管该部门的人员数据
     * @param deptIds
     * @return
     */
    private List<Map<String, Object>> findUserAllJGDeptById(String deptIds) {
        List<String> idList = Splitter.on(",").splitToList(deptIds);
        StringBuilder deptSqlBuffer = new StringBuilder("SELECT * FROM JE_RBAC_USER WHERE ");
        String[] params = new String[idList.size()];
        for (int i = 0; i < idList.size(); i++) {
            if (i == 0) {
                deptSqlBuffer.append(" USER_MONITORDEPT_ID LIKE {" + i + "}");
            } else {
                deptSqlBuffer.append(" OR USER_MONITORDEPT_ID LIKE {" + i + "}");
            }
            params[i] = "%" + idList.get(i) + "%";
        }
        List<Map<String, Object>> deptBeans = metaService.selectSql(deptSqlBuffer.toString(), params);
        return deptBeans;
    }

    @Override
    public void removeDepartmentUser(String userDeptIds) {
        JSONArray array = JSONArray.parseArray(userDeptIds);
        if (array != null && array.size() > 0) {
            List<Map<String, String>> mainDepartUserMapList = new ArrayList<>();
            List<Map<String, String>> otherDepartUserMapList = new ArrayList<>();
            for (int i = 0; i < array.size(); i++) {
                Map<String, String> otherDepartUserMap = new HashMap<>();
                String departmentId = array.getJSONObject(i).getString("departmentId");
                String userId = array.getJSONObject(i).getString("userIds");
                String mainCode = array.getJSONObject(i).getString("mainCode");
                //判断人员是否是主部门 主部门与当前人的所有数据，全部删掉；若是分部门则只删除与分部门的关系
                if (mainCode.equals("1")) {
                    throw new DepartmentUserException("组织关系为主部门的员工不允许移除！");
                } else {
                    otherDepartUserMap.put(departmentId, userId);
                    otherDepartUserMapList.add(otherDepartUserMap);
                }
            }
            rbacDepartmentUserService.removeUsers(mainDepartUserMapList, otherDepartUserMapList);
        }
    }

    @Override
    public String findDeptLeader(String departmentId) {
        DynaBean deptLeaderBean = metaService.selectOne("JE_RBAC_DEPARTMENT", ConditionsWrapper.builder()
                .eq("JE_RBAC_DEPARTMENT_ID", departmentId));
        if (deptLeaderBean == null) {
            return null;
        }
        return deptLeaderBean.getStr("DEPARTMENT_MAJOR_ID");
    }

    @Override
    public List<String> findAllPathDepartments(String departmentIds) {
        List<String> idList = Splitter.on(",").splitToList(departmentIds);
        StringBuilder departmentSqlBuffer = new StringBuilder("SELECT * FROM JE_RBAC_DEPARTMENT WHERE ");
        String[] params = new String[idList.size()];
        for (int i = 0; i < idList.size(); i++) {
            if (i == 0) {
                departmentSqlBuffer.append(" SY_PATH LIKE {" + i + "}");
            } else {
                departmentSqlBuffer.append(" OR SY_PATH LIKE {" + i + "}");
            }
            params[i] = "%" + idList.get(i) + "%";
        }
        List<String> departmentIdList = new ArrayList<>();
        List<Map<String, Object>> companyBeans = metaService.selectSql(departmentSqlBuffer.toString(), params);
        for (Map<String, Object> eachMap : companyBeans) {
            departmentIdList.add((String) eachMap.get("JE_RBAC_DEPARTMENT_ID"));
        }
        return departmentIdList;
    }

    @Override
    public JSONTreeNode buildOneCompanyTreeData(String... companyIds) {
        DynaBean table = beanService.getResourceTable("JE_RBAC_DEPARTMENT");
        List<DynaBean> columns = (List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS);
        JSONTreeNode template = beanService.buildJSONTreeNodeTemplate(columns);
        JSONTreeNode companyRootNode = companyRpcService.buildCompanyTree(companyIds);

        List<DynaBean> departmentBeanList;
        if (companyIds == null || companyIds.length <= 0) {
            departmentBeanList = findAll();
        } else {
            departmentBeanList = findCompanyAllDepartments(companyIds);
        }
        recursiveCompanyJsonTreeNode(template, companyRootNode, departmentBeanList);
        return companyRootNode;
    }

    @Override
    public JSONTreeNode buildOnlyCompanyTreeData(Boolean colorFlag) {
        //查询所有公司放入map中
        List<DynaBean> companyList = metaService.select("JE_RBAC_COMPANY", ConditionsWrapper.builder());
        Map<String, String> companyMap = new HashMap<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
        }

        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        List<DynaBean> allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("TYPE", "company").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        for (DynaBean eachBean : allBeanList) {
            if ("ROOT".equals(eachBean.getStr("SY_PARENT")) && "company".equals(eachBean.getStr("TYPE"))) {
                JSONTreeNode node = buildTreeNode("", rootNode.getId(), eachBean, colorFlag);
                //递归存放子级
                recursionDynaBeanTree(node, allBeanList, companyMap, colorFlag);
                rootNode.getChildren().add(node);
            }
        }
        return rootNode;
    }

    @Override
    public JSONTreeNode buildForbiddenCompanyTreeData(Boolean colorFlag, Boolean forbidden) {
        //查询公司放入map中
        List<DynaBean> companyList = null;
        if (forbidden) {
            companyList = metaService.select("JE_RBAC_COMPANY", ConditionsWrapper.builder());
        } else {
            companyList = metaService.select("JE_RBAC_COMPANY", ConditionsWrapper.builder().eq("SY_STATUS", "1"));
        }
        Map<String, String> companyMap = new HashMap<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
        }

        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        List<DynaBean> allBeanList = null;
        if (forbidden) {
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("TYPE", "company").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        } else {
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("TYPE", "company").eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));

        }
        for (DynaBean eachBean : allBeanList) {
            if ("ROOT".equals(eachBean.getStr("SY_PARENT")) && "company".equals(eachBean.getStr("TYPE"))) {
                JSONTreeNode node = buildTreeNode("", rootNode.getId(), eachBean, colorFlag);
                //递归存放子级
                recursionDynaBeanTree(node, allBeanList, companyMap, colorFlag);
                rootNode.getChildren().add(node);
            }
        }
        return rootNode;
    }

    @Override
    public JSONTreeNode buildDeptTreeForDictionary(DicInfoVo dicInfoVo) {
        ConditionsWrapper conditionsWrapper = buildWrapper(dicInfoVo);
        //查询所有公司放入map中
        List<DynaBean> companyList = metaService.select("JE_RBAC_COMPANY",
                conditionsWrapper.eq("SY_STATUS", "1"));
        Map<String, String> companyMap = new HashMap<>();
        List<String> companyId = new ArrayList<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
            companyId.add(eachBean.getStr("JE_RBAC_COMPANY_ID"));
        }
        List<DynaBean> allBeanList;
        if (dicInfoVo.getWhereSql() == null) {
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));

        } else {
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1").in("ID", companyId).apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        }

        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        for (DynaBean eachBean : allBeanList) {
            List<DynaBean> childBeanList = allBeanList;
            if (dicInfoVo.getWhereSql() != null) {
                if ("company".equals(eachBean.getStr("TYPE"))) {
                    //查找子级
                    childBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1").like("SY_PATH", "%" + eachBean.getStr("ID") + "%").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));

                    JSONTreeNode node = buildTreeNode(dicInfoVo.getFieldCode(), rootNode.getId(), eachBean, false);
                    //递归存放子级
                    recursionDynaBeanTree(node, childBeanList, companyMap, false);
                    rootNode.getChildren().add(node);
                }
            } else {
                if ("ROOT".equals(eachBean.getStr("SY_PARENT")) && "company".equals(eachBean.getStr("TYPE"))) {

                    JSONTreeNode node = buildTreeNode(dicInfoVo.getFieldCode(), rootNode.getId(), eachBean, false);
                    //递归存放子级
                    recursionDynaBeanTree(node, childBeanList, companyMap, false);
                    rootNode.getChildren().add(node);
                }
            }
        }
        return rootNode;
    }

    @Override
    public JSONTreeNode buildCompanyDeptTreeData(DicInfoVo dicInfoVo) {
        return buildCompanyTreeData(dicInfoVo.getFieldCode(), false);
//        ConditionsWrapper conditionsWrapper = buildWrapper(dicInfoVo);
//
//        DynaBean table = beanService.getResourceTable("JE_RBAC_VCOMPANYDEPT");
//        List<DynaBean> columns = (List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS);
//        JSONTreeNode template = beanService.buildJSONTreeNodeTemplate(columns);
//        JSONTreeNode companyRootNode = buildCompanyTree(conditionsWrapper, template);
//
//        List<DynaBean> departmentBeanList = metaService.select("JE_RBAC_VCOMPANYDEPT", conditionsWrapper
//                .eq("SY_STATUS", "1").eq("TYPE", "department").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
//
//        recursiveCompanyJsonTreeNode(template, companyRootNode, departmentBeanList);
//        return companyRootNode;
    }

    @Override
    public JSONTreeNode buildCompanyDeptUserTreeData(DicInfoVo dicInfoVo) {
        //公司部门树
        JSONTreeNode treeData = buildCompanyDeptTreeData(dicInfoVo);
        List<DynaBean> departmentUserBeanList = metaService.select("JE_RBAC_VACCOUNTDEPT", ConditionsWrapper.builder().eq("USER_STATUS", "1"));
        recursiveDepartmentUserTreeNode(treeData, departmentUserBeanList);
        return treeData;
    }

    public void recursiveDepartmentUserTreeNode(JSONTreeNode rootNode, List<DynaBean> departmentUserBeanList) {
        for (DynaBean eachDepartmentUserBean : departmentUserBeanList) {
            eachDepartmentUserBean.put("DEPARTMENT_ICON", IconType.TCONTYPE_ROLE.getVal());
            if (!rootNode.getId().equals(eachDepartmentUserBean.getStr("ACCOUNTDEPT_DEPT_ID"))) {
                continue;
            }
            parseAccountDepartmentToTree(rootNode, eachDepartmentUserBean);
        }

        if (rootNode.getChildren() == null || rootNode.getChildren().isEmpty()) {
            return;
        }
        for (JSONTreeNode eachChildNode : rootNode.getChildren()) {
            recursiveDepartmentUserTreeNode(eachChildNode, departmentUserBeanList);
        }
    }

    /**
     * 构建人员信息
     *
     * @param resultJSONTreeNode
     * @param user
     */
    public static void parseAccountDepartmentToTree(JSONTreeNode resultJSONTreeNode, DynaBean user) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(user.getStr("JE_RBAC_ACCOUNTDEPT_ID"));
        node.setCode(user.getStr("ACCOUNT_CODE"));
        //排除部门，组织架构；其余调用均添加部门标示
        node.setText(user.getStr("ACCOUNT_NAME"));
        node.setNodeType("LEAF");
        JSONObject nodeInfo = new JSONObject();
        nodeInfo.put("sex", user.getStr("ACCOUNT_SEX"));
        nodeInfo.put("ACCOUNT_NAME", user.getStr("ACCOUNT_NAME"));
        node.setNodeInfo(nodeInfo.toJSONString());
        node.setNodeInfoType("json");
        node.setIcon(IconType.TCONTYPE_ROLE.getVal());
        node.setParent(resultJSONTreeNode.getId());
        node.setChecked(false);
        node.setLayer(resultJSONTreeNode.getLayer() + 1);
        node.setBean(user.getValues());
        resultJSONTreeNode.getChildren().add(node);

    }

    public JSONTreeNode buildCompanyTree(ConditionsWrapper conditionsWrapper, JSONTreeNode template) {
        JSONTreeNode rootNode = TreeUtil.buildRootNode();

        List<DynaBean> allCompanyBeanList = metaService.select("je_rbac_vcompanydept", conditionsWrapper
                .eq("SY_STATUS", "1").eq("TYPE", "company").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));

        for (DynaBean eachBean : allCompanyBeanList) {
            if ("ROOT".equals(eachBean.getStr("SY_PARENT"))) {
                JSONTreeNode node = buildTreeNode(template, rootNode.getId(), eachBean);
                if (node != null) {
                    rootNode.getChildren().add(node);
                }
            }
        }

        //递归形成树形结构
        for (JSONTreeNode eachNode : rootNode.getChildren()) {
            recursiveJsonTreeNode(template, eachNode, allCompanyBeanList);
        }

        return rootNode;
    }

    private void recursiveJsonTreeNode(JSONTreeNode template, JSONTreeNode rootNode, List<DynaBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }

        JSONTreeNode treeNode;
        for (DynaBean eachBean : list) {
            if (!Strings.isNullOrEmpty(rootNode.getId()) && !Strings.isNullOrEmpty(eachBean.getStr("SY_PARENT"))) {
                if (rootNode.getId().equals(eachBean.getStr("SY_PARENT"))) {
                    treeNode = buildTreeNode(template, rootNode.getId(), eachBean);
                    rootNode.getChildren().add(treeNode);
                }
            }
        }

        if (rootNode.getChildren() == null || rootNode.getChildren().isEmpty()) {
            rootNode.setLeaf(true);
            return;
        }

        rootNode.setLeaf(false);

        for (JSONTreeNode eachNode : rootNode.getChildren()) {
            recursiveJsonTreeNode(template, eachNode, list);
        }

    }

    @Override
    public JSONTreeNode buildRoleUserTree(DicInfoVo dicInfoVo) {
        //获取角色树
        JSONTreeNode jsonTreeNode = rbacGrantRoleService.buildRoleTree(false);

        List<DynaBean> accountRoleBeanList = metaService.select("JE_RBAC_VACCOUNTROLE", ConditionsWrapper.builder().eq("USER_STATUS", "1"));
        recursiveAccountRoleTreeNode(jsonTreeNode, accountRoleBeanList, false);
        return jsonTreeNode;
    }

    private void recursiveAccountRoleTreeNode(JSONTreeNode rootNode, List<DynaBean> accountRoleBeanList, Boolean multiple) {
        if ("role".equals(rootNode.getNodeInfoType())) {
            JSONTreeNode node;
            for (DynaBean accountRoleBean : accountRoleBeanList) {
                if (!rootNode.getId().equals(accountRoleBean.getStr("ACCOUNTROLE_ROLE_ID"))) {
                    continue;
                }
                DynaBean accountUserBean = metaService.selectOne("JE_RBAC_VUSERQUERY", ConditionsWrapper.builder().eq("USER_ID", accountRoleBean.getStr("USER_ASSOCIATION_ID")).eq("DEPARTMENT_ID", accountRoleBean.getStr("ACCOUNTROLE_DEPT_ID")).eq("USER_STATUS", "1"));
                if (accountUserBean == null || accountUserBean.getStr("JE_RBAC_ACCOUNTDEPT_ID") == null) {
                    continue;
                }
                node = new JSONTreeNode();
                node.setId(accountUserBean.getStr("JE_RBAC_ACCOUNTDEPT_ID") + "-" + accountRoleBean.getStr("JE_RBAC_ACCOUNTROLE_ID"));
                node.setCode(accountUserBean.getStr("USER_CODE"));
                node.setText(accountUserBean.getStr("USER_NAME"));

                node.setNodeType("LEAF");
                node.setNodeInfo(accountUserBean.getStr("USER_CODE"));
                node.setNodeInfoType("user");
                node.setIcon(IconType.TCONTYPE_ROLE.getVal());
                node.setParent(rootNode.getId());
                node.setChecked(multiple);
                node.setLayer(rootNode.getLayer() + 1);
                HashMap<String, Object> bean = accountUserBean.getValues();
                bean.put("pinyin", HypyUtil.getPinYin(node.getText()));
                node.setBean(bean);
                rootNode.getChildren().add(node);
            }
        }

        if (rootNode.getChildren() == null || rootNode.getChildren().isEmpty()) {
            return;
        }

        for (JSONTreeNode eachChildNode : rootNode.getChildren()) {
            recursiveAccountRoleTreeNode(eachChildNode, accountRoleBeanList, multiple);
        }

    }

    private ConditionsWrapper buildWrapper(DicInfoVo dicInfoVo) {
        Query query = new Query();
        if (StringUtil.isNotEmpty(dicInfoVo.getWhereSql())) {
            query.addCustoms(dicInfoVo.getWhereSql());
        }
        if (StringUtil.isNotEmpty(dicInfoVo.getOrderSql())) {
            JSONArray jsonArray = JSON.parseArray(dicInfoVo.getOrderSql());
            if (jsonArray != null && jsonArray.size() > 0) {
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    query.addOrder(jsonObject.getString("code"), jsonObject.getString("type"));
                }
            }
        }
        ConditionsWrapper conditionsWrapper = query.buildWrapper();
        if (StringUtil.isNotEmpty(query.buildOrder())) {
            if (conditionsWrapper.getSql().toUpperCase().contains("ORDER BY")) {
                conditionsWrapper.apply("," + query.buildOrder());
            } else {
                conditionsWrapper.apply(" ORDER BY " + query.buildOrder());
            }
        }
        return conditionsWrapper;
    }

    @Override
    public JSONTreeNode buildCompanyTreeData(String nodeInfo, Boolean colorFlag, String... companyIds) {
        //查询所有公司放入map中
        List<DynaBean> companyList = metaService.select("JE_RBAC_COMPANY",
                ConditionsWrapper.builder().eq("SY_STATUS", "1"));
        Map<String, String> companyMap = new HashMap<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
        }
        List<DynaBean> allBeanList;
        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        if (!Strings.isNullOrEmpty(nodeInfo)) {
            rootNode.setId(rootNode.getId() + "_" + nodeInfo);
        }
        rootNode.setNodeInfo(nodeInfo);
        if (companyIds != null && companyIds.length > 0) {
            List<String> companyIdList = Splitter.on(",").splitToList(Joiner.on(",").join(companyIds));
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder()
                    .in("ID", companyIdList).eq("TYPE", "company").eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
            for (DynaBean eachBean : allBeanList) {
                if ("company".equals(eachBean.getStr("TYPE"))) {
                    JSONTreeNode node = buildTreeNode(nodeInfo, rootNode.getId(), eachBean, colorFlag);
                    //查找子级
                    List<DynaBean> childBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1").like("SY_PATH", "%" + eachBean.getStr("ID") + "%").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
                    //递归存放子级
                    recursionDynaBeanTree(node, childBeanList, companyMap, colorFlag);
                    rootNode.getChildren().add(node);
                }
            }
        } else {
            allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
            for (DynaBean eachBean : allBeanList) {
                if ("ROOT".equals(eachBean.getStr("SY_PARENT")) && "company".equals(eachBean.getStr("TYPE"))) {
                    JSONTreeNode node = buildTreeNode(nodeInfo, rootNode.getId(), eachBean, colorFlag);
                    //递归存放子级
                    recursionDynaBeanTree(node, allBeanList, companyMap, colorFlag);
                    rootNode.getChildren().add(node);
                }
            }
        }
        return rootNode;
    }

    @Override
    public JSONTreeNode buildCompanyTreeData(Boolean colorFlag, String... companyIds) {
        return buildCompanyTreeData("", colorFlag, companyIds);
    }

    @Override
    public JSONTreeNode buildAsyncCompanyTreeData(Boolean colorFlag, String rootId) {
        List<DynaBean> companyList = metaService.select("JE_RBAC_COMPANY",
                ConditionsWrapper.builder().eq("SY_STATUS", "1"));
        Map<String, String> companyMap = new HashMap<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
        }

        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        rootNode.setId(rootId);
        List<DynaBean> allBeanList = metaService.select("je_rbac_vcompanydept", ConditionsWrapper.builder().eq("SY_STATUS", "1")
                .eq("SY_PARENT", rootId)
                .apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));

        List<String> parentId = new ArrayList<>();
        for (DynaBean dynaBean : allBeanList) {
            parentId.add(dynaBean.getStr("ID"));
        }

        Map<String, Integer> childCounts = new HashMap<>();
        if (parentId.size() > 0) {
            List<Map<String, Object>> list = metaService.selectSql
                    (String.format("SELECT COUNT(*) COUNT,SY_PARENT FROM  JE_RBAC_VCOMPANYDEPT  WHERE SY_PARENT IN(%s) AND SY_STATUS='1' GROUP BY SY_PARENT",
                            StringUtil.buildArrayToString(parentId)));
            for (Map<String, Object> map : list) {
                int count = 0;
                if (map.get("COUNT") != null) {
                    count = Integer.parseInt(String.valueOf(map.get("COUNT")));
                }
                String id = String.valueOf(map.get("SY_PARENT"));
                childCounts.put(id, count);
            }
        }
        recursionDynaBeanTree(rootNode, allBeanList, companyMap, colorFlag, true, childCounts);
        return rootNode;
    }

    @Override
    public List<DynaBean> findAsyncNodes(String value) {
        List<DynaBean> resultList = new ArrayList<>();
        List<DynaBean> list = metaService.select("JE_RBAC_VCOMPANYDEPT", ConditionsWrapper.builder().like("NAME", value)
                .eq("SY_STATUS", "1"));
        for (DynaBean dynaBean : list) {
            resultList.add(buildResultSearchTreeBean(dynaBean.getStr("ID"), dynaBean.getStr("CODE"), dynaBean.getStr("NAME")
                    , dynaBean.getStr("SY_PATH"), dynaBean.getStr("SY_PARENT")));
        }
        return resultList;
    }

    private DynaBean buildResultSearchTreeBean(String id, String code, String text, String path, String parent) {
        DynaBean dynaBean = new DynaBean();
        dynaBean.setStr("id", id);
        dynaBean.setStr("code", code);
        dynaBean.setStr("text", text);
        dynaBean.setStr("nodePath", path);
        dynaBean.setStr("parent", parent);
        return dynaBean;
    }


    @Override
    public JSONTreeNode buildCompanyTreeDataByQuery(Boolean colorFlag, BaseMethodArgument param, String rootId) {
        return buildCompanyTreeDataByQuery(colorFlag, param, rootId, false);
    }

    @Override
    public JSONTreeNode buildCompanyTreeDataByQuery(Boolean colorFlag, BaseMethodArgument param, String rootId, Boolean async) {
        Query query = param.buildQuery();
        ConditionsWrapper conditionsWrapper = query.buildWrapper();
        //查询所有公司放入map中
        List<DynaBean> companyList = metaService.select("JE_RBAC_COMPANY",
                ConditionsWrapper.builder().eq("SY_STATUS", "1"));
        Map<String, String> companyMap = new HashMap<>();
        for (DynaBean eachBean : companyList) {
            companyMap.put(eachBean.getPkValue(), eachBean.getStr("COMPANY_CODE"));
        }
        List<DynaBean> allBeanList;
        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        if (Strings.isNullOrEmpty(rootId)) {
            allBeanList = metaService.select("je_rbac_vcompanydept", conditionsWrapper.ne("ID", "ROOT")
                    .eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        } else {
            rootNode.setId(rootId);
            rootNode.setNodeInfoType("department");
            allBeanList = metaService.select("je_rbac_vcompanydept", conditionsWrapper.ne("ID", "ROOT").eq("SY_PARENT", rootId)
                    .eq("SY_STATUS", "1").apply("ORDER BY SY_TREEORDERINDEX ASC,SY_ORDERINDEX ASC"));
        }

        List<String> parentId = new ArrayList<>();
        List<String> departmentId = new ArrayList<>();
        Map<String, Integer> childCounts = new HashMap<>();
        //遍历所有公司
        if (async) {
            for (DynaBean eachBean : allBeanList) {
                if (!Strings.isNullOrEmpty(eachBean.getStr("TYPE")) && eachBean.getStr("TYPE").equals("department")) {
                    departmentId.add(eachBean.getStr("ID"));
                }
                parentId.add(eachBean.getStr("ID"));
            }
        }

        if (async) {
            if (parentId.size() > 0) {
                List<Map<String, Object>> list = metaService.selectSql
                        (String.format("SELECT COUNT(*) COUNT,SY_PARENT FROM  je_rbac_vcompanydept  WHERE SY_PARENT IN(%s) AND SY_STATUS='1' GROUP BY SY_PARENT",
                                StringUtil.buildArrayToString(parentId)));
                for (Map<String, Object> map : list) {
                    int count = 0;
                    if (map.get("COUNT") != null) {
                        count = Integer.parseInt(String.valueOf(map.get("COUNT")));
                    }
                    String id = String.valueOf(map.get("SY_PARENT"));
                    childCounts.put(id, count);
                }
            }

            if (departmentId.size() > 0) {
                List<Map<String, Object>> list = metaService.selectSql
                        (String.format("SELECT COUNT(*) COUNT,DEPARTMENT_ID FROM  JE_RBAC_VUSERQUERY  WHERE DEPARTMENT_ID IN(%s) AND USER_STATUS='1' GROUP BY DEPARTMENT_ID",
                                StringUtil.buildArrayToString(departmentId)));
                for (Map<String, Object> map : list) {
                    int count = 0;
                    if (map.get("COUNT") != null) {
                        count = Integer.parseInt(String.valueOf(map.get("COUNT")));
                    }
                    String id = String.valueOf(map.get("DEPARTMENT_ID"));
                    childCounts.put(id, count);
                }
            }
        }

        //遍历所有公司
        for (DynaBean eachBean : allBeanList) {
            if ("ROOT".equals(eachBean.getStr("SY_PARENT")) && "company".equals(eachBean.getStr("TYPE"))) {
                JSONTreeNode node = buildTreeNode("", rootNode.getId(), eachBean, colorFlag, async, childCounts);
                rootNode.getChildren().add(node);
            }
        }


        for (DynaBean eachBean : allBeanList) {
            if (rootNode.getChildren().size() > 0) {
                boolean isHave = false;
                for (JSONTreeNode treeNode : rootNode.getChildren()) {
                    if (String.valueOf(eachBean.get("SY_PATH")).contains(treeNode.getNodePath()) || treeNode.getId().equals(eachBean.get("ID"))) {
                        isHave = true;
                        break;
                    }
                }
                if (!isHave) {
                    JSONTreeNode node = buildTreeNode("", rootNode.getId(), eachBean, colorFlag, async, childCounts);
                    rootNode.getChildren().add(node);
                }
            } else {
                JSONTreeNode node = buildTreeNode("", rootNode.getId(), eachBean, colorFlag, async, childCounts);
                rootNode.getChildren().add(node);
            }
        }

        for (JSONTreeNode node : rootNode.getChildren()) {
            //递归存放子级
            recursionDynaBeanTree(node, allBeanList, companyMap, colorFlag);
        }

        return rootNode;
    }

    /**
     * 解析公司部门视图信息为treeNode
     *
     * @param parentNodeId
     * @param bean
     * @param colorFlag    是否设置 颜色
     */
    public JSONTreeNode buildTreeNode(String nodeInfo, String parentNodeId, DynaBean bean, Boolean colorFlag, boolean async, Map<String, Integer> childCounts) {
        JSONTreeNode node = new JSONTreeNode();
        node.setParent(parentNodeId);
        node.setId(bean.getStr("ID"));
        if (!Strings.isNullOrEmpty(nodeInfo)) {
            node.setId(bean.getStr("ID") + "_" + nodeInfo);
        }
        if (childCounts != null && childCounts.containsKey(node.getId()) && childCounts.get(node.getId()) > 0) {
            node.setLeaf(false);
            node.setNodeType("GENERAL");
        } else {
            node.setLeaf(true);
            node.setNodeType("LEAF");
        }
        node.setCode(bean.getStr("CODE"));
        node.setText(bean.getStr("NAME"));
        node.setNodeInfoType(bean.getStr("TYPE"));
        node.setIcon(bean.getStr("ICON"));
        node.setNodeInfo(nodeInfo);
        if (colorFlag) {
            if (IconType.TCONTYPE_COMPANY.getVal().equals(bean.getStr("ICON"))) {
                node.setIconColor(IconType.TCONTYPE_COMPANY.getIconColor());
            } else if (IconType.TCONTYPE_GROUP.getVal().equals(bean.getStr("ICON"))) {
                node.setIconColor(IconType.TCONTYPE_GROUP.getIconColor());
            } else if (IconType.TCONTYPE_DEPART.getVal().equals(bean.getStr("ICON"))) {
                node.setIconColor(IconType.TCONTYPE_DEPART.getIconColor());
            }
        }
        node.setLayer(bean.getStr("SY_LAYER"));
        node.setTreeOrderIndex(bean.getStr("SY_TREEORDERINDEX"));
        node.setOrderIndex(bean.getStr("SY_ORDERINDEX"));
        node.setNodePath(bean.getStr("SY_PATH"));
        node.setBean(bean.getValues());
        node.setAsync(async);
        return node;
    }

    public JSONTreeNode buildTreeNode(String nodeInfo, String parentNodeId, DynaBean bean, Boolean colorFlag) {
        return buildTreeNode(nodeInfo, parentNodeId, bean, colorFlag, false, null);
    }

    /***
     * 递归修改树形属性
     */
    private void recursionDynaBeanTree(JSONTreeNode parentNode, List<DynaBean> childBeanList, Map<String, String> companyMap,
                                       Boolean colorFlag, Boolean async, Map<String, Integer> childCounts) {
        if (childBeanList == null || childBeanList.isEmpty()) {
            return;
        }
        for (DynaBean eachBean : childBeanList) {
            if (parentNode.getId().equals(eachBean.getStr("SY_PARENT")) ||
                    (!Strings.isNullOrEmpty(parentNode.getNodeInfo()) && parentNode.getId().equals(eachBean.getStr("SY_PARENT") + "_" + parentNode.getNodeInfo()))) {
                JSONTreeNode node = buildTreeNode(parentNode.getNodeInfo(), parentNode.getId(), eachBean, colorFlag, async, childCounts);
                if (eachBean.getStr("TYPE").equals("department")) {
                    node.getBean().put("COMPANY_CODE", companyMap.get(eachBean.getStr("SY_COMPANY_ID")));
                }
                recursionDynaBeanTree(node, childBeanList, companyMap, colorFlag, async, childCounts);
                parentNode.getChildren().add(node);
            }
        }
    }

    private void recursionDynaBeanTree(JSONTreeNode parentNode, List<DynaBean> childBeanList, Map<String, String> companyMap, Boolean colorFlag) {
        recursionDynaBeanTree(parentNode, childBeanList, companyMap, colorFlag, false, null);
    }

    private void recursiveCompanyJsonTreeNode(JSONTreeNode template, JSONTreeNode companyNode, List<DynaBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }

        for (DynaBean eachBean : list) {
            if (Strings.isNullOrEmpty(eachBean.getStr("SY_COMPANY_ID"))) {
                continue;
            }
            if (eachBean.getStr("SY_COMPANY_ID").equals(companyNode.getId()) && "ROOT".equals(eachBean.getStr("SY_PARENT"))) {
                JSONTreeNode departmentNode = buildTreeNode(template, companyNode.getId(), eachBean);
                departmentNode.getBean().put("COMPANY_CODE", companyNode.getBean().get("COMPANY_CODE"));
                recursiveDepartmentJsonTreeNode(template, departmentNode, list);
                companyNode.getChildren().add(departmentNode);
            }
        }

        if (companyNode.getChildren() == null || companyNode.getChildren().isEmpty()) {
            return;
        }

        for (JSONTreeNode eachCompanyNode : companyNode.getChildren()) {
            recursiveCompanyJsonTreeNode(template, eachCompanyNode, list);
        }

    }

    private void recursiveDepartmentJsonTreeNode(JSONTreeNode template, JSONTreeNode rootNode, List<DynaBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }

        JSONTreeNode treeNode;
        for (DynaBean eachBean : list) {
            if (rootNode.getId().equals(eachBean.getStr("SY_PARENT"))) {
                treeNode = buildTreeNode(template, rootNode.getId(), eachBean);
                treeNode.getBean().put("COMPANY_CODE", rootNode.getBean().get("COMPANY_CODE"));
                rootNode.getChildren().add(treeNode);
            }
        }

        if (rootNode.getChildren() == null || rootNode.getChildren().isEmpty()) {
            rootNode.setLeaf(true);
            return;
        }

        rootNode.setLeaf(false);

        for (JSONTreeNode eachNode : rootNode.getChildren()) {
            recursiveDepartmentJsonTreeNode(template, eachNode, list);
        }

    }

    /**
     * 构建树形节点
     *
     * @param template     模板
     * @param parentNodeId 父节点ID
     * @param bean         当前bean
     * @return
     */
    private JSONTreeNode buildTreeNode(JSONTreeNode template, String parentNodeId, DynaBean bean) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(bean.getPkValue());
        node.setParent(parentNodeId);
        node.setLeaf(true);
        if (StringUtil.isNotEmpty(template.getText())) {
            node.setText(bean.getStr(template.getText()));
        }
        if (StringUtil.isNotEmpty(template.getCode())) {
            node.setCode(bean.getStr(template.getCode()));
        }
        //节点信息
        node.setNodeInfo(bean.getStr(template.getCode()));
        //节点信息类型
        node.setNodeInfoType("department");
        //节点类型
        if (StringUtil.isNotEmpty(template.getNodeType())) {
            node.setNodeType(bean.getStr(template.getNodeType()));
        }
        //节点路径
        if (StringUtil.isNotEmpty(template.getNodePath())) {
            node.setNodePath(bean.getStr(template.getNodePath()));
        }
        //是否禁用
        if (StringUtil.isNotEmpty(template.getDisabled())) {
            node.setDisabled(bean.getStr(template.getDisabled()));
        }
        //树形排序
        if (StringUtil.isNotEmpty(template.getTreeOrderIndex())) {
            node.setTreeOrderIndex(bean.getStr(template.getTreeOrderIndex()));
        }
        //节点类型
        if (StringUtil.isNotEmpty(template.getNodeType())) {
            node.setNodeType(bean.getStr(template.getNodeType()));
        }
        //图标样式
        if (StringUtil.isNotEmpty(template.getIcon())) {
            node.setIcon(bean.getStr(template.getIcon()));
        }
        //是否禁用
        if (StringUtil.isNotEmpty(template.getDisabled())) {
            node.setDisabled(bean.getStr(template.getDisabled()));
        } else {
            node.setDisabled("0");
        }
        //描述
        if (StringUtil.isNotEmpty(template.getDescription())) {
            node.setDescription(bean.getStr(template.getDescription()));
        }
        //排序
        if (StringUtil.isNotEmpty(template.getOrderIndex())) {
            node.setOrderIndex(bean.getStr(template.getOrderIndex()) + "");
        }
        node.setBean(bean.getValues());
        return node;
    }


}
