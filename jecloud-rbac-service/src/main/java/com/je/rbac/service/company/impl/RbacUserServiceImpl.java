/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.company.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.impl.CommonServiceImpl;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.company.RbacUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RbacUserServiceImpl implements RbacUserService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonServiceImpl commonService;


    @Override
    public DynaBean findBaseUserById(String userId) {
        return metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder().eq("JE_RBAC_USER_ID", userId));
    }

    @Override
    public DynaBean findBaseUserByCode(String userCode) {
        return metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder().eq("USER_CODE", userCode));
    }

    @Override
    public DynaBean findBaseUserByName(String userName) {
        return metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder().eq("USER_NAME", userName));
    }

    @Override
    public DynaBean findBaseUserByPhone(String phone) {
        return metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder().eq("USER_PHONE", phone));
    }

    @Override
    public DynaBean findBaseUserByEmail(String email) {
        return metaService.selectOne("JE_RBAC_USER", ConditionsWrapper.builder().eq("USER_MAIL", email));
    }

    @Override
    public List<DynaBean> findByTenantId(String tenantId) {
        return metaService.select("JE_RBAC_USER", ConditionsWrapper.builder().eq("SY_TENANT_ID", tenantId));
    }

    @Override
    public List<DynaBean> findByTenantCode(String tenantCode) {
        return metaService.select("JE_RBAC_USER", ConditionsWrapper.builder().eq("SY_TENANT_CODE", tenantCode));
    }

    @Override
    public List<DynaBean> findByTenantName(String tenantName) {
        return metaService.select("JE_RBAC_USER", ConditionsWrapper.builder().eq("SY_TENANT_NAME", tenantName));
    }

    @Override
    public void updateUserRoleName(String roleId, String roleName) {
        String tableCode = "JE_RBAC_USER";
        String roleNameFiledCode = "USER_ROLE_NAME";
        String roleIdFiledCode = "USER_ROLE_ID";
        String pkCode = "JE_RBAC_USER_ID";
        commonService.updateDictionaryItemNameById(roleId, roleName, tableCode, roleNameFiledCode, roleIdFiledCode, pkCode);
    }

}
