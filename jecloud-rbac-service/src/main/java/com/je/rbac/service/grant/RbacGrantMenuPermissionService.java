/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant;

import com.je.rbac.service.permission.GrantTypeEnum;

/**
 * 授权服务定义
 */
public interface RbacGrantMenuPermissionService {

    /**
     * 保存菜单权限
     *
     * @param roleIds
     * @param addedStrData        增加的权限
     * @param deletedStrData      移除的权限
     */
    void updateRoleMenuPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deletedStrData);

    /**
     * 保存部门菜单权限
     *
     * @param deptIds
     * @param addedStrData        增加的权限
     * @param deletedStrData      移除的权限
     */
    void updateDeptMenuPermissions(GrantTypeEnum grantType,String deptIds, String addedStrData, String deletedStrData);

    /**
     * 保存机构菜单权限
     *
     * @param orgIds
     * @param addedStrData   增加的权限
     * @param deletedStrData 移除的权限
     */
    void updateOrgMenuPermissions(GrantTypeEnum grantType,String orgIds, String addedStrData, String deletedStrData);

    /**
     * 保存权限组菜单权限
     *
     * @param permGroupIds 权限组ID集合
     * @param addedStrData   增加的权限
     * @param deletedStrData 移除的权限
     */
    void updatePermGroupMenuPermissions(GrantTypeEnum grantType,String permGroupIds, String addedStrData, String deletedStrData);

    /**
     * 保存开发组菜单权限
     *
     * @param roleIds
     * @param addedStrData        增加的权限
     * @param deletedStrData      移除的权限
     */
    void updateDevelopMenuPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deletedStrData);

}
