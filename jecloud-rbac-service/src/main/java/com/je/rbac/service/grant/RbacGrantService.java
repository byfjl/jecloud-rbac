/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant;

import com.je.rbac.exception.PermissionException;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;

import java.util.List;
import java.util.Map;

public interface RbacGrantService {

    /**
     * 加载菜单最顶部
     * @return
     */
    List<Map<String,Object>> loadTopMenus(GrantMethodEnum grantMethod,String ids,boolean develop);

    /**
     * 导入功能授权
     * @param strData 功能json
     * @return
     */
    void importFuncs(String strData);

    /**
     * 移除功能授权
     * @param type 权限集合类型
     * @param targetIds 权限集合id列表
     * @param funcIds 功能id列表
     */
    void removeFuncs(String type,String targetIds,String funcIds);

    /**
     * 加载导入的功能
     * @return
     */
    List<Map<String,Object>> loadImportFuncs();

    /**
     * 自己控制权限
     * @param grantMethod 类型
     * @param id
     * @param strData
     */
    void selfControlPerm(GrantTypeEnum grantType,GrantMethodEnum grantMethod, String id, String strData) throws PermissionException;

    /**
     * 恢复部门继承
     * @param grantType 类型
     * @param id 类型
     * @param strData
     */
    void cancelSelfControlPerm(GrantTypeEnum grantType,GrantMethodEnum grantMethod,String id, String strData) throws PermissionException;

}
