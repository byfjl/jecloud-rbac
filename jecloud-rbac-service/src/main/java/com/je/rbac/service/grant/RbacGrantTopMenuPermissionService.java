/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant;

import java.util.List;
import java.util.Map;

public interface RbacGrantTopMenuPermissionService {

    /**
     * 查找用户顶部菜单权限
     * @param userId
     * @return
     */
    List<Map<String,Object>> findUserTopMenuPermissions(String userId);

    /**
     * 可见人员
     * @param menuCode
     * @param userIdList
     */
    void updateTopMenuSeeUsers(String menuCode, List<String> userIdList);

    /**
     * 删除可见人员
     * @param menuCode
     * @param userIdList
     */
    void removeTopMenuSeeUsers(String menuCode, List<String> userIdList);

    /**
     * 可见角色
     * @param menuCode
     * @param roleIdList
     */
    void updateTopMenuSeeRoles(String menuCode,List<String> roleIdList);

    /**
     * 删除可见角色
     * @param menuCode
     * @param roleIdList
     */
    void removeTopMenuSeeRoles(String menuCode,List<String> roleIdList);

    /**
     * 可见部门
     * @param menuCode
     * @param deptIdList
     */
    void updateTopMenuSeeDepts(String menuCode,List<String> deptIdList);

    /**
     * 删除可见部门
     * @param menuCode
     * @param deptIdList
     */
    void remoeveTopMenuSeeDepts(String menuCode,List<String> deptIdList);

    /**
     * 可见机构
     * @param menuCode
     * @param orgIdList
     */
    void updateTopMenuSeeOrgs(String menuCode,List<String> orgIdList);

    /**
     * 删除可见机构
     * @param menuCode
     * @param orgIdList
     */
    void removeTopMenuSeeOrgs(String menuCode,List<String> orgIdList);
}
