/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.department;

import com.je.core.entity.extjs.JSONTreeNode;

import java.util.List;
import java.util.Map;

public interface RbacGrantDepartmentService {

    /**
     * 移除部门角色账号
     * @param roleId
     * @param departmentId
     * @param accountId
     */
    long removeDepartmetnAccountRole(String roleId,String departmentId,String accountId);

    /**
     * 移除账户部门
     * @param departmentId
     * @param accountId
     */
    void removeDepartmentAccount(String departmentId,String accountId);

    /**
     * 查找角色人员
     * @param roleIds
     * @return
     */
    List<Map<String,Object>> findRoleUsers(String roleIds);

    /**
     * 构建公司部门人员树--账号部门
     * @param companyIds
     * @return
     */
    JSONTreeNode buildCompanyDepartmentUserTree(Boolean multiple,String... companyIds);

    /**
     * 构建公司部门人员树--账号部门
     * @param multiple
     * @param addOwn 是否显示自己
     * @param companyIds
     * @return
     */
    JSONTreeNode buildCompanyDepartmentUserTreeAddOwn(Boolean multiple, Boolean addOwn, String userId, String... companyIds);

    /**
     * 构建公司部门人员树
     * @param companyIds
     * @return
     */
    JSONTreeNode buildCompanyDepartmentUserOrgStrTree(String... companyIds);

}
