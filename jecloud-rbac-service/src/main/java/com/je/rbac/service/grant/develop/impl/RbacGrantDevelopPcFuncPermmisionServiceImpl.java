/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.develop.impl;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.develop.RbacGrantDevelopPcFuncPermmisionService;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PcFuncBaseService;
import com.je.rbac.service.role.RbacRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class RbacGrantDevelopPcFuncPermmisionServiceImpl implements RbacGrantDevelopPcFuncPermmisionService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private PcFuncBaseService pcFuncBaseService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private RbacRoleService rbacRoleService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void saveDevelopFuncShowPermission(String roleId, String funcCode, GrantTypeEnum grantType) {
        //写入基础权限
        DynaBean funcBasePermBean = pcFuncBaseService.writePcFuncShowPermission(funcCode, true);
        savePermission(roleId,funcBasePermBean,grantType);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void saveDevelopFuncConfigPermission(String roleId, String funcCode, GrantTypeEnum grantType) {
        //写入基础权限
        DynaBean funcBasePermBean = pcFuncBaseService.writePcFuncConfigPermission(funcCode, true);
        savePermission(roleId,funcBasePermBean,grantType);
    }

    private void savePermission(String roleId, DynaBean funcBasePermBean, GrantTypeEnum grantType){
        Set<String> extendRoleIdList = rbacRoleService.findExtendRoleIds(Lists.newArrayList(roleId));
        extendRoleIdList.add(roleId);
        List<DynaBean> associationList = metaService.select("JE_RBAC_ROLEPERM", ConditionsWrapper.builder()
                .eq("ROLEPERM_GRANTTYPE_CODE", grantType.name())
                .in("JE_RBAC_ROLE_ID", extendRoleIdList)
                .eq("JE_RBAC_PERM_ID", funcBasePermBean.getStr("JE_RBAC_PERM_ID")));
        List<String> havedRolePermissionList = new ArrayList<>();
        for (DynaBean eachAssociationBean : associationList) {
            havedRolePermissionList.add(eachAssociationBean.getStr("JE_RBAC_PERM_ID"));
        }

        //写入关联关系
        if (havedRolePermissionList.contains(funcBasePermBean.getStr("JE_RBAC_PERM_ID"))) {
            return;
        }

        DynaBean rolePermBean = new DynaBean("JE_RBAC_ROLEPERM", false);
        rolePermBean.set("JE_RBAC_ROLE_ID", roleId);
        rolePermBean.set("JE_RBAC_PERM_ID", funcBasePermBean.getStr("JE_RBAC_PERM_ID"));
        rolePermBean.set("ROLEPERM_EXCLUDE_CODE", "0");
        rolePermBean.set("ROLEPERM_EXCLUDE_NAME", "否");
        //授权方式
        rolePermBean.set("ROLEPERM_TYPE_CODE", GrantMethodEnum.ROLE.name());
        rolePermBean.set("ROLEPERM_TYPE_NAME", GrantMethodEnum.ROLE.getDesc());
        //授权类型
        rolePermBean.set("ROLEPERM_GRANTTYPE_CODE", grantType.name());
        rolePermBean.set("ROLEPERM_GRANTTYPE_NAME", grantType.getDesc());
        //todo 设置租户信息
        commonService.buildModelCreateInfo(rolePermBean);
        metaService.insert(rolePermBean);
    }

    @Override
    public void removeDevelopFuncPermission(String roleId, String funcCode, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<DynaBean> permissionBaseBeanList = pcFuncBaseService.findPcFuncBasePermission(funcCode, update, delete,true);
        List<String> permIdList = new ArrayList<>();
        for (DynaBean eachBean : permissionBaseBeanList) {
            permIdList.add(eachBean.getStr("JE_RBAC_PERM_ID"));
        }
        metaService.delete("JE_RBAC_ROLEPERM", ConditionsWrapper.builder()
                .eq("JE_RBAC_ROLE_ID", roleId)
                .in("JE_RBAC_PERM_ID", permIdList)
                .eq("ROLEPERM_GRANTTYPE_CODE", grantType.name()));
    }

    @Override
    public List<DynaBean> findDevelopFuncPermission(String roleId, String funcCode, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<String> operatorList = Lists.newArrayList(PermissionOperationTypeEnum.SHOW.getCode());
        if (update) {
            operatorList.add(PermissionOperationTypeEnum.UPDATE.getCode());
        }
        if (delete) {
            operatorList.add(PermissionOperationTypeEnum.DELETE.getCode());
        }
        operatorList.add(PermissionOperationTypeEnum.CONFIG.getCode());
        return metaService.select("JE_RBAC_VROLEPERM", ConditionsWrapper.builder()
                .eq("ROLEPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_ROLE_ID", roleId)
                .eq("PERM_OBJECT", funcCode)
                .in("PERM_TYPE_CODE", Lists.newArrayList(
                        PermissionTypeEnum.FUNC_PC_BASE.name(),
                        PermissionTypeEnum.BUTTON_PC.name()
                )).in("PERM_OPERATE_CODE", operatorList));
    }

}
