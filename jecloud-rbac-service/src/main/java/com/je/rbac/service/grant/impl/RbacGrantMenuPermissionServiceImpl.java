/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.func.MetaFuncRpcService;
import com.je.rbac.service.grant.RbacGrantMenuPermissionService;
import com.je.rbac.service.grant.department.*;
import com.je.rbac.service.grant.develop.*;
import com.je.rbac.service.grant.org.*;
import com.je.rbac.service.grant.permgroup.*;
import com.je.rbac.service.grant.role.*;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.PermissionLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.je.rbac.service.grant.impl.DevelopPlatformServiceImpl.DEV_KEY;

@Service
public class RbacGrantMenuPermissionServiceImpl implements RbacGrantMenuPermissionService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaResourceService metaResourceService;

    //-----------------菜单-----------------
    @Autowired
    private RbacGrantRoleMenuPermmisionService rbacGrantRoleMenuPermmisionService;
    @Autowired
    private RbacGrantDepartmentMenuPermmisionService rbacGrantDepartmentMenuPermmisionService;
    @Autowired
    private RbacGrantOrgMenuPermmisionService rbacGrantOrgMenuPermmisionService;
    @Autowired
    private RbacGrantDevelopMenuPermmisionService rbacGrantDevelopMenuPermmisionService;
    @Autowired
    private RbacGrantPermGroupMenuPermmisionService rbacGrantPermGroupMenuPermmisionService;

    //-----------------插件-----------------
    @Autowired
    private RbacGrantRolePluginPermissionService rbacGrantRolePluginPermissionService;
    @Autowired
    private RbacGrantDepartmentPluginPermissionService rbacGrantDepartmentPluginPermissionService;
    @Autowired
    private RbacGrantOrgPluginPermissionService rbacGrantOrgPluginPermissionService;
    @Autowired
    private RbacGrantDevelopPluginPermissionService rbacGrantDevelopPluginPermissionService;
    @Autowired
    private RbacGrantPermGroupPluginPermissionService rbacGrantPermGroupPluginPermissionService;

    //-----------------功能-----------------
    @Autowired
    private RbacGrantRolePcFuncPermmisionService rbacGrantRolePcFuncPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcFuncPermmisionService rbacGrantDepartmentPcFuncPermmisionService;
    @Autowired
    private RbacGrantOrgPcFuncPermmisionService rbacGrantOrgPcFuncPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcFuncPermmisionService rbacGrantPermGroupPcFuncPermmisionService;
    @Autowired
    private RbacGrantDevelopPcFuncPermmisionService rbacGrantDevelopPcFuncPermmisionService;

    //-----------------子功能-----------------
    @Autowired
    private RbacGrantRolePcSubFuncPermmisionService rbacGrantRolePcSubFuncPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcSubFuncPermmisionService rbacGrantDepartmentPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantOrgPcSubFuncPermmisionService rbacGrantOrgPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcSubFuncPermmisionService rbacGrantPermGroupPcSubFuncPermmisionService;
    @Autowired
    private RbacGrantDevelopPcSubFuncPermmisionService rbacGrantDevelopPcSubFuncPermmisionService;

    //-----------------功能按钮-----------------
    @Autowired
    private RbacGrantRolePcFuncButtonPermmisionService grantRolePcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantDepartmentPcFuncButtonPermmisionService grantDepartmentPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantOrgPcFuncButtonPermmisionService grantOrgPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantPermGroupPcFuncButtonPermmisionService grantPermGroupPcFuncButtonPermmisionService;
    @Autowired
    private RbacGrantDevelopPcFuncButtonPermmisionService grantDevelopPcFuncButtonPermmisionService;
    @Autowired
    private SystemSettingRpcService systemSettingRpcService;
    @Autowired
    private MetaFuncRpcService metaFuncRpcService;
    @Autowired
    private PermissionLoadService permissionLoadService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateRoleMenuPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deletedStrData) {
        List<String> roleIdList = Splitter.on(",").splitToList(roleIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.ROLE, roleIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deletedStrData)) {
            deleteMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.ROLE, roleIdList, deletedStrData);
        }
        permissionLoadService.reloadRolePermCache(roleIdList, false);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateDeptMenuPermissions(GrantTypeEnum grantType, String deptIds, String addedStrData, String deletedStrData) {
        List<String> deptIdList = Splitter.on(",").splitToList(deptIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.DEPARTMENT, deptIdList, addedStrData);
        }

        //移除的权限
        if (!Strings.isNullOrEmpty(deletedStrData)) {
            deleteMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.DEPARTMENT, deptIdList, deletedStrData);
        }
        permissionLoadService.reloadDepartmentPermCache(deptIdList);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateOrgMenuPermissions(GrantTypeEnum grantType, String orgIds, String addedStrData, String deletedStrData) {
        List<String> orgIdList = Splitter.on(",").splitToList(orgIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.ORG, orgIdList, addedStrData);
        }
        //移除的权限
        if (!Strings.isNullOrEmpty(deletedStrData)) {
            deleteMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.ORG, orgIdList, deletedStrData);
        }
        permissionLoadService.reloadOrgPermCache(orgIdList);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updatePermGroupMenuPermissions(GrantTypeEnum grantType, String permGroupIds, String addedStrData, String deletedStrData) {
        List<String> permGroupIdList = Splitter.on(",").splitToList(permGroupIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.PERM_GROUP, permGroupIdList, addedStrData);
        }
        //移除的权限
        if (!Strings.isNullOrEmpty(deletedStrData)) {
            deleteMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.PERM_GROUP, permGroupIdList, deletedStrData);
        }
        permissionLoadService.reloadPermGroupPermCache(permGroupIdList, true);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void updateDevelopMenuPermissions(GrantTypeEnum grantType, String roleIds, String addedStrData, String deletedStrData) {
        List<String> roleIdList = Splitter.on(",").splitToList(roleIds);
        //增加的权限
        if (!Strings.isNullOrEmpty(addedStrData)) {
            addMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.DEVELOP, roleIdList, addedStrData);
        }
        //移除的权限
        if (!Strings.isNullOrEmpty(deletedStrData)) {
            deleteMenuAndFuncPermissionStrData(grantType, GrantMethodEnum.DEVELOP, roleIdList, deletedStrData);
        }
        permissionLoadService.reloadDevelopRolePermCache(roleIdList);
    }

    private String getFuncCode(DynaBean funcBean) {
        if (funcBean == null) {
            return null;
        }
        return funcBean.getStr("FUNCINFO_FUNCCODE");
    }

    private void addMenuAndFuncPermissionStrData(GrantTypeEnum grantType, GrantMethodEnum grantMethod, List<String> idList, String strData) {
        Map<String, JSONObject> subFuncCodeMap = filterSubFuncsFromPermissionStrData(strData);
        Map<String, DynaBean> subFuncIdAndFuncMap = metaFuncRpcService.findSubFuncParentFuncInfos(subFuncCodeMap.keySet().stream().collect(Collectors.toList()));
        if (GrantMethodEnum.ROLE.equals(grantMethod)) {
            for (String eachId : idList) {
                Map<DynaBean, String> menuBeanList = filterMenusFromPermissionStrDataWithCheck(strData);
                Map<String, String> menuIdMap = new HashMap<>();
                List<String> pluginCodeList = new ArrayList<>();
                for (Map.Entry<DynaBean, String> eachEntry : menuBeanList.entrySet()) {
                    menuIdMap.put(eachEntry.getKey().getStr("JE_CORE_MENU_ID"), eachEntry.getValue());
                    if (!"MENU".equals(eachEntry.getKey().getStr("MENU_NODEINFOTYPE"))
                            && !"MT".equals(eachEntry.getKey().getStr("MENU_NODEINFOTYPE"))) {
                        pluginCodeList.add(eachEntry.getKey().getStr("MENU_NODEINFO"));
                    }
                }

                Map<String, String> granFuncCodeMap = filterFuncsFromPermissionStrDataWithCheck(strData);
                Map<String, Map<String, String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrDataWithCheck(strData);
                //插件权限
                rbacGrantRolePluginPermissionService.saveRolePluginPermissions(eachId, pluginCodeList, grantType);
                //菜单权限
                rbacGrantRoleMenuPermmisionService.saveRoleMenuPermissions(eachId, menuIdMap, grantType);
                //功能权限
                for (String eachFuncCode : granFuncCodeMap.keySet()) {
                    rbacGrantRolePcFuncPermmisionService.saveRoleFuncShowPermission(eachId, eachFuncCode, granFuncCodeMap.get(eachFuncCode), grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantRolePcSubFuncPermmisionService.saveRoleSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), eachEntry.getValue().getString("checked"), grantType);
                }
                //功能按钮权限
                for (Map.Entry<String, Map<String, String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    grantRolePcFuncButtonPermmisionService.saveRoleFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }
        } else if (GrantMethodEnum.DEPARTMENT.equals(grantMethod)) {
            for (String eachId : idList) {
                Map<DynaBean, String> menuBeanList = filterMenusFromPermissionStrDataWithCheck(strData);
                Map<String, String> menuIdMap = new HashMap<>();
                List<String> pluginCodeList = new ArrayList<>();
                for (Map.Entry<DynaBean, String> eachEntry : menuBeanList.entrySet()) {
                    menuIdMap.put(eachEntry.getKey().getStr("JE_CORE_MENU_ID"), eachEntry.getValue());
                    if (!"MENU".equals(eachEntry.getKey().getStr("MENU_NODEINFOTYPE"))
                            && !"MT".equals(eachEntry.getKey().getStr("MENU_NODEINFOTYPE"))) {
                        pluginCodeList.add(eachEntry.getKey().getStr("MENU_NODEINFO"));
                    }
                }

                Map<String, String> granFuncCodeMap = filterFuncsFromPermissionStrDataWithCheck(strData);
                Map<String, Map<String, String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrDataWithCheck(strData);
                //插件权限
                rbacGrantDepartmentPluginPermissionService.saveDepartmentPluginPermissions(eachId, pluginCodeList, grantType);
                //菜单权限
                rbacGrantDepartmentMenuPermmisionService.saveDeptMenuPermission(eachId, menuIdMap, grantType);
                //功能权限
                for (String eachFuncCode : granFuncCodeMap.keySet()) {
                    rbacGrantDepartmentPcFuncPermmisionService.saveDeptFuncShowPermission(eachId, eachFuncCode, granFuncCodeMap.get(eachFuncCode), grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDepartmentPcSubFuncPermmisionService.saveDeptSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), eachEntry.getValue().getString("checked"), grantType);
                }
                //功能按钮权限
                for (Map.Entry<String, Map<String, String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    grantDepartmentPcFuncButtonPermmisionService.saveDeptFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }
        } else if (GrantMethodEnum.ORG.equals(grantMethod)) {
            for (String eachId : idList) {
                List<DynaBean> menuBeanList = filterMenusFromPermissionStrData(strData);
                List<String> menuIdList = new ArrayList<>();
                List<String> pluginCodeList = new ArrayList<>();
                for (DynaBean eachMenuBean : menuBeanList) {
                    menuIdList.add(eachMenuBean.getStr("JE_CORE_MENU_ID"));
                    if (!"MENU".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))
                            && !"MT".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))) {
                        pluginCodeList.add(eachMenuBean.getStr("MENU_NODEINFO"));
                    }
                }
                List<String> granFuncCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                //插件权限
                rbacGrantOrgPluginPermissionService.saveOrgPluginPermissions(eachId, pluginCodeList, grantType);
                //菜单权限
                rbacGrantOrgMenuPermmisionService.saveOrgMenuPermission(eachId, menuIdList, grantType);
                //功能权限
                for (String eachFuncCode : granFuncCodeList) {
                    rbacGrantOrgPcFuncPermmisionService.saveOrgFuncShowPermission(eachId, eachFuncCode, grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantOrgPcSubFuncPermmisionService.saveOrgSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType);
                }
                //功能按钮权限
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    grantOrgPcFuncButtonPermmisionService.saveOrgFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }
        } else if (GrantMethodEnum.PERM_GROUP.equals(grantMethod)) {
            for (String eachId : idList) {
                List<DynaBean> menuBeanList = filterMenusFromPermissionStrData(strData);
                List<String> menuIdList = new ArrayList<>();
                List<String> pluginCodeList = new ArrayList<>();
                for (DynaBean eachMenuBean : menuBeanList) {
                    menuIdList.add(eachMenuBean.getStr("JE_CORE_MENU_ID"));
                    if (!"MENU".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))
                            && !"MT".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))) {
                        pluginCodeList.add(eachMenuBean.getStr("MENU_NODEINFO"));
                    }
                }
                List<String> granFuncCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                rbacGrantPermGroupPluginPermissionService.savePermGroupPluginPermissions(eachId, pluginCodeList, grantType);
                rbacGrantPermGroupMenuPermmisionService.savePermGroupMenuPermission(eachId, menuIdList, grantType);
                for (String eachFuncCode : granFuncCodeList) {
                    rbacGrantPermGroupPcFuncPermmisionService.savePermGroupFuncShowPermission(eachId, eachFuncCode, grantType);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantPermGroupPcSubFuncPermmisionService.savePermGroupSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType);
                }
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    grantPermGroupPcFuncButtonPermmisionService.savePermGroupFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }
        } else if (GrantMethodEnum.DEVELOP.equals(grantMethod)) {
            List<DynaBean> roleList = metaService.select("JE_RBAC_ROLE", ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID", idList));
            String platformDevelopRoleId = systemSettingRpcService.findSettingValue(DEV_KEY);
            for (DynaBean eachBean : roleList) {
                String eachId = eachBean.getStr("JE_RBAC_ROLE_ID");
                List<DynaBean> platformProductBeanList = metaResourceService.selectByTableCodeAndNativeQuery("JE_PRODUCT_MANAGE", NativeQuery.build()
                        .eq("PRODUCT_TYPE", "2"));
                List<String> productIdList = new ArrayList<>();
                for (DynaBean eachProductBean : platformProductBeanList) {
                    productIdList.add(eachProductBean.getStr("JE_PRODUCT_MANAGE_ID"));
                }
                List<DynaBean> menuBeanList = filterMenusFromPermissionStrData(strData);
                List<String> menuIdList = new ArrayList<>();
                Map<String, Boolean> pluginMap = new HashMap<>();
                for (DynaBean eachMenuBean : menuBeanList) {
                    menuIdList.add(eachMenuBean.getStr("JE_CORE_MENU_ID"));
                    //不是功能和菜单需要配置插件权限
                    if (!"MENU".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))
                            && !"MT".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))) {
                        pluginMap.put(eachMenuBean.getStr("MENU_NODEINFO"), productIdList.contains(eachMenuBean.getStr("SY_PRODUCT_ID")));
                    }
                }
                List<String> granFuncCodeList = filterFuncsFromPermissionStrData(strData);
                Map<String, String> productMap = metaFuncRpcService.findFuncProductMap(granFuncCodeList);
                Map<String, List<String>> grantFuncButtonMap = filterFuncButtonsFromPermissionStrData(strData);
                rbacGrantDevelopPluginPermissionService.saveDevelopPluginPermissions(eachId, pluginMap, grantType);
                rbacGrantDevelopMenuPermmisionService.saveDevelopMenuPermissions(eachId, menuIdList, grantType);
                for (String eachFuncCode : granFuncCodeList) {
                    rbacGrantDevelopPcFuncPermmisionService.saveDevelopFuncShowPermission(eachId, eachFuncCode, grantType);
                    if (eachId.equals(platformDevelopRoleId)
                            || (!Strings.isNullOrEmpty(eachBean.getStr("SY_PRODUCT_ID")) && eachBean.getStr("SY_PRODUCT_ID").equals(productMap.get(eachFuncCode)))) {
                        rbacGrantDevelopPcFuncPermmisionService.saveDevelopFuncConfigPermission(eachId, eachFuncCode, grantType);
                    }
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantRolePcSubFuncPermmisionService.saveRoleSubFuncShowPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), eachEntry.getValue().getString("checked"), grantType);
                }
                for (Map.Entry<String, List<String>> eachEntry : grantFuncButtonMap.entrySet()) {
                    grantDevelopPcFuncButtonPermmisionService.saveDevelopFuncButtonShowPermission(eachId, eachEntry.getKey(), eachEntry.getValue(), grantType);
                }
            }
        }
    }

    /**
     * 移除菜单和功能权限
     *
     * @param grantType
     * @param grantMethod
     * @param idList
     * @param strData
     */
    private void deleteMenuAndFuncPermissionStrData(GrantTypeEnum grantType, GrantMethodEnum grantMethod, List<String> idList, String strData) {
        List<DynaBean> menuBeanList = filterMenusFromPermissionStrData(strData);
        List<String> menuIdList = new ArrayList<>();
        List<String> pluginCodeList = new ArrayList<>();
        for (DynaBean eachMenuBean : menuBeanList) {
            menuIdList.add(eachMenuBean.getStr("JE_CORE_MENU_ID"));
            if (!"MENU".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))
                    && !"MT".equals(eachMenuBean.getStr("MENU_NODEINFOTYPE"))) {
                pluginCodeList.add(eachMenuBean.getStr("MENU_NODEINFO"));
            }
        }
        List<String> funcCodeList = filterFuncsFromPermissionStrData(strData);
        Map<String, JSONObject> subFuncCodeMap = filterSubFuncsFromPermissionStrData(strData);
        Map<String, DynaBean> subFuncIdAndFuncMap = metaFuncRpcService.findSubFuncParentFuncInfos(subFuncCodeMap.keySet().stream().collect(Collectors.toList()));
        List<String> funcButtonPermIdList = filterDeleteFuncButtonsFromPermissionStrData(strData);
        for (String eachId : idList) {
            if (GrantMethodEnum.ROLE.equals(grantMethod)) {
                //插件
                rbacGrantRolePluginPermissionService.removeRolePluginPermissions(eachId, pluginCodeList, grantType);
                //菜单
                rbacGrantRoleMenuPermmisionService.removeRoleMenuPermissions(eachId, menuIdList, grantType, true, true);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantRolePcFuncPermmisionService.removeRoleFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantRolePcSubFuncPermmisionService.removeRoleSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //按钮
                grantRolePcFuncButtonPermmisionService.removeRoleFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.DEPARTMENT.equals(grantMethod)) {
                //插件
                rbacGrantDepartmentPluginPermissionService.removeDepartmentPluginPermissions(eachId, pluginCodeList, grantType);
                //菜单
                rbacGrantDepartmentMenuPermmisionService.removeDeptMenuPermission(eachId, menuIdList, grantType, true, true);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantDepartmentPcFuncPermmisionService.removeDeptFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDepartmentPcSubFuncPermmisionService.removeDeptSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                grantDepartmentPcFuncButtonPermmisionService.removeDeptFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.ORG.equals(grantMethod)) {
                //插件
                rbacGrantOrgPluginPermissionService.removeOrgPluginPermissions(eachId, pluginCodeList, grantType);
                //菜单
                rbacGrantOrgMenuPermmisionService.removeOrgMenuPermission(eachId, menuIdList, grantType, true, true);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantOrgPcFuncPermmisionService.removeOrgFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantOrgPcSubFuncPermmisionService.removeOrgSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                grantOrgPcFuncButtonPermmisionService.removeOrgFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.PERM_GROUP.equals(grantMethod)) {
                //插件
                rbacGrantPermGroupPluginPermissionService.removePermGroupPluginPermissions(eachId, funcCodeList, grantType);
                //菜单
                rbacGrantPermGroupMenuPermmisionService.removePermGroupMenuPermission(eachId, menuIdList, grantType, true, true);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantPermGroupPcFuncPermmisionService.removePermGroupFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantPermGroupPcSubFuncPermmisionService.removePermGroupSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                grantPermGroupPcFuncButtonPermmisionService.removePermGroupFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            } else if (GrantMethodEnum.DEVELOP.equals(grantMethod)) {
                //插件
                rbacGrantDevelopPluginPermissionService.removeDevelopPluginPermissions(eachId, pluginCodeList, grantType);
                //菜单
                rbacGrantDevelopMenuPermmisionService.removeDevelopMenuPermissions(eachId, menuIdList, grantType, true, true);
                //功能
                for (String eachFuncCode : funcCodeList) {
                    rbacGrantDevelopPcFuncPermmisionService.removeDevelopFuncPermission(eachId, eachFuncCode, grantType, true, true);
                }
                //子功能
                for (Map.Entry<String, JSONObject> eachEntry : subFuncCodeMap.entrySet()) {
                    rbacGrantDevelopPcSubFuncPermmisionService.removeDevelopSubFuncPermission(eachId, getFuncCode(subFuncIdAndFuncMap.get(eachEntry.getKey())), eachEntry.getValue().getString("nodeInfo"), grantType, true, true);
                }
                //功能按钮
                grantDevelopPcFuncButtonPermmisionService.removeDevelopFuncButtonPermission(eachId, funcButtonPermIdList, grantType);
            }
        }
    }

    private Map<DynaBean, String> filterMenusFromPermissionStrDataWithCheck(String strData) {
        Map<String, String> menuIdCheckMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            menuIdCheckMap.put(strJsonArray.getJSONObject(i).getString("id"),
                    strJsonArray.getJSONObject(i).containsKey("checked") ? strJsonArray.getJSONObject(i).getString("checked") : null);
        }
        List<DynaBean> menuBeanList = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder()
                .selectColumns("JE_CORE_MENU_ID,MENU_CODE,MENU_MENUNAME")
                .in("JE_CORE_MENU_ID", menuIdCheckMap.keySet()));
        Map<DynaBean, String> result = new HashMap<>();
        for (DynaBean eachBean : menuBeanList) {
            result.put(eachBean, menuIdCheckMap.get(eachBean.getStr("JE_CORE_MENU_ID")));
        }
        return result;
    }

    private List<DynaBean> filterMenusFromPermissionStrData(String strData) {
        List<String> menuIdList = new ArrayList<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if ("MT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType")) || "subFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                menuIdList.add(strJsonArray.getJSONObject(i).getString("id"));
            } else if ("REPORT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                menuIdList.add(strJsonArray.getJSONObject(i).getString("id"));
            } else if ("CHART".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                menuIdList.add(strJsonArray.getJSONObject(i).getString("id"));
            } else {
                menuIdList.add(strJsonArray.getJSONObject(i).getString("id"));
            }
        }
        List<DynaBean> menuBeanList = metaService.select("JE_CORE_MENU", ConditionsWrapper.builder().in("JE_CORE_MENU_ID", menuIdList));
        return menuBeanList;
    }

    private Map<String, String> filterFuncsFromPermissionStrDataWithCheck(String strData) {
        Map<String, String> funcCodeMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if ("MT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType")) || "commonSubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                funcCodeMap.put(strJsonArray.getJSONObject(i).getString("nodeInfo"),
                        strJsonArray.getJSONObject(i).containsKey("checked") ? strJsonArray.getJSONObject(i).getString("checked") : null);
            }
        }
        return funcCodeMap;
    }

    private List<String> filterFuncsFromPermissionStrData(String strData) {
        List<String> funcCodeList = new ArrayList<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if ("MT".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType")) || "commonSubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                funcCodeList.add(strJsonArray.getJSONObject(i).getString("nodeInfo"));
            }
        }
        return funcCodeList;
    }

    /**
     * @param strData
     * @return Map<String, String> key为subFunc的id,value为当前json对象
     */
    private Map<String, JSONObject> filterSubFuncsFromPermissionStrData(String strData) {
        Map<String, JSONObject> funcCodeMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if ("diySubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))
                    || "commonSubFunc".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                funcCodeMap.put(strJsonArray.getJSONObject(i).getString("id"), strJsonArray.getJSONObject(i));
            }
        }
        return funcCodeMap;
    }

    private Map<String, Map<String, String>> filterFuncButtonsFromPermissionStrDataWithCheck(String strData) {
        Map<String, Map<String, String>> resultMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        Map<String, String> buttonCodeMap;
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if (resultMap.containsKey(strJsonArray.getJSONObject(i).getString("funcCode"))) {
                buttonCodeMap = resultMap.get(strJsonArray.getJSONObject(i).getString("funcCode"));
            } else {
                buttonCodeMap = new HashMap<>();
                resultMap.put(strJsonArray.getJSONObject(i).getString("funcCode"), buttonCodeMap);
            }
            buttonCodeMap.put(strJsonArray.getJSONObject(i).getString("nodeInfo"),
                    strJsonArray.getJSONObject(i).containsKey("checked") ? strJsonArray.getJSONObject(i).getString("checked") : null);
        }
        return resultMap;
    }

    private Map<String, List<String>> filterFuncButtonsFromPermissionStrData(String strData) {
        Map<String, List<String>> resultMap = new HashMap<>();
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        List<String> buttonCodeList;
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            if (resultMap.containsKey(strJsonArray.getJSONObject(i).getString("funcCode"))) {
                buttonCodeList = resultMap.get(strJsonArray.getJSONObject(i).getString("funcCode"));
            } else {
                buttonCodeList = new ArrayList<>();
                resultMap.put(strJsonArray.getJSONObject(i).getString("funcCode"), buttonCodeList);
            }
            buttonCodeList.add(strJsonArray.getJSONObject(i).getString("nodeInfo"));
        }
        return resultMap;
    }

    /**
     * 过滤删除的按钮信息
     *
     * @param strData
     * @return
     */
    private List<String> filterDeleteFuncButtonsFromPermissionStrData(String strData) {
        JSONArray strJsonArray = JSONArray.parseArray(strData);
        List<String> buttonPermIdList = new ArrayList<>();
        for (int i = 0; strJsonArray != null && !strJsonArray.isEmpty() && i < strJsonArray.size(); i++) {
            if (!"button".equals(strJsonArray.getJSONObject(i).getString("nodeInfoType"))) {
                continue;
            }
            buttonPermIdList.add(strJsonArray.getJSONObject(i).getString("permId"));
        }
        return buttonPermIdList;
    }

}
