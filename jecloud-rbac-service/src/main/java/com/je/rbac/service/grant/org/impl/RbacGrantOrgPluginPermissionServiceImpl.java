/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.org.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.org.RbacGrantOrgPluginPermissionService;
import com.je.rbac.service.permission.GrantMethodEnum;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.template.PcPluginBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RbacGrantOrgPluginPermissionServiceImpl implements RbacGrantOrgPluginPermissionService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private PcPluginBaseService pcPluginBaseService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void saveOrgPluginPermission(String orgId, String pluginCode, GrantTypeEnum grantType) {
        List<DynaBean> pluginBasePermissionList = new ArrayList<>();
        pluginBasePermissionList.add(pcPluginBaseService.writePcPluginShowPermission(pluginCode, true));
        saveOrgPermAssocaition(orgId, pluginBasePermissionList, grantType);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void removeOrgPluginPermission(String orgId, String pluginCode, GrantTypeEnum grantType) {
        List<String> menuPermissionList = new ArrayList<>();
        menuPermissionList.add(pcPluginBaseService.formatPcPluginShowTemplate(pluginCode));
        List<DynaBean> rolePermList = metaService.select("JE_RBAC_VORGPERM", ConditionsWrapper.builder()
                .eq("ORGPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_ORG_ID", orgId)
                .in("PERM_CODE", menuPermissionList));
        List<String> menuPermissionIdList = new ArrayList<>();
        for (DynaBean eachRolePermBean : rolePermList) {
            menuPermissionIdList.add(eachRolePermBean.getStr("JE_RBAC_ORGPERM_ID"));
        }
        metaService.delete("JE_RBAC_ORGPERM", ConditionsWrapper.builder().in("JE_RBAC_ORGPERM_ID", menuPermissionIdList));
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void saveOrgPluginPermissions(String orgId, List<String> pluginCodeList, GrantTypeEnum grantType) {
        List<DynaBean> allPermmissionBeanList = new ArrayList<>();
        for (String pluginCode : pluginCodeList) {
            allPermmissionBeanList.add(pcPluginBaseService.writePcPluginShowPermission(pluginCode,true));
        }
        saveOrgPermAssocaition(orgId, allPermmissionBeanList, grantType);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void removeOrgPluginPermissions(String orgId, List<String> pluginCodeList, GrantTypeEnum grantType) {
        List<String> menuPermissionList = new ArrayList<>();
        for (String pluginCode : pluginCodeList) {
            menuPermissionList.add(pcPluginBaseService.formatPcPluginShowTemplate(pluginCode));
        }
        List<DynaBean> rolePermList = metaService.select("JE_RBAC_VORGPERM", ConditionsWrapper.builder()
                .eq("ORGPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_ORG_ID", orgId)
                .in("PERM_CODE", menuPermissionList));
        List<String> menuPermissionIdList = new ArrayList<>();
        for (DynaBean eachRolePermBean : rolePermList) {
            menuPermissionIdList.add(eachRolePermBean.getStr("JE_RBAC_ORGPERM_ID"));
        }
        metaService.delete("JE_RBAC_ORGPERM", ConditionsWrapper.builder().in("JE_RBAC_ORGPERM_ID", menuPermissionIdList));
    }

    /**
     * 保存关系
     *
     * @param orgId
     * @param permissionList
     */
    private void saveOrgPermAssocaition(String orgId, List<DynaBean> permissionList, GrantTypeEnum grantType) {
        //查询已存在的关联关系
        List<String> permissionIdList = new ArrayList<>();
        for (DynaBean eachPermissionBean : permissionList) {
            permissionIdList.add(eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
        }
        List<DynaBean> associationList = metaService.select("JE_RBAC_ORGPERM", ConditionsWrapper.builder()
                .eq("ORGPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_ORG_ID", orgId)
                .in("JE_RBAC_PERM_ID", permissionIdList));
        List<String> havedRolePermissionList = new ArrayList<>();
        for (DynaBean eachAssociationBean : associationList) {
            havedRolePermissionList.add(eachAssociationBean.getStr("JE_RBAC_PERM_ID"));
        }
        //写入关联关系
        DynaBean rolePermBean;
        for (DynaBean eachPermissionBean : permissionList) {
            //如果已包含从关联关系，则不做任何操作
            if (havedRolePermissionList.contains(eachPermissionBean.getStr("JE_RBAC_PERM_ID"))) {
                continue;
            }
            rolePermBean = new DynaBean("JE_RBAC_ORGPERM", false);
            rolePermBean.set("JE_RBAC_ORG_ID", orgId);
            rolePermBean.set("JE_RBAC_PERM_ID", eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
            rolePermBean.set("ORGPERM_EXCLUDE_CODE", "0");
            rolePermBean.set("ORGPERM_EXCLUDE_NAME", "否");
            //授权方式
            rolePermBean.set("ORGPERM_TYPE_CODE", GrantMethodEnum.ROLE.name());
            rolePermBean.set("ORGPERM_TYPE_NAME", GrantMethodEnum.ROLE.getDesc());
            //授权类型
            rolePermBean.set("ORGPERM_GRANTTYPE_CODE", grantType.name());
            rolePermBean.set("ORGPERM_GRANTTYPE_NAME", grantType.getDesc());
            //todo 设置租户信息
            commonService.buildModelCreateInfo(eachPermissionBean);
            metaService.insert(rolePermBean);
        }
    }

}
