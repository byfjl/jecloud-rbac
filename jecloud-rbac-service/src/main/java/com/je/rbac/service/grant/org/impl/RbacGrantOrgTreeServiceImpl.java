/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.org.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.org.RbacGrantOrgTreeService;
import com.je.rbac.service.permission.GrantTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RbacGrantOrgTreeServiceImpl implements RbacGrantOrgTreeService {

    @Autowired
    private MetaService metaService;

    /**
     * 机构类型，设置check
     *
     * @param grantType
     * @param orgIds
     * @param rootNode
     */
    @Override
    public void checkWithOrg(GrantTypeEnum grantType, String orgIds, JSONTreeNode rootNode) {
        List<String> orgIdList = Splitter.on(",").splitToList(orgIds);
        //查询授权类型是菜单授权的集合
        List<Map<String, Object>> rolePermMapList = metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VORGPERM")
                .eq("ORGPERM_GRANTTYPE_CODE", grantType.name())
                .in("JE_RBAC_ORG_ID", orgIdList)
                .eq("PERM_OPERATE_CODE", "show")
                .in("PERM_TYPE_CODE", Lists.newArrayList("MENU", "FUNC_PC_BASE", "BUTTON_PC", "SUBFUNC_PC")));
        if (rolePermMapList.size() == 0) {
            return;
        }
        recursiveOrgSetTreeCheck(rootNode, rolePermMapList);
    }

    private void recursiveOrgSetTreeCheck(JSONTreeNode root, List<Map<String, Object>> beanList) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        boolean flag = false;
        for (JSONTreeNode eachChildNode : root.getChildren()) {
            for (Map<String, Object> eachPermedBean : beanList) {
                if (!eachPermedBean.get("PERM_CODE").equals(eachChildNode.getBean().get("PERM_SHOW"))) {
                    continue;
                }
                eachChildNode.setChecked(true);
                eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_ORGPERM_ID"));
                eachChildNode.getBean().put("ORG_ID", eachPermedBean.get("JE_RBAC_ORG_ID"));
                eachChildNode.getBean().put("ORG_NAME", eachPermedBean.get("ORG_NAME"));
                eachChildNode.getBean().put("exclude", false);
                eachChildNode.getBean().put("extend", false);
                flag = true;
            }
            recursiveOrgSetTreeCheck(eachChildNode, beanList);
        }
        if (!flag && root.getBean().containsKey("PERM_ID")) {
            root.setChecked(true);
        } else if (!root.getChecked()) {
            root.setChecked(flag);
        }
    }

}
