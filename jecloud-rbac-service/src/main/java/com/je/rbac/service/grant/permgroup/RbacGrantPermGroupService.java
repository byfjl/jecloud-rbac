/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.permgroup;

import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.dd.DicInfoVo;
import com.je.rbac.exception.PermGroupException;
import com.je.rbac.exception.PermissionException;

import java.util.Map;

public interface RbacGrantPermGroupService {

    /**
     * 保存权限组
     * @param name
     */
    void insertPermGroup(String name);

    /**
     * 更新权限组
     * @param id
     * @param name
     */
    void updatePermGroup(String id,String name) throws PermGroupException;

    /**
     * 移动
     * @param id
     * @param targetIndex
     */
    void move(String id,int targetIndex);

    /**
     * 移除权限组
     * @param id
     */
    void removePermGroup(String id) throws PermGroupException;

    /**
     * 权限字典树
     * @param dicInfoVo
     * @return
     */
    JSONTreeNode buildPermGroupTree(DicInfoVo dicInfoVo);

}
