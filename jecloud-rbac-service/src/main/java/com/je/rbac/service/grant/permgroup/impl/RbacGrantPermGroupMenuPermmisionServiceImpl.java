/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.permgroup.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupMenuPermmisionService;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.permission.template.PcMenuDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RbacGrantPermGroupMenuPermmisionServiceImpl implements RbacGrantPermGroupMenuPermmisionService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private PcMenuDataService pcMenuDataService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void savePermGroupMenuPermission(String permGroupId, String menuId, GrantTypeEnum grantType) {
        //写入基础权限
        List<DynaBean> funcBasePermissionList = new ArrayList<>();
        funcBasePermissionList.add(pcMenuDataService.writeMenuDataShowTemplate(menuId, true));
        savePermGroupPermAssocaition(permGroupId, funcBasePermissionList, grantType);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void removePermGroupMenuPermission(String permGroupId, String menuId, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<String> menuPermissionList = new ArrayList<>();
        menuPermissionList.add(pcMenuDataService.formatMenuDataShowTemplate(menuId));
        if (update) {
            menuPermissionList.add(pcMenuDataService.formatMenuDataUpdateTemplate(menuId));
        }
        if (delete) {
            menuPermissionList.add(pcMenuDataService.formatMenuDataDeleteTemplate(menuId));
        }
        List<DynaBean> rolePermList = metaService.select("JE_RBAC_VPERMGROUP", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId).in("PERM_CODE", menuPermissionList));
        List<String> menuPermissionIdList = new ArrayList<>();
        for (DynaBean eachRolePermBean : rolePermList) {
            menuPermissionIdList.add(eachRolePermBean.getStr("JE_RBAC_PERMGROUPPERM_ID"));
        }
        metaService.delete("JE_RBAC_PERMGROUPPERM", ConditionsWrapper.builder().in("JE_RBAC_PERMGROUPPERM_ID", menuPermissionIdList));
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void savePermGroupMenuPermission(String permGroupId, List<String> menuIdList, GrantTypeEnum grantType) {
        List<DynaBean> allPermmissionBeanList = new ArrayList<>();
        for (String menuId : menuIdList) {
            allPermmissionBeanList.add(pcMenuDataService.writeMenuDataShowTemplate(menuId, true));
        }
        savePermGroupPermAssocaition(permGroupId, allPermmissionBeanList, grantType);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void removePermGroupMenuPermission(String permGroupId, List<String> menuIdList, GrantTypeEnum grantType, boolean update, boolean delete) {
        List<String> menuPermissionList = new ArrayList<>();
        for (String eachMenuId : menuIdList) {
            menuPermissionList.add(pcMenuDataService.formatMenuDataShowTemplate(eachMenuId));
            if (update) {
                menuPermissionList.add(pcMenuDataService.formatMenuDataUpdateTemplate(eachMenuId));
            }
            if (delete) {
                menuPermissionList.add(pcMenuDataService.formatMenuDataDeleteTemplate(eachMenuId));
            }
        }

        List<DynaBean> rolePermList = metaService.select("JE_RBAC_VPERMGROUP", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId)
                .in("PERM_CODE", menuPermissionList));
        List<String> menuPermissionIdList = new ArrayList<>();
        for (DynaBean eachRolePermBean : rolePermList) {
            menuPermissionIdList.add(eachRolePermBean.getStr("JE_RBAC_PERMGROUPPERM_ID"));
        }
        metaService.delete("JE_RBAC_PERMGROUPPERM", ConditionsWrapper.builder().in("JE_RBAC_PERMGROUPPERM_ID", menuPermissionIdList));
    }

    /**
     * 保存关系
     *
     * @param permGroupId
     * @param permissionList
     */
    private void savePermGroupPermAssocaition(String permGroupId, List<DynaBean> permissionList, GrantTypeEnum grantType) {
        //查询已存在的关联关系
        List<String> permissionIdList = new ArrayList<>();
        for (DynaBean eachPermissionBean : permissionList) {
            permissionIdList.add(eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
        }
        List<DynaBean> associationList = metaService.select("JE_RBAC_PERMGROUPPERM", ConditionsWrapper.builder()
                .eq("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name())
                .eq("JE_RBAC_PERMGROUP_ID", permGroupId)
                .in("JE_RBAC_PERM_ID", permissionIdList));
        List<String> havedRolePermissionList = new ArrayList<>();
        for (DynaBean eachAssociationBean : associationList) {
            havedRolePermissionList.add(eachAssociationBean.getStr("JE_RBAC_PERM_ID"));
        }
        //写入关联关系
        DynaBean permGroupPermBean;
        for (DynaBean eachPermissionBean : permissionList) {
            //如果已包含从关联关系，则不做任何操作
            if (havedRolePermissionList.contains(eachPermissionBean.getStr("JE_RBAC_PERM_ID"))) {
                continue;
            }
            permGroupPermBean = new DynaBean("JE_RBAC_PERMGROUPPERM", false);
            permGroupPermBean.set("JE_RBAC_PERMGROUP_ID", permGroupId);
            permGroupPermBean.set("JE_RBAC_PERM_ID", eachPermissionBean.getStr("JE_RBAC_PERM_ID"));
            //授权类型
            permGroupPermBean.set("PERMGROUPPERM_GRANTTYPE_CODE", grantType.name());
            permGroupPermBean.set("PERMGROUPPERM_GRANTTYPE_NAME", grantType.getDesc());
            //todo 设置租户信息
            commonService.buildModelCreateInfo(eachPermissionBean);
            metaService.insert(permGroupPermBean);
        }
    }

}
