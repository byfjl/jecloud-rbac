/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.permgroup.impl;

import com.google.common.base.Splitter;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.permgroup.RbacGrantPermGroupTreeService;
import com.je.rbac.service.permission.GrantTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RbacGrantPermGroupTreeServiceImpl implements RbacGrantPermGroupTreeService {

    @Autowired
    private MetaService metaService;

    @Override
    public void checkWithPermGroup(GrantTypeEnum grantType, String permGroupIds, JSONTreeNode rootNode) {
        List<String> permGroupIdList = Splitter.on(",").splitToList(permGroupIds);
        List<Map<String,Object>> permGroupMapList = metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VPERMGROUP")
                .eq("PERMGROUPPERM_GRANTTYPE_CODE",grantType.name())
                .in("JE_RBAC_PERMGROUP_ID", permGroupIdList));
        recursivePermGroupSetTreeCheck(rootNode, permGroupMapList);
    }

    /**
     * 设置权限组权限
     *
     * @param root
     * @param permGroupList
     */
    private void recursivePermGroupSetTreeCheck(JSONTreeNode root, List<Map<String,Object>> permGroupList) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        //注意：权限组权限不会存在排他权限
        boolean flag = false;
        for (JSONTreeNode eachChildNode : root.getChildren()) {
            for (Map<String,Object> eachPermedBean : permGroupList) {
                if (!eachPermedBean.get("PERM_CODE").equals(eachChildNode.getBean().get("PERM_SHOW"))) {
                    continue;
                }
                //权限组标识，可以是多个
                if (eachChildNode.getBean().containsKey("permGroupIds")) {
                    eachChildNode.getBean().put("permGroupIds", eachChildNode.getBean().get("permGroupIds").toString() + "," + eachPermedBean.get("JE_RBAC_PERMGROUP_ID"));
                    eachChildNode.getBean().put("permGroupNames", eachChildNode.getBean().get("permGroupNames").toString() + "," + eachPermedBean.get("PERMGROUP_NAME"));
                } else {
                    eachChildNode.getBean().put("permGroupIds", eachPermedBean.get("JE_RBAC_PERMGROUP_ID"));
                    eachChildNode.getBean().put("permGroupNames", eachPermedBean.get("PERMGROUP_NAME"));
                }
                //通过权限组设置的权限标识
                eachChildNode.getBean().put("permGroupSet", true);
                eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_PERMGROUPPERM_ID"));
                eachChildNode.setChecked(true);
                flag = true;
            }
            recursivePermGroupSetTreeCheck(eachChildNode,permGroupList);
        }
        //设置是否check
        root.setChecked(flag);
    }

}
