/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.role;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.exception.AccountException;
import com.je.rbac.exception.RoleException;

import java.util.List;
import java.util.Map;

public interface RbacGrantRoleService {

    /**
     * 写入文件夹
     * @param folderName
     */
    DynaBean insertFolder(String folderName,String iconCls, String remark,boolean develop);

    /**
     * 更新文件夹
     * @param folderId
     * @param folderName
     */
    void updateFolder(String folderId,String folderName);

    /**
     * 移除文件夹
     * @param folderId
     */
    void removeFolder(String folderId) throws RoleException;

    /**
     * 增加角色
     * @param parentRoleId 父角色ID
     * @param roleName 角色名称
     * @param permGroupIds 权限组ID
     * @param permGroupNames 权限组Name
     * @param baseRoleId 基础角色ID
     * @param remark 备注信息
     */
    DynaBean insertRole(String parentRoleId,String roleName,String permGroupIds,String permGroupNames,String baseRoleId,String iconCls,String remark,boolean develop) throws RoleException;

    /**
     * 更新角色
     * @param roleId 角色ID
     * @param roleName 角色名称
     * @param permGroupIds 权限组ID
     * @param permGroupNames 权限组Name
     * @param baseRoleId 基础角色ID
     * @param remark 备注信息
     */
    void updateRole(String roleId,String roleName,String permGroupIds,String permGroupNames,String baseRoleId,String remark) throws RoleException;

    /**
     * 移除角色
     * @param roleId
     */
    void removeRole(String roleId) throws RoleException;

    /**
     * 移动角色及其子角色
     * @param roleId
     * @param targetParentId
     * @param develop 是否是开发
     */
    void move(String roleId,String targetParentId,Integer order,boolean develop) throws RoleException;

    /**
     * 查找角色树
     * @param roleOrFolderIds
     * @param develop
     * @return
     */
    JSONTreeNode buildRoleTree(boolean develop,String... roleOrFolderIds);

    /**
     * 根据条件查角色树
     * @param develop
     * @param param
     * @return
     */
    JSONTreeNode buildRoleTreeByQuery(boolean develop, BaseMethodArgument param);

    /**
     * 根据账号导入
     * @param roleId
     * @param strData
     */
    void importAccountByAccounts(String roleId,String strData) throws AccountException;

    /**
     * 根据人员导入
     * @param roleId
     * @param strData
     */
    void importAccountByUsers(String roleId,String strData) throws AccountException;

    /**
     * 账号部门排序
     * @param roleId
     * @param strData
     */
    void importAccountUserSysOrderIdex(String roleId,String strData) throws AccountException;

    /**
     * 根据人员移除
     * @param roleId
     * @param strData
     */
    void removeAccountByUsers(String roleId,String strData) throws AccountException;

    /**
     * 按照部门人员导入账号
     * @param roleId
     * @param departmentId
     * @param userIds
     */
    void importAccountByUsers(String roleId,String departmentId,String userIds,String sysOrderIndex) throws AccountException;

    /**
     * 按照部门人员移除账号
     * @param roleId
     * @param departmentId
     * @param userIds
     */
    void removeAccountByUsers(String roleId,String departmentId,String userIds) throws AccountException;

    /**
     * 按部门导入账号
     * @param roleId 角色ID
     * @param departments 部门集合
     */
    void importAccountByDepartments(String roleId,String departments) throws AccountException;

    /**
     * 按部门移除账号
     * @param roleId 角色ID
     * @param departments 部门集合
     */
    void removeAccountByDepartments(String roleId,String departments) throws AccountException;

    /**
     * 按照机构导入账号
     * @param roleId 角色ID
     * @param orgId 机构ID
     * @param ids 机构数据ID集合
     */
    void importAccountByOrgs(String roleId,String orgId,String ids) throws AccountException;

    /**
     * 按照机构移除账号
     * @param roleId 角色ID
     * @param orgId 机构ID
     * @param ids 机构数据ID集合
     */
    void removeAccountByOrgs(String roleId,String orgId,String ids) throws AccountException;

    /**
     * 移除账户角色
     * @param roleId
     * @param accountMap
     */
    void removeAccountRole(String roleId, Map<String, List<String>> accountMap) throws AccountException;

    /**
     * 移除账号角色
     * @param roleId 角色ID
     * @param departmentId 部门ID
     * @param accountIdList 账号ID
     */
    void removeAccountRole(String roleId,String departmentId,List<String> accountIdList) throws AccountException;

    /**
     * 当角色名称修改时，同步更新用户、外部机构引用角色名称
     */
    void syncUpdateReferencesRoleName(String roleId, String roleName);

}
