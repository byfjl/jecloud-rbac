/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.grant.role.impl;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.grant.role.RbacGrantRoleTreeService;
import com.je.rbac.service.permission.GrantTypeEnum;
import com.je.rbac.service.role.RbacRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RbacGrantRoleTreeServiceImpl implements RbacGrantRoleTreeService {

    private static final Logger logger = LoggerFactory.getLogger(RbacGrantRoleTreeServiceImpl.class);

    @Autowired
    private MetaService metaService;
    @Autowired
    private RbacRoleService rbacRoleService;

    /**
     * 角色类型，设置check
     *
     * @param grantType
     * @param roleIds
     * @param rootNode
     */
    @Override
    public void checkWithRole(GrantTypeEnum grantType, String roleIds, JSONTreeNode rootNode) {
        List<String> roleIdList = Splitter.on(",").splitToList(roleIds);
        Set<String> extendRoleIdSet = rbacRoleService.findExtendRoleIds(roleIdList);
        List<String> allRoleIdList = new ArrayList<>();
        allRoleIdList.addAll(roleIdList);
        allRoleIdList.addAll(extendRoleIdSet);

        List<DynaBean> roleBeanList = metaService.select("JE_RBAC_ROLE",ConditionsWrapper.builder().in("JE_RBAC_ROLE_ID",allRoleIdList));
        //查询权限组
        List<String> permGroupIdList = new ArrayList<>();
        for (DynaBean eachBean : roleBeanList) {
            if(Strings.isNullOrEmpty(eachBean.getStr("ROLE_PERMGROUP_ID"))){
                continue;
            }
            permGroupIdList.addAll(Splitter.on(",").splitToList(eachBean.getStr("ROLE_PERMGROUP_ID")));
        }

        if (permGroupIdList != null && permGroupIdList.size()>0) {
            List<Map<String,Object>> permGroupMapList = metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VPERMGROUP").in("JE_RBAC_PERMGROUP_ID", permGroupIdList));
            recursivePermGroupSetTreeCheck(rootNode, permGroupMapList);
        }

        List<Map<String,Object>> rolePermMapList = metaService.selectSql(ConditionsWrapper.builder().table("JE_RBAC_VROLEPERM").eq("ROLEPERM_GRANTTYPE_CODE", grantType.name())
                .eq("PERM_OPERATE_CODE", "show")
                .in("JE_RBAC_ROLE_ID", allRoleIdList)
                .in("PERM_TYPE_CODE", Lists.newArrayList("MENU", "FUNC_PC_BASE", "BUTTON_PC","SUBFUNC_PC")));

        recursiveRoleSetTreeCheck(rootNode, rolePermMapList, roleIdList, extendRoleIdSet);

    }

    /**
     * 设置权限组权限
     *
     * @param root
     * @param permGroupList
     */
    private void recursivePermGroupSetTreeCheck(JSONTreeNode root, List<Map<String,Object>> permGroupList) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        //注意：权限组权限不会存在排他权限
        boolean flag = false;
        for (JSONTreeNode eachChildNode : root.getChildren()) {
            for (Map<String,Object> eachPermedBean : permGroupList) {
                if (!eachPermedBean.get("PERM_CODE").equals(eachChildNode.getBean().get("PERM_SHOW"))) {
                    continue;
                }
                //权限组标识，可以是多个
                if (eachChildNode.getBean().containsKey("permGroupIds")) {
                    eachChildNode.getBean().put("permGroupIds", eachChildNode.getBean().get("permGroupIds").toString() + "," + eachPermedBean.get("JE_RBAC_PERMGROUP_ID"));
                    eachChildNode.getBean().put("permGroupNames", eachChildNode.getBean().get("permGroupNames").toString() + "," + eachPermedBean.get("PERMGROUP_NAME"));
                } else {
                    eachChildNode.getBean().put("permGroupIds", eachPermedBean.get("JE_RBAC_PERMGROUP_ID"));
                    eachChildNode.getBean().put("permGroupNames", eachPermedBean.get("PERMGROUP_NAME"));
                }
                //通过权限组设置的权限标识
                flag = true;
                eachChildNode.getBean().put("permGroupSet", true);
                eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_PERMGROUPPERM_ID"));
                eachChildNode.getBean().put("extend", true);
                eachChildNode.getBean().put("extendByPermGroup", true);
                eachChildNode.setChecked(true);
            }
            recursivePermGroupSetTreeCheck(eachChildNode, permGroupList);
        }
        //设置是否check
        root.setChecked(flag);
    }

    private void recursiveRoleSetTreeCheck(JSONTreeNode root, List<Map<String,Object>> beanList, List<String> currentRoleIdList, Set<String> extendRoleIdList) {
        if (root.getChildren() == null || root.getChildren().isEmpty()) {
            return;
        }
        boolean flag = false;
        for (JSONTreeNode eachChildNode : root.getChildren()) {
            for (Map<String,Object> eachPermedBean : beanList) {
                if (!eachPermedBean.get("PERM_CODE").equals(eachChildNode.getBean().get("PERM_SHOW"))) {
                    continue;
                }
                //-----------------此处的逻辑在于有可能出现非怕他权限在排他权限后，造成PermId出错--------------------
                //设置是否继承,请注意逻辑：如果是继承，并且排他是true，则不要设置PERM_ID，因为排他权限优先级最高
                if (!currentRoleIdList.contains(eachPermedBean.get("JE_RBAC_ROLE_ID"))
                        && extendRoleIdList.contains(eachPermedBean.get("JE_RBAC_ROLE_ID"))) {
                    eachChildNode.getBean().put("extend", true);
                    eachChildNode.getBean().put("extendByRole", true);
                }

                //设置是否排他权限,请注意逻辑：如果是排他，则设置权限为排他权限ID
                if ("1".equals(eachPermedBean.get("ROLEPERM_EXCLUDE_CODE"))) {
                    eachChildNode.getBean().put("exclude", true);
                    eachChildNode.getBean().put("extend", false);
                    eachChildNode.getBean().put("ROLE_ID", eachPermedBean.get("JE_RBAC_ROLE_ID"));
                    eachChildNode.getBean().put("ROLE_NAME", eachPermedBean.get("ROLE_NAME"));
                    eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_ROLEPERM_ID"));
                    if ("1".equals(eachPermedBean.get("ROLEPERM_NOT_CHECKED"))) {
                        eachChildNode.setChecked(false);
                    } else {
                        eachChildNode.setChecked(true);
                    }
                }

                //如果不是排他和继承的，则设置权限ID
                if (!eachChildNode.getBean().containsKey("exclude") || !Boolean.valueOf(eachChildNode.getBean().get("exclude").toString())) {
                    eachChildNode.getBean().put("exclude", false);
                    eachChildNode.getBean().put("ROLE_ID", eachPermedBean.get("JE_RBAC_ROLE_ID"));
                    eachChildNode.getBean().put("ROLE_NAME", eachPermedBean.get("ROLE_NAME"));
                    eachChildNode.getBean().put("PERM_ID", eachPermedBean.get("JE_RBAC_ROLEPERM_ID"));
                    eachChildNode.setChecked(true);
                }

                //如果非继承，则设置继承属性
                if (!eachChildNode.getBean().containsKey("extend")) {
                    eachChildNode.getBean().put("extend", false);
                }

                //如果flag不是true，并且checked，则flag应该是ture
                if (!flag && eachChildNode.getChecked()) {
                    flag = true;
                }

            }
            recursiveRoleSetTreeCheck(eachChildNode, beanList, currentRoleIdList, extendRoleIdList);
        }

        //如果此节点包含权限，则判断是否是排他权限，如果是排他权限，则根据排他权限设置，如果不是，肯定为true
        if(root.getBean().containsKey("PERM_ID")){
            if(!root.getBean().containsKey("exclude")){
                root.setChecked(true);
            }
        }else{
            root.setChecked(flag);
        }
    }

}
