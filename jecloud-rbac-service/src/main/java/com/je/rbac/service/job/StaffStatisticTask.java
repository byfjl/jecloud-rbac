/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.job;
import cn.hutool.core.date.StopWatch;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.DateUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.*;

/***
 * 统计每月在职人员
 */
@Component
public class StaffStatisticTask {
    private static Logger logger = LoggerFactory.getLogger(StaffStatisticTask.class);

    @Autowired
    protected MetaService metaService;

    @XxlJob("staffStatisticHandler")
    public void executeJob(){
        logger.info("staffStatisticHandler.executeJob task start: "+DateUtils.formatDateTime(new Date()));
        StopWatch watch = new StopWatch();
        watch.start();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat monthSdf = new SimpleDateFormat("yyyy-MM");
        String dateStr = sdf.format(new Date());
        String dateDbStr = monthSdf.format(DateUtils.getDate(dateStr,"yyyy-MM"));

        List<Map<String, Object>> entryDataMaps= metaService.selectSql("SELECT * FROM JE_RBAC_USER WHERE USER_EMPLOYEE_STATUS = '1'");

        Map<String, Object> onJobMap = new HashMap<>();
        onJobMap.put("ACCOUNTONJOB_TJRQ", dateDbStr);
        onJobMap.put("ACCOUNTONJOB_TJSL",(entryDataMaps.size()));

        DynaBean accountObJobBean = metaService.selectOne("JE_RBAC_ACCOUNTONJOB", ConditionsWrapper.builder().eq("ACCOUNTONJOB_TJRQ",dateDbStr));
        if(accountObJobBean == null){
            DynaBean logBean = new DynaBean("JE_RBAC_ACCOUNTONJOB", false);
            logBean.set("ACCOUNTONJOB_TJRQ", dateDbStr);
            logBean.set("ACCOUNTONJOB_TJSL", (entryDataMaps.size()));
            logBean.set("SY_CREATETIME", DateUtils.formatDateTime(new Date()));
            metaService.insert(logBean);
        }else{
            metaService.executeSql("UPDATE JE_RBAC_ACCOUNTONJOB SET ACCOUNTONJOB_TJSL={0} WHERE ACCOUNTONJOB_TJRQ={1}",
                    (entryDataMaps.size()), dateDbStr);
        }
        watch.stop();
        logger.info("staffStatisticHandler.executeJob task end: "+DateUtils.formatDateTime(new Date()));
        logger.info("staffStatisticHandler.executeJob task 耗时: "+watch.getTotalTimeSeconds()+" 秒");

    }

}
