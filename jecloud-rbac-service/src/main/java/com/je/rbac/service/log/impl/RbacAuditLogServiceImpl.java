/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.log.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.document.InternalFileBO;
import com.je.common.base.document.InternalFileUpload;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.DocumentInternalRpcService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.log.RbacAuditLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.File;
import java.util.List;

@Service
public class RbacAuditLogServiceImpl implements RbacAuditLogService {

    @Autowired
    private Environment environment;
    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private DocumentInternalRpcService documentInternalRpcService;

    @Override
    @Transactional
    public void doDocRecords(List<String> recordTypeList, String typeCode, String typeName) {
        File file = FileUtil.file(environment.getProperty("servicecomb.downloads.directory") + File.separator + "audit_" + System.currentTimeMillis() + ".log");
        List<DynaBean> logList = metaService.select("JE_RBAC_AUDITLOG", ConditionsWrapper.builder().in("AUDITLOG_LOGTYPE_CODE", recordTypeList));
        for (int i = 0; logList != null && i < logList.size(); i++) {
            FileUtil.appendString(JSON.toJSONString(logList.get(i)), file, "UTF-8");
        }
        File zipFile = ZipUtil.zip(file);
        InternalFileUpload fileUpload = new InternalFileUpload();
        fileUpload.setFileName(zipFile.getName());
        fileUpload.setContentType("application/octet-stream");
        fileUpload.setSize(zipFile.length());
        InternalFileBO fileBO = documentInternalRpcService.saveSingleFile(fileUpload, zipFile, SecurityUserHolder.getCurrentAccountRealUserId());
        DynaBean docBean = new DynaBean("JE_RBAC_AUDITDOC", false);
        JSONObject fileContent = new JSONObject();
        fileContent.put("fileKey", fileBO.getFileKey());
        fileContent.put("relName", fileBO.getRelName());
        fileContent.put("suffix", "zip");
        fileContent.put("size", zipFile.length());
        JSONArray array = new JSONArray();
        array.add(fileContent);
        docBean.set("AUDITDOC_FILE_NAME", array.toString());
        docBean.set("AUDITDOC_FILE_KEY", fileBO.getFileKey());
        docBean.set("AUDITDOC_SIZE", zipFile.length());
        docBean.set("AUDITDOC_TYPE_CODE", typeCode);
        docBean.set("AUDITDOC_TYPE_NAME", typeName);
        commonService.buildModelCreateInfo(docBean);
        metaService.insert(docBean);
        metaService.delete("JE_RBAC_AUDITLOG",ConditionsWrapper.builder().in("AUDITLOG_LOGTYPE_CODE", recordTypeList));
    }

}
