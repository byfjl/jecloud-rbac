/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.menu;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.result.BaseRespResult;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.rbac.exception.MenuException;
import java.util.List;
import java.util.Map;

public interface MenuService {

    /**
     * 加载首页顶部菜单
     * @param accountId
     * @param plan
     * @return
     */
    List<Map<String,Object>> loadHomeTopMenus(String departmentId,String accountId,String tenantId,String plan);

    /**
     * 加载首页菜单
     * @param accountId
     * @param plan
     * @return
     */
    JSONTreeNode loadHomeMenus(String departmentId,String accountId,String tenantId,String plan);

    /**
     * 根据用户id获取快捷菜单功能
     *
     * @param userId 用户id
     * @return
     */
    List<Map<String, Object>> getQuickMenu(String userId);

    /**
     * 保存菜单信息
     *
     * @param dynaBean 菜单DynaBean
     * @return
     */
    DynaBean doSave(DynaBean dynaBean);

    /**
     * 获取树
     *
     * @param query 查询条件
     * @param node  node节点
     * @return
     */
    JSONTreeNode getTree(Query query, String node);


    /**
     * 移动菜单
     *
     * @param menuId       主键
     * @param toModuleId   新模块信息
     * @return
     */
    BaseRespResult menuMove(String menuId, String toModuleId);

    /**
     * 删除
     *
     * @return
     */
    int doRemove(String menuIds);

    /**
     * 加载菜单提醒
     *
     * @param funcCode
     */
    JSONObject loadBadge(String funcCode);

    /**
     * 对指定的菜单进行授权给当前登录人的开发角色
     * @param pkValue
     * @return
     */
    BaseRespResult doDevelopPerm(String pkValue);


    /***
     * 更新关联顶部菜单信息
     * @param menuId
     * @param topMenuIds    关联顶部菜单ID
     * @param topMenuNames   关联顶部菜单NAME
     * @return
     */
    int updateTopMenuRelation(String menuId,String topMenuIds,String topMenuNames);


    /***
     * 若该菜单所属产品 由方案类改为平台类，则之前该菜单所分配的人员同步清除该菜单权限
     */
    void syncUpdateUserRolePermByProductId(DynaBean dynaBean);


    /***
     *校验功能编码
     */
    void checkFunCodeData(DynaBean dynaBean) throws MenuException;

    /***
     * 删除顶部菜单关联表中菜单数据
     * @param menuId
     * @param topMenuId
     * @return
     */
    int doRemoveHeadMenuRelation(String menuId, String topMenuId);

    int doRemoveAllHeadMenuRelation(String menuId);

    /***
     * 删除顶部菜单关联表中菜单数据
     * @param topMenuId
     * @param topMenuId
     * @return
     */
    int doRemoveHeadMenuRelationByTopMenuId(String topMenuId);

    /***
     * 修改菜单表 顶部菜单关联字段
     * @param menuId
     * @param topMenuId
     * @param topMenuName
     * @return
     */
    int updateTopMenuRelationBean(String menuId,String topMenuId,String topMenuName);


    /***
     * 根据menuId 查询 指定菜单数据
     * @param menuId
     * @return
     */
    DynaBean findById(String menuId);


    /***
     * 检查原菜单是否已不是一级菜单，若不是，则检查是否关联顶部菜单，若关联，则清除关联关系
     * @param sourceBean
     * @param moveAfterBean
     */
    void checkAndUpdate(DynaBean sourceBean,DynaBean moveAfterBean);

    /**
     * 加载快捷菜单
     * @param plan
     * @return
     */
    List<Map<String, Object>> getCommonMenuByPlan(String plan);

    /**
     * 根据菜单id获取菜单所属方案
     * @param id
     * @return
     */
    JSONObject getMenuScheme(String id);

}
