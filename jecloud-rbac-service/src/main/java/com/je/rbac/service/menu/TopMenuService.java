/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.menu;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.rbac.exception.MenuException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TopMenuService {

    /***
     * 通过顶部菜单id查询
     * @param headMenuId
     * @return
     */
    DynaBean selectTopMenuById(String headMenuId);

    /***
     * 通过顶部菜单id查询关联表
     * @param headMenuId
     * @return
     */
    List<DynaBean> selectTopMenuRelationByTopMenuId(String headMenuId);


    /***
     * 判断菜单在关联表中是否已存在关联
     * @param menuIds
     * @return
     */
    List<DynaBean> selectTopMenuRelationByMenuId(List<String> menuIds);

    /***
     * 删除顶部菜单关联菜单 关联表数据；更新菜单表
     * @param topMenuIds
     * @return
     */
    void updateMenuRelation(List<String> topMenuIds);


    /***
     * 根据主键查询顶部菜单关联表
     * @param headMenuRelationId
     * @return
     */
    DynaBean selectTopMenuRelationById(String headMenuRelationId);

    DynaBean doSave(BaseMethodArgument param, HttpServletRequest request);

    DynaBean doUpdate(BaseMethodArgument param, HttpServletRequest request);

    /***
     * 校验
     * @param topMenuIds
     * @return
     */
    void checkChildMenu(List<String> topMenuIds) throws MenuException;

    /**
     * 更新可见角色权限
     * @param menuId
     * @param sourceSeeRoleIds
     * @param targetSeeRoleIds
     */
    void updateSeeRolePermissions(String menuId,String sourceSeeRoleIds,String targetSeeRoleIds);

    /**
     * 更新可见部门权限
     * @param menuId
     * @param sourceSeeDeptIds
     * @param targetSeeDeptIds
     */
    void updateSeeDeptPermissions(String menuId,String sourceSeeDeptIds,String targetSeeDeptIds);

    /**
     * 更新可见机构权限
     * @param menuId
     * @param sourceSeeOrgIds
     * @param targetSeeOrgIds
     */
    void updateSeeOrgPermissions(String menuId,String sourceSeeOrgIds,String targetSeeOrgIds);

    /**
     * 更新可见人员权限
     * @param menuId
     * @param sourceSeeUserIds
     * @param targetSeeUserIds
     */
    void updateSeeUserPermissions(String menuId,String sourceSeeUserIds,String targetSeeUserIds);

    void saveDeleteLog(List<DynaBean> list);
}
