/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.organization;

import com.je.common.auth.impl.PlatformOrganization;
import com.je.common.base.DynaBean;
import com.je.rbac.exception.AccountException;
import com.je.rbac.exception.DepartmentUserException;

import java.util.List;
import java.util.Map;

/**
 * 组织机构映射基础服务
 */
public interface RbacOrganizationService {

    /***
     * 校验机构是否被禁用
     * @param tableCode
     * @return
     */
    boolean checkOrgStatus(String tableCode);

    /***
     * 通过tableCode获取关联机构
     * @param tableCode
     * @return
     */
    DynaBean getOrgByTableCode(String tableCode);

    /**
     * 检查是否是系统级数据
     * @param orgId
     * @return
     */
    boolean checkSystemUnique(String orgId);

    /**
     * 检查机构名称是否唯一
     * @param dynaBean
     * @return
     */
    boolean checkOrgNameUnique(DynaBean dynaBean);

    /**
     * 检查机构编码是否唯一
     * @param dynaBean
     * @return
     */
    boolean checkOrgCodeUnique(DynaBean dynaBean);

    /**
     * 检查机构资源表编码是否唯一
     * @param dynaBean
     * @return
     */
    boolean checkResourceTableCodeUnique(DynaBean dynaBean);

    /**
     * 通过OrgId修改角色部门名称，账号部门名称
     * @param dynaBean
     * @return
     */
    void updateDeptNameByOrgId(DynaBean dynaBean);

    /**
     * 保存
     * @param OrgBean
     * @return
     */
    DynaBean doSave(DynaBean OrgBean) throws DepartmentUserException;

    /**
     * 根据ID查找
     * @param id
     * @return
     */
    DynaBean findById(String id);

    /**
     * 根据ID构建
     * @param id
     * @return
     */
    default PlatformOrganization buildById(String id){
        return buildOrganization(findById(id));
    }

    /**
     * 机构bean
     * @param orgBean
     * @return
     */
    default PlatformOrganization buildOrganization(DynaBean orgBean){
        PlatformOrganization platformOrganization = new PlatformOrganization();
        platformOrganization.parse(orgBean.getValues());
        return platformOrganization;
    }

    /**
     * 根据code查找
     * @param code
     * @return
     */
    DynaBean findByCode(String code);

    /**
     * 根据编码构建
     * @param code 机构编码
     * @return
     */
    default PlatformOrganization buildByCode(String code){
        return buildOrganization(findByCode(code));
    }

    /**
     * 根据name查找
     * @param name
     * @return
     */
    DynaBean findByName(String name);

    /**
     * 根据名称构建
     * @param name 机构名称
     * @return
     */
    default PlatformOrganization buildByName(String name){
        return buildOrganization(findByName(name));
    }

    /**
     * 根据tableCode查找
     * @param tableCode
     * @return
     */
    DynaBean findByTableCode(String tableCode);

    /**
     * 根据租户ID查找
     * @param tenantId 租户ID
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户Name查找
     * @param tenantName 租户Name
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

    /**
     * 禁用机构
     * @param orgIds
     */
    void disableOrgs(String orgIds);

    /**
     * 启用机构
     * @param orgIds
     */
    void enableOrgs(String orgIds);

    /**
     * 开通账号
     * @param orgId
     * @param userIds
     * @param syStatus 默认状态为 正常；若新增用户为 禁用
     */
    void openAccount(String orgId, String userIds,String syStatus) throws AccountException;

    /**
     * 操作外部用户和账号
     * @param orgId
     * @param userIds
     */
    void operationUserWithAccount(String opType,String orgId,String userIds) throws AccountException;


    /**
     * 移除机构
     * @param orgIds 机构ID列表
     */
    void removeOrg(String orgIds);

    /***
     * 更新用户名称
     * @param userBean
     */
    void updateUserName(DynaBean userBean);

    /***
     * 同步账号信息
     * @param userBean
     */
    void syncAccountBy(DynaBean userBean);

}
