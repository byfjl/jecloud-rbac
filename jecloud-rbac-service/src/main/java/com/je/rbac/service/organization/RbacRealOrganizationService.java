/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.organization;

import com.je.common.auth.impl.RealOrganization;
import com.je.common.auth.impl.RealOrganizationUser;
import com.je.rbac.exception.AccountException;

/**
 * 真实机构映射基础服务
 */
public interface RbacRealOrganizationService {

    /**
     * 查找真实的用户，设置对象属性
     *
     * @param orgId    机构ID
     * @param realId   真实用户ID
     * @param realUser 真实的用户对象
     * @return
     */
    void fillRealUser(String orgId, String realId, RealOrganizationUser realUser) throws AccountException;

    /**
     * 查找所属机构，设置对象属性
     *
     * @param realOrgId        真实机构ID（部门ID或供应商）
     * @param realOrganization 真实机构对象
     * @return
     */
    void fillOrganization(String realOrgId, RealOrganization realOrganization) throws AccountException;
}
