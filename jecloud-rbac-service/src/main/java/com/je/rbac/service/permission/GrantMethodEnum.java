/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission;

/**
 * 授权方式
 */
public enum GrantMethodEnum {

    ROLE("角色授权"),
    DEPARTMENT("部门授权"),
    ORG("机构授权"),
    PERM_GROUP("权限组授权"),
    DEVELOP("开发组授权"),
    USER("用户授权");

    String desc;

    GrantMethodEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static GrantMethodEnum getGrantType(String type){
        if(ROLE.name().equalsIgnoreCase(type)){
            return ROLE;
        }else if(DEPARTMENT.name().equalsIgnoreCase(type)){
            return DEPARTMENT;
        }else if(ORG.name().equalsIgnoreCase(type)){
            return ORG;
        }else if(PERM_GROUP.name().equalsIgnoreCase(type)){
            return PERM_GROUP;
        }else if(DEVELOP.name().equalsIgnoreCase(type)){
            return DEVELOP;
        }else {
            return null;
        }
    }

}
