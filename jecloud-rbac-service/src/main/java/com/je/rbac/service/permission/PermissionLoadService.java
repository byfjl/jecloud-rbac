/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission;

import java.util.List;
import java.util.concurrent.Future;

/**
 * 权限缓存服务
 */
public interface PermissionLoadService {

    /**
     * 重新加载角色权限缓存
     * <p>注意：刷新角色权限要同时刷新权限组缓存</p>
     * @param roleIdList
     */
    Future<Boolean> reloadRolePermCache(List<String> roleIdList, boolean withPermGroup);

    /**
     * 清空角色缓存
     * @param roleIdList
     */
    Future<Boolean> removeRolePermCache(List<String> roleIdList);

    /**
     * 重新加载所有角色权限缓存
     */
    Future<Boolean> reloadAllRolePermCache(boolean withPermGroup);

    /**
     * 清空所有角色权限缓存
     */
    Future<Boolean> removeAllRolePermCache();

    /**
     * 重新加载所有开发角色权限缓存
     */
    Future<Boolean> reloadDevelopRolePermCache(List<String> roleIdList);

    /**
     * 清空所有开发角色权限缓存
     */
    Future<Boolean> removeDevelopRolePermCache(List<String> roleIdList);

    /**
     * 重新加载所有开发角色权限缓存
     */
    Future<Boolean> reloadAllDevelopRolePermCache();

    /**
     * 清空所有开发角色权限缓存
     */
    Future<Boolean> removeAllDevelopRolePermCache();

    /**
     * 重新加载所有部门权限缓存
     * @param departmentIdList
     */
    Future<Boolean> reloadDepartmentPermCache(List<String> departmentIdList);

    /**
     * 清空所有部门权限缓存
     * @param departmentIdList
     */
    Future<Boolean> removeDepartmentPermCache(List<String> departmentIdList);

    /**
     * 重新加载所有部门权限缓存
     */
    Future<Boolean> reloadAllDepartmentPermCache();

    /**
     * 清空所有部门权限缓存
     */
    Future<Boolean> removeAllDepartmentPermCache();

    /**
     * 重新加载权限组权限缓存
     * @param permGroupIdList
     */
    Future<Boolean> reloadPermGroupPermCache(List<String> permGroupIdList,boolean withRole);

    /**
     * 清空权限组权限缓存
     * @param permGroupIdList
     */
    void removePermGroupPermCache(List<String> permGroupIdList);

    /**
     * 重新加载所有权限组权限缓存
     */
    Future<Boolean> reloadAllPermGroupPermCache(boolean withRole);

    /**
     * 清空所有权限组权限缓存
     */
    void removeAllPermGroupPermCache();

    /**
     * 重新加载机构权限缓存
     * @param orgIds
     */
    Future<Boolean> reloadOrgPermCache(List<String> orgIds);

    /**
     * 清空机构权限缓存
     * @param orgIds
     */
    Future<Boolean> removeOrgPermCache(List<String> orgIds);

    /**
     * 重新加载所有机构权限缓存
     */
    Future<Boolean> reloadAllOrgPermCache();

    /**
     * 清空所有机构权限缓存
     */
    void removeAllOrgPermCache();

    /**
     * 重新加载用户权限缓存
     * @param userIds
     */
    Future<Boolean> reloadUserPermCache(List<String> userIds);

    /**
     * 清空用户权限缓存
     * @param userIds
     */
    void removeUserPermCache(List<String> userIds);

    /**
     * 重新加载所有用户权限缓存
     */
    void reloadAllUserPermCache();

    /**
     * 清空所有用户权限缓存
     */
    Future<Boolean> removeAllUserPermCache();

    /**
     * 重新加载所有类型的缓存
     */
    void reloadAllTypedPermsCache();

    /**
     * 重新加载所有类型的缓存
     */
    void removeAllTypedPermsCache();

    /**
     * 移除账号权限缓存
     * @param accountIdList
     */
    void removeAccountPermsCache(List<String> accountIdList);

}
