/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface RbacPermmisionBaseService {

    /**
     * 根据ID查找
     *
     * @param id
     * @return
     */
    DynaBean findById(String id);

    default String buildPermissionById(String id) {
        if (Strings.isNullOrEmpty(id)) {
            return null;
        }
        return buildPermission(findById(id));
    }

    /**
     * 根据Code查找
     *
     * @param code
     * @return
     */
    DynaBean findByCode(String code);

    default String buildPermissionByCode(String code) {
        if (Strings.isNullOrEmpty(code)) {
            return null;
        }
        return buildPermission(findByCode(code));
    }

    /**
     * 根据名称查找
     *
     * @param name
     * @return
     */
    DynaBean findName(String name);

    default String buildPermissionByName(String name) {
        if (Strings.isNullOrEmpty(name)) {
            return null;
        }
        return buildPermission(findName(name));
    }

    /**
     * 根据主键集合查找
     *
     * @param idList
     * @return
     */
    List<Map<String,Object>> findByIds(List<String> idList);

    default List<String> buildPermissionByIds(List<String> idList) {
        if (idList == null || idList.isEmpty()) {
            return null;
        }
        return buildPermissions(findByIds(idList));
    }

    /**
     * 根据编码集合查找
     *
     * @param codeList
     * @return
     */
    List<Map<String,Object>> findByCodes(List<String> codeList);

    default List<String> buildPermissionByCodes(List<String> codeList) {
        if (codeList == null || codeList.isEmpty()) {
            return null;
        }
        return buildPermissions(findByCodes(codeList));
    }

    /**
     * 根据权限类型查找
     *
     * @param type
     * @return
     */
    List<DynaBean> findByType(PermissionTypeEnum type);

    /**
     * 根据类型、是否输出查找
     *
     * @param type
     * @param output
     * @return
     */
    List<DynaBean> findByTypeAndOutput(PermissionTypeEnum type, boolean output);

    /**
     * 根据租户ID查找
     *
     * @param tenantId
     * @return
     */
    List<DynaBean> findByTenantId(String tenantId);

    /**
     * 根据租户Name查找
     *
     * @param tenantName
     * @return
     */
    List<DynaBean> findByTenantName(String tenantName);

    /**
     * 批量构建权限实体
     *
     * @param permissionBeans
     * @return
     */
    default List<String> buildPermissions(List<Map<String, Object>> permissionBeans) {
        if (permissionBeans == null || permissionBeans.isEmpty()) {
            return null;
        }
        List<String> permissions = new ArrayList<>();
        for (Map<String, Object> eachBean : permissionBeans) {
            permissions.add(buildPermission(eachBean));
        }
        return permissions;
    }

    /**
     * 根据部门查找权限
     *
     * @param departmentIdList
     * @return
     */
    List<Map<String, Object>> findDepartmentPermissions(List<String> departmentIdList);

    /**
     * 根据部门查找权限
     *
     * @param departmentIdList
     * @return
     */
    List<Map<String, Object>> findDepartmentFuncPermissions(List<String> departmentIdList,String funcCode);

    /**
     * 根据部门构建
     *
     * @param departmentIdList
     * @return
     */
    default List<String> buildDepartmentPermissions(List<String> departmentIdList) {
        if (departmentIdList == null || departmentIdList.isEmpty()) {
            return null;
        }
        return buildPermissions(findDepartmentPermissions(departmentIdList));
    }

    /**
     * 根据人员查找权限
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> findUserPermissions(String userId);

    /**
     * 根据人员查找权限
     *
     * @param userId
     * @return
     */
    List<Map<String, Object>> findUserFuncPermissions(String userId,String funcCode);

    /**
     * 根据用户构建
     *
     * @param userId
     * @return
     */
    default List<String> buildUserPermissions(String userId) {
        if (Strings.isNullOrEmpty(userId)) {
            return null;
        }
        return buildPermissions(findUserPermissions(userId));
    }

    /**
     * @param roleIds 角色列表
     * @return
     */
    List<Map<String, Object>> findRolePermissions(List<String> roleIds);

    /**
     * @param roleIds 角色列表
     * @return
     */
    List<Map<String, Object>> findRoleFuncPermissions(List<String> roleIds,String funcCode);

    /**
     * 根据角色构建
     *
     * @param roleIds
     * @return
     */
    default List<String> buildRolesPermissions(List<String> roleIds) {
        if (roleIds == null || roleIds.isEmpty()) {
            return null;
        }
        return buildPermissions(findRolePermissions(roleIds));
    }

    /**
     * 查找机构权限
     *
     * @param orgId 机构ID
     * @return
     */
    List<Map<String,Object>> findOrgPermissions(String orgId);

    /**
     * 查找机构权限
     *
     * @param orgId 机构ID
     * @return
     */
    List<Map<String,Object>> findOrgFuncPermissions(String orgId,String funcCode);

    /**
     * 根据机构构建
     *
     * @param orgId
     * @return
     */
    default List<String> buildOrgPermissions(String orgId) {
        if (Strings.isNullOrEmpty(orgId)) {
            return null;
        }
        return buildPermissions(findOrgPermissions(orgId));
    }

    /**
     * 构建权限实体
     *
     * @param permissionBean
     * @return
     */
    default String buildPermission(Map<String, Object> permissionBean) {
        if (permissionBean == null) {
            return null;
        }
        return permissionBean.get("PERM_CODE") == null ? null : permissionBean.get("PERM_CODE").toString();
    }

    /**
     * 构建权限实体
     *
     * @param permissionBean
     * @return
     */
    default String buildPermission(DynaBean permissionBean) {
        if (permissionBean == null) {
            return null;
        }
        return permissionBean.getStr("PERM_CODE");
    }

    /**
     * 查找机构权限
     *
     * @param permGroupIdList 权限组权限
     * @return
     */
    List<Map<String, Object>> findPermGroupPermissions(List<String> permGroupIdList);

    /**
     * 查找机构权限
     *
     * @param permGroupIdList 权限组权限
     * @return
     */
    List<Map<String, Object>> findPermGroupFuncPermissions(List<String> permGroupIdList,String funcCode);

    /**
     * 根据机构构建
     *
     * @param permGroupIdList 权限组
     * @return
     */
    default List<String> buildPermGroupPermissions(List<String> permGroupIdList) {
        if (permGroupIdList == null || permGroupIdList.isEmpty()) {
            return null;
        }
        return buildPermissions(findPermGroupPermissions(permGroupIdList));
    }

}
