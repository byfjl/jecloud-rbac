/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import com.je.common.base.DynaBean;
import java.util.List;
import java.util.Map;

/**
 * PC功能按钮权限服务
 */
public interface PcFuncButtonService {

    /**
     * 格式化PC功能按钮权限模板
     * @param funcCode
     * @param buttonCode
     * @return
     */
    String formatPcFuncButtonShowTemplate(String funcCode,String buttonCode);

    /**
     * 格式化PC功能按钮权限模板
     * @param funcCode
     * @param buttonCode
     * @return
     */
    String formatPcFuncButtonUpdateTemplate(String funcCode,String buttonCode);

    /**
     * 格式化PC功能按钮权限模板
     * @param funcCode
     * @param buttonCode
     * @return
     */
    String formatPcFuncButtonDeleteTemplate(String funcCode,String buttonCode);

    /**
     * 写入功能加载权限
     * @param funcCode
     * @param checkDb
     * @return
     */
    DynaBean writePcFuncButtonShowPermission(String funcCode,String buttonCode,boolean checkDb);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCode
     * @return
     */
    DynaBean findPcFuncButtonShowPermission(String funcCode,String buttonCode);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCodeList
     * @return
     */
    List<DynaBean> findPcFuncButtonShowPermission(String funcCode,List<String> buttonCodeList);

    /**
     * 写入功能更新权限
     * @param funcCode
     * @param checkDb
     * @return
     */
    DynaBean writePcFuncButtonUpdatePermission(String funcCode,String buttonCode,boolean checkDb);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCode
     * @return
     */
    DynaBean findPcFuncButtonUpdatePermission(String funcCode,String buttonCode);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCodeList
     * @return
     */
    List<DynaBean> findPcFuncButtonUpdatePermission(String funcCode,List<String> buttonCodeList);

    /**
     * 写入功能删除权限
     * @param funcCode
     * @param checkDb
     * @return
     */
    DynaBean writePcFuncButtonDeletePermission(String funcCode,String buttonCode,boolean checkDb);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCode
     * @return
     */
    DynaBean findPcFuncButtonDeletePermission(String funcCode,String buttonCode);

    /**
     * 查找展示权限
     * @param funcCode
     * @param buttonCodeList
     * @return
     */
    List<DynaBean> findPcFuncButtonDeletePermission(String funcCode,List<String> buttonCodeList);

    /**
     * 写入功能本身权限
     * @param funcCode
     * @param update 是否包含更新
     * @param delete 是否包含删除
     * @return
     */
    List<DynaBean> writePcFuncButtonPermission(String funcCode,String buttonCode,boolean update,boolean delete);

    /**
     * 查找功能本身权限
     * @param funcCode
     * @return
     */
    List<DynaBean> findPcFuncButtonPermission(String funcCode,boolean update,boolean delete);

    /**
     * 查找功能本身权限
     * @param funcCode
     * @return
     */
    List<DynaBean> findPcFuncButtonPermission(String funcCode,List<String> buttonCodeList,boolean update,boolean delete);

    /**
     * 修改按钮权限编码
     * @param funcCode
     * @param oldButtonCode
     * @param newButtonCode
     */
    void modifyPermCode(String funcCode,String oldButtonCode,String newButtonCode);

    /**
     * 查找功能按钮权限
     * @param funcCodeList
     * @return
     */
    Map<String,List<DynaBean>> findPcFuncButtonVPermission(String roleId, List<String> funcCodeList);

}
