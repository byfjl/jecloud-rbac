/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import com.je.common.base.DynaBean;
import java.util.List;

/**
 * 菜单数据权限基础操作
 */
public interface PcMenuDataService {
    
    /**
     * 格式化菜单权限模板
     * @param menuCode
     * @return
     */
    String formatMenuDataShowTemplate(String menuCode);

    /**
     * 格式化菜单权限模板
     * @param menuCode
     * @return
     */
    String formatMenuDataUpdateTemplate(String menuCode);

    /**
     * 格式化菜单权限模板
     * @param menuCode
     * @return
     */
    String formatMenuDataDeleteTemplate(String menuCode);

    /**
     * 写入权限模板
     * @param menuCode
     * @param checkDb
     * @return
     */
    DynaBean writeMenuDataShowTemplate(String menuCode, boolean checkDb);

    /**
     * 写入权限模板
     * @param menuCode
     * @param checkDb
     * @return
     */
    DynaBean writeMenuDataUpdateTemplate(String menuCode, boolean checkDb);

    /**
     * 写入权限模板
     * @param menuCode
     * @param checkDb
     * @return
     */
    DynaBean writeMenuDataDeleteTemplate(String menuCode, boolean checkDb);

    /**
     * 写入权限模板
     * @param menuCode
     * @param update
     * @param delete
     * @return
     */
    List<DynaBean> writeMenuDataTemplate(String menuCode, boolean update, boolean delete);

    /**
     * 菜单展示权限查询
     * @param menuCode
     * @return
     */
    DynaBean findMenuShowPermission(String menuCode);

    /**
     * 菜单更新权限查询
     * @param menuCode
     * @return
     */
    DynaBean findMenuUpdatePermission(String menuCode);

    /**
     * 菜单删除权限查询
     * @param menuCode
     * @return
     */
    DynaBean findMenuDeletePermission(String menuCode);

    /**
     * 查询按钮权限
     * @param menuCode
     * @return
     */
    List<DynaBean> findMenuPermission(String menuCode);

}
