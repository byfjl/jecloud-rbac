/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * 权限模板,模板使用spring expression解析器
 */
public enum PermissionPcFuncTemplateEnum {

    //--------------------------PC功能权限模板---------------------------
    /**
     * PC功能配置权限
     */
    FUNC_PC_CONFIG("功能配置权限", "pc-func-${#funcCode}-config", false, "功能配置权限"),
    /**
     * PC功能查看权限
     */
    FUNC_PC_SHOW("功能加载权限", "pc-func-${#funcCode}-show", false, "功能配置加载权限"),
    /**
     * PC功能更新权限
     */
    FUNC_PC_UPDATE("功能更新权限", "pc-func-${#funcCode}-update", false, "功能配置更新权限"),
    /**
     * PC功能删除权限
     */
    FUNC_PC_DELETE("功能删除权限", "pc-func-${#funcCode}-delete", false, "功能配置删除权限"),
    ;

    private String name;
    private String template;
    private boolean output;
    private String outputTemplate;
    private String desc;

    public String getName() {
        return name;
    }

    public String getTemplate() {
        return template;
    }

    public boolean isOutput() {
        return output;
    }

    public String getOutputTemplate() {
        return outputTemplate;
    }

    public String getDesc() {
        return desc;
    }

    PermissionPcFuncTemplateEnum(String name, String template, boolean output, String outputTemplate, String desc) {
        this.name = name;
        this.template = template;
        this.output = output;
        this.outputTemplate = outputTemplate;
        this.desc = desc;
    }

    PermissionPcFuncTemplateEnum(String name, String template, boolean output, String desc) {
        this.name = name;
        this.template = template;
        this.output = output;
        this.desc = desc;
    }

    public static void main(String[] args) {
        ExpressionParser parser = new SpelExpressionParser();
        ParserContext parserContext = new ParserContext() {

            @Override
            public boolean isTemplate() {
                return true;
            }

            @Override
            public String getExpressionPrefix() {
                return "${";
            }

            @Override
            public String getExpressionSuffix() {
                return "}";
            }

        };
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("funcCode", "aaa");
        String result = parser.parseExpression("aaa-${#funcCode}", parserContext).getValue(context, String.class);
        System.out.println("--------" + result);
    }

}
