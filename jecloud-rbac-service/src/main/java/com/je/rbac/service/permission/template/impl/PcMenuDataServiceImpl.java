/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.permission.template.impl;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.permission.PermissionOperationTypeEnum;
import com.je.rbac.service.permission.PermissionTypeEnum;
import com.je.rbac.service.permission.template.PcMenuDataService;
import com.je.rbac.service.permission.template.PermissionMenuTemplateEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PcMenuDataServiceImpl extends AbstractPermissionTemplateService implements PcMenuDataService {

    @Autowired
    private MetaService metaService;

    @Override
    public String formatMenuDataShowTemplate(String menuCode) {
        return formatTemplate(PermissionMenuTemplateEnum.MENU_SHOW.getTemplate(), Entry.build("menuCode", menuCode));
    }

    @Override
    public String formatMenuDataUpdateTemplate(String menuCode) {
        return formatTemplate(PermissionMenuTemplateEnum.MENU_UPDATE.getTemplate(), Entry.build("menuCode", menuCode));
    }

    @Override
    public String formatMenuDataDeleteTemplate(String menuCode) {
        return formatTemplate(PermissionMenuTemplateEnum.MENU_DELETE.getTemplate(), Entry.build("menuCode", menuCode));
    }

    @Override
    public DynaBean writeMenuDataShowTemplate(String menuCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataShowTemplate(menuCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writeDataPermission(PermissionTypeEnum.MENU,PermissionMenuTemplateEnum.MENU_SHOW.getName(),
                formatMenuDataShowTemplate(menuCode),menuCode,
                PermissionMenuTemplateEnum.MENU_SHOW.isOutput(),
                "", PermissionOperationTypeEnum.SHOW);
    }

    @Override
    public DynaBean writeMenuDataUpdateTemplate(String menuCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataUpdateTemplate(menuCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writeDataPermission(PermissionTypeEnum.MENU,PermissionMenuTemplateEnum.MENU_UPDATE.getName(),
                formatMenuDataUpdateTemplate(menuCode),menuCode,
                PermissionMenuTemplateEnum.MENU_UPDATE.isOutput(),
                "",PermissionOperationTypeEnum.UPDATE);
    }

    @Override
    public DynaBean writeMenuDataDeleteTemplate(String menuCode, boolean checkDb) {
        if (checkDb) {
            DynaBean bean = metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataDeleteTemplate(menuCode)));
            if (bean != null) {
                return bean;
            }
        }
        return writeDataPermission(PermissionTypeEnum.MENU,PermissionMenuTemplateEnum.MENU_DELETE.getName(),
                formatMenuDataDeleteTemplate(menuCode),menuCode,
                PermissionMenuTemplateEnum.MENU_DELETE.isOutput(),
                "",PermissionOperationTypeEnum.DELETE);
    }

    @Override
    public List<DynaBean> writeMenuDataTemplate(String menuCode, boolean update, boolean delete) {
        List<String> permCodeList = Lists.newArrayList(formatMenuDataShowTemplate(menuCode));
        if (update) {
            permCodeList.add(formatMenuDataUpdateTemplate(menuCode));
        }
        if (delete) {
            permCodeList.add(formatMenuDataDeleteTemplate(menuCode));
        }
        List<DynaBean> beanList = metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_CODE", permCodeList));

        //此处逻辑在于校验单个权限的添加是否还需要校验，用于提升性能，不用每个添加都查询数据库
        DynaBean showBean = null;
        DynaBean updateBean = null;
        DynaBean deleteBean = null;
        for (DynaBean eachBean : beanList) {
            if (formatMenuDataShowTemplate(menuCode).equals(eachBean.getStr("PERM_CODE"))) {
                showBean = eachBean;
                continue;
            }
            if (update && formatMenuDataUpdateTemplate(menuCode).equals(eachBean.getStr("PERM_CODE"))) {
                updateBean = eachBean;
                continue;
            }
            if (delete && formatMenuDataDeleteTemplate(menuCode).equals(eachBean.getStr("PERM_CODE"))) {
                deleteBean = eachBean;
                continue;
            }
        }

        List<DynaBean> resultList = new ArrayList<>();
        if (showBean != null) {
            resultList.add(showBean);
        } else {
            resultList.add(writeMenuDataShowTemplate(menuCode, false));
        }

        if(update){
            if (updateBean != null) {
                resultList.add(updateBean);
            } else {
                resultList.add(writeMenuDataUpdateTemplate(menuCode, false));
            }
        }
        if(delete){
            if (deleteBean != null) {
                resultList.add(deleteBean);
            } else {
                resultList.add(writeMenuDataDeleteTemplate(menuCode, false));
            }
        }

        return resultList;
    }

    @Override
    public DynaBean findMenuShowPermission(String menuCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataShowTemplate(menuCode)));
    }

    @Override
    public DynaBean findMenuUpdatePermission(String menuCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataUpdateTemplate(menuCode)));
    }

    @Override
    public DynaBean findMenuDeletePermission(String menuCode) {
        return metaService.selectOne("JE_RBAC_PERM", ConditionsWrapper.builder().eq("PERM_CODE", formatMenuDataDeleteTemplate(menuCode)));
    }

    @Override
    public List<DynaBean> findMenuPermission(String menuCode) {
        return metaService.select("JE_RBAC_PERM", ConditionsWrapper.builder().in("PERM_CODE", Lists.newArrayList(
                formatMenuDataShowTemplate(menuCode),
                formatMenuDataUpdateTemplate(menuCode),
                formatMenuDataDeleteTemplate(menuCode)
        )));
    }

}
