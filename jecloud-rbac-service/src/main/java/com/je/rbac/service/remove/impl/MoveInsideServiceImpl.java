/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.remove.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 在里面
 */

@Service("rmoveInsideService")
public class MoveInsideServiceImpl extends AbstractMoveService {

    @Override
    @Transactional(rollbackFor = {RuntimeException.class})
    public void move(String tableCode, String id, String toId, DynaBean resourceBean, DynaBean targetBean) {
        updateDynaBeanTableInfo(resourceBean);
        updateDynaBeanTableInfo(targetBean);
        DynaBean oldResourceOne = resourceBean.clone();

        //修改target节点 SY_NODETYPE 类型
        if("LEAF".equals(targetBean.getStr("SY_NODETYPE"))){
            metaService.executeSql("UPDATE "+targetBean.getTableCode()+" SET SY_NODETYPE = 'GENERAL' WHERE "+targetBean.getPkCode()+" = {0}", targetBean.getPkValue());
        }

        int currentChildCount = 0;
        List<Map<String,Object>> currentChildList = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) AS MAX_COUNT FROM JE_RBAC_VCOMPANYDEPT WHERE SY_PARENT = {0}",targetBean.getStr("ID"));
        if(currentChildList.size()>0){
            currentChildCount = Integer.valueOf(currentChildList.get(0).get("MAX_COUNT").toString());
        }
        resourceBean.setStr("SY_TREEORDERINDEX", targetBean.getStr("SY_TREEORDERINDEX") + String.format("%06d", currentChildCount+1));
        resourceBean.setStr("SY_ORDERINDEX", (currentChildCount+1)+"");

        String syParent = targetBean.getStr("ID");
        if(resourceBean.getStr("TYPE").equals("department") && (targetBean.getStr("TYPE").equals("company"))){
            syParent = "ROOT";
        }
        resourceBean.setStr("SY_PARENT", syParent);
        resourceBean.setStr("DEPARTMENT_PARENT",targetBean.getStr("ID"));
        resourceBean.setStr("SY_PARENTPATH", targetBean.getStr("SY_PATH"));
        resourceBean.setStr("SY_PATH", String.format("%s/%s", targetBean.getStr("SY_PATH"), resourceBean.getPkValue()));
        //查询当前bean下是否有子级
        List<DynaBean> allChildBeanList =  findNextChildren(resourceBean);
        String syNodeType = (allChildBeanList!=null && allChildBeanList.size()>0)? "GENERAL" : "LEAF";
        resourceBean.setStr("SY_NODETYPE",syNodeType);
        resourceBean.setStr("SY_LAYER",Strings.isNullOrEmpty(targetBean.getStr("SY_LAYER"))? "1" : (Integer.valueOf(targetBean.getStr("SY_LAYER")))+"");

        //构建公司，集团信息
        buildTargetInsideCompanyInfo(resourceBean,targetBean);
        metaService.update(resourceBean);
        getUpdateChildTreeOrderIndexSql(resourceBean, oldResourceOne);
    }

}
