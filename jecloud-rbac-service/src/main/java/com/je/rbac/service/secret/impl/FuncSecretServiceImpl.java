/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.rbac.service.secret.impl;

import cn.hutool.core.date.DateUtil;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.rbac.service.secret.FuncSecretService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class FuncSecretServiceImpl implements FuncSecretService {

    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean doSaveOrUpdate(DynaBean dynaBean) {
        String pkValue = dynaBean.getStr("JE_CORE_FUNCINFO_ID");
        long result = metaService.countBySql(ConditionsWrapper.builder().table("JE_RBAC_FUNCSECRET").eq("JE_CORE_FUNCINFO_ID", pkValue));
        if (result > 0) {
            commonService.buildModelModifyInfo(dynaBean);
            metaService.executeSql("UPDATE JE_RBAC_FUNCSECRET SET FUNCSECRET_SECRET_CODE = {0},FUNCSECRET_SECRET_NAME={1},FUNCSECRET_RELATION_CODE={2},FUNCSECRET_RELATION_NAME={3} WHERE JE_CORE_FUNCINFO_ID = {4}",
                    dynaBean.getStr("FUNCSECRET_SECRET_CODE"),
                    dynaBean.getStr("FUNCSECRET_SECRET_NAME"),
                    dynaBean.getStr("FUNCSECRET_RELATION_CODE"),
                    dynaBean.getStr("FUNCSECRET_RELATION_NAME"),pkValue);
        } else {
            commonService.buildModelCreateInfo(dynaBean);
            metaService.insert(dynaBean);
        }

        return dynaBean;
    }

    @Override
    public int doRemove(String funcId) {
        return metaService.delete("JE_RBAC_FUNCSECRET", ConditionsWrapper.builder().eq("JE_CORE_FUNCINFO_ID", funcId));
    }

    @Override
    public Map<String, Object> funcSecret(String funcId) {
        DynaBean funcSecret = metaService.selectOne("JE_RBAC_FUNCSECRET", ConditionsWrapper.builder()
                .eq("JE_CORE_FUNCINFO_ID", funcId));
        if (funcSecret == null) {
            return null;
        }
        return funcSecret.getValues();
    }

}
